<?php

// change the following paths if necessary
//$yii=dirname(__FILE__).'/yii/framework/yii.php';
$yii=dirname(__FILE__).'/yii/framework/YiiBase.php';
require_once($yii);
//require_once(dirname(__FILE__).'/protected/components/Framework/DataBase/DataBaseAccessor.php');

//
class Yii extends YiiBase {
    /**
     * @return WebApplication
     */
    public static function app()
    {
        return parent::app();
    }
}
use Chocolate\Binding\BindingService;
class WebApplication extends CWebApplication{
    /**
     * @var $erp Framework\DataBase\DataBaseAccessor
     */
    public $erp;
    /**
     * @var $user CWebUser
     */
    public $user;

//    /**
//     * @var $sass Sass
//     */
//    public $sass;
    /**
     * @var $bind BindingService
     */
    public $bind;

    /**
     * @var Chocolate\Cache\Cache
     */
    public $cache;
}

$config=dirname(__FILE__).'/protected/config/main.php';

//загрузчик сторонних расширений Composer.
$composerAutoload = dirname(__FILE__) . '/protected/vendor/autoload.php';
require_once($composerAutoload);

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
define('CHOCOLATE_DEBUG', true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

Yii::createWebApplication($config)->run();
