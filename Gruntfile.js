//Стандартный экспорт модуля в nodejs
module.exports = function (grunt) {
    // Инициализация конфига GruntJS
    grunt.initConfig({
        concat: {
            options: {
                separator: ';'
            },
            libs: {
                src: [
                    'js/libs/jquery.min.js',
                    'js/libs/jquery-ui.min.js',
                    'js/libs/jquery.cookie.js',
                    'js/libs/jquery.dragtable.js',
                    'js/libs/jquery.ui-contextmenu.min.js',
                    'js/libs/lodash.min.js',
                    'js/libs/jquery.floatThead.js',
                    'js/libs/bootstrap.min.js',
                    'js/libs/jquery.ba-bbq.min.js',
                    'js/libs/select2.min.js',
                    'js/libs/jquery.dynatree.min.js',
                    'js/libs/bootstrap-datetimepicker.js',
                    'js/libs/jquery.toggle.buttons.js',
//                    'js/libs/jquery.yiigridview.js' // вроде не нужна,
                    'js/libs/bootstrap-editable.min.js',
                    'js/libs/jquery.tablesorter.js',
                    'js/libs/jquery.tablesorter.widgets.js',
//                    'js/libs/jquery.tablesorter.pager.js' //пагинация убрана,
                    'js/libs/date.format.js',
                    'js/libs/json_parse.js',
                    'js/libs/jquery.ui.widget.js',
                    'js/libs/tmpl.min.js',
                    'js/libs/jquery.iframe-transport.js',
                    'js/libs/jquery.fileupload.js',
                    'js/libs/jquery.fileupload-ui.js',
                    'js/libs/jquery.fileupload-locale.js',
//                    'js/libs/resize.end.js' //заменена на jquery.ba-throttle-debounce,
                    'js/libs/jquery.fileDownload.js',
                    'js/libs/jquery.fancybox.js',
                    'js/libs/jquery.ba-throttle-debounce.min.js'
                ],
                dest: 'js/erp.js'
            },
            main: {
                src: [
                    'js/main/static/chocolate.storage.js',
                    'js/main/static/ch.options.js',
                    'js/main/static/ch.object.storage.js',
                    'js/main/static/ch.table.helper.js',
                    'js/main/static/ch.editable.callback.js',
                    'js/main/static/ch.card.init.callback.js',
                    'js/main/static/ch.attachments.js',
                    'js/main/static/ch.response.status.js',
                    'js/main/static/ch.ajaxQueue.js',
                    'js/main/classes/ch.dynatree.js',
                    'js/main/classes/ch.map.js',
                    'js/main/classes/ch.tab.js',
                    'js/main/classes/ch.messages.container.js',
                    'js/main/classes/response/ch.response.js',
                    'js/main/classes/response/ch.grid.response.js',
                    'js/main/classes/response/ch.search.response.js',
                    'js/main/classes/response/ch.packageResponse.js',
                    'js/main/classes/ch.table.js',
                    'js/main/classes/ch.filter.form.js',
                    'js/main/classes/elements/ch.grid.column.header.js',
                    'js/main/classes/elements/ch.grid.column.body.js',
                    'js/main/classes/elements/ch.card.element.js',
                    'js/main/classes/elements/ch.GridColumn.js',
                    'js/main/classes/ch.form.settings.js',
                    'js/main/classes/ch.grid.form.js',
                    'js/main/classes/ch.card.js',
                    'js/main/static/chocolate.draw.js',
                    'js/main/classes/ch.canvas.options.js',
                    'js/main/classes/ch.canvas.js',
                    'js/main/classes/elements/ch.editable.js',
                    'js/main/classes/elements/ch.text.area.editable.column.js',
                    'js/main/classes/elements/ch.text.area.editable.card.js',
                    'js/main/framework/fm.cardsCollection.js',
                    'js/main/framework/fm.ChildGridCollection.js',
                    'js/main/static/chocolate.js',
                    'js/main/static/binding.service.js',
                    'js/main/static/chocolate.events.js',
                    'js/main/functions/ch.cardFunctions.js',
                    'js/main/functions/ch.functions.js',
                    'js/main/majestic/majestic.vars.js',
                    'js/main/majestic/majestic.method.js',
                    'js/main/majestic/clear_vars_method.js',
                    'js/main/majestic/set_var_method.js',
                    'js/main/majestic/get_active_row_id_method.js',
                    'js/main/majestic/remove_var_method.js',
                    'js/main/majestic/majestic_method_factory.js',
                    'js/main/majestic/majestic.expression.js',
                    'js/main/majestic/majestic.interpreter.js',
                    'js/main/majestic/majestic.wizard_method.js',
                    'js/main/majestic/majestic.queue.js',
                    'js/main/majestic/majestic.wizard.js',
                    'js/main/majestic/task_wizard.js',
                    'js/main/majestic/select_service_task_step.js',
                    'js/main/majestic/select_executors_task_step.js',
                    'js/main/majestic/select_description_task_step.js',
                    'js/main/functions/open_wizard_dialog.js',
                    'js/main/functions/open_task_wizard_dialog_end.js',
                    'js/main/run.js'
//                    'js/main/chocolate.js',
//                    'js/main/majestic.js'
                ],
                dest: 'js/main.js'
            }
        },
        uglify: {
            main: {
                files: {
                    // Результат задачи concat
                    'js/erp.min.js': ['js/erp.js']
                }
            }
        },
        cssmin: {
            combine: {
                files: {
                    'css/libs/erp.css': [
                        'css/libs/font-awersome.css',
                        'css/libs/jquery.fancybox.css',
                        'css/libs/ui.dynatree.css',
                        'css/libs/bootstrap.min.css',
                        'css/libs/bootstrap-toggle-buttons.css',
                        'css/libs/jquery.fileupload-ui.css',
                        'css/libs/bootstrap-editable.css',
//                        'css/libs/bootstrap-yii.css' //не используется,
                        'css/libs/datetimepicker.css',
                        'css/libs/jquery-ui-bootstrap.css'
                    ]
                },
                minify: {
                    expand: true,
                    cwd: 'css/libs/',
                    src: ['erp.css']
//                    ,
//                    dest: 'css/libs/',
//                    ext: '.min.css'
                }
            }
        }
//        ,
//        removelogging: {
//            dist: {
//                src: "js/erp.min.js",
//                dest: "js/erp.min.js"
//            }
//        }


    });

    //Загрузка модулей, которые предварительно установлены
//    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
//    grunt.loadNpmTasks("grunt-remove-logging");
    //Эти задания будут выполнятся сразу же когда вы в консоли напечатание grunt, и нажмете Enter
//    grunt.registerTask('default', ['jshint', 'concat']);
//    grunt.registerTask('default', ['concat', 'uglify', 'cssmin']);
    grunt.registerTask('default', ['concat', 'uglify', 'cssmin']);
    grunt.registerTask('debug', ['concat', 'cssmin']);


};