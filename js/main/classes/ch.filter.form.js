
function ChFilterForm($filter_form) {
    this.$form = $filter_form;
}
ChFilterForm.prototype = {
    getData: function () {
        var data = this.$form.serializeArray(),
            result = {};
        $.each(data, function (i, element) {
            var value = element.value,
                name = element.name;
//            console.log(name, value)
            if (name.slice(-2) == '[]') {
                name = name.slice(0, name.length - 2)
                if (typeof(result[name]) == 'undefined') {
                    result[name] = '';
                }
                result[name] += value + '|'
            } else {
                if (value != '') {
                    result[name] = value
                }
            }
        });
//        console.log(result)
        return result;
    }
}
