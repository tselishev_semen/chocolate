/**
 * Класс, отвечающий за инициализацию скриптов таблицы(Resort, resize, pagination, dragtable, scrolling
 */
function ChTable($table) {
    this.$table = $table;
    /**
     * @type {ChGridForm}
     */
    this.ch_form = ChObjectStorage.create($table.closest('form'), 'ChGridForm');
}
ChTable.prototype = {
    _initTableSorter: function () {
        var $footer = this.ch_form.getFooter(),
            $page_display = $footer.find("span.pagedisplay");
        this.$table.tablesorter(
            {
                widgets: ["zebra", "filter"],
                headers: {
                    0: { sorter: false}
                },
                widgetOptions: {
                    filter_hideEmpty: false,
                    savePages: false
                }
            })
//            .tablesorterPager({
//                container: $footer,
//                cssPageDisplay: $page_display,
//                output: ChOptions.tablesorter.output,
//                size: ChOptions.tablesorter.size
//            });
    },
    _initResize: function () {
        var $table = this.$table,
            $headers = this.ch_form.getTh().filter(function (i) {
                return i > 0
            }).children('div'),
            start_width = 0,
            ch_form = ChObjectStorage.create($table.closest('form'), 'ChGridForm');

        $headers.each(function (i) {
            var $column_resize, $body_resize;
            $(this).resizable({
                handles: 'e',
                containment: "parent",
                distance: 1,
                stop: function (event, ui) {
                    try {
                        var index = ui.element.parent().get(0).cellIndex,
                            ui_width = ui.size.width,
                            $fixed_table = ch_form.getFixedTable();

                        ch_form.setColumnWidth(index, ui_width);

                        $table.children("colgroup").children("col").eq(index).width(ui_width);
                        $fixed_table.children("colgroup").children("col").eq(index).width(ui_width);
                        ui.element.width(ui_width);

                        var end_width = ui.element.width(),
                            delta_width = end_width - start_width;

                        if (delta_width < 0) {
                            var table_width = $fixed_table.get(0).offsetWidth + delta_width;
                            $fixed_table.width(table_width);
                            $table.width(table_width);
                        }
                    } catch (e) {
                        console.log(e)
                    } finally {
                        $column_resize.remove();
                        $body_resize.remove();
                    }
                },
                resize: function (event, ui) {
                    var position = ui.element.offset();
                    $column_resize.css({ left: position.left + ui.size.width});
                    $body_resize.css({ left: position.left + ui.size.width});
                },
                start: function (event, ui) {
                    var position = ui.element.offset(),
                        header_height = 24,
                        real_height = $table.closest("div").height() - header_height,
                        visible_height = $table.height() - header_height;

                    $column_resize = $("<span>", { class: "column-resize column-resize-header"})
                        .css({
                            top: position.top,
                            left: position.left
                        });
                    $body_resize = $("<span>", { class: "column-resize column-resize-body"})
                        .css({
                            top: position.top + header_height,
                            left: position.left,
                            height: Math.min(visible_height, real_height)
                        });

                    Chocolate.$content.append($column_resize).append($body_resize)
                    start_width = ui.element.width();
                }
            });
        })
    },
    _initFloatThead: function () {
//        console.log('777')
        var _this = this;
        _this.$table.floatThead({
            scrollContainer: function ($table) {
                return _this.ch_form._getUserGrid();
            }
        });
    },
    _initData: function () {
        this.ch_form.restoreData();
    },
    _initSettings: function () {
        this.ch_form.setDefaultSettings();
        if (this.ch_form.ch_form_settings.isAutoUpdate()) {
            this.ch_form.ch_form_settings.startAutoUpdate();
        }
    },
    _initContextMenu: function () {
        var $fixed_table = this.ch_form.getFixedTable(),
            $th = $fixed_table.children('thead').find('th'),
            $sorted_th = $th.filter(function (i) {
                return i > 0
            }),
            _this = this,
            count = $th.length - 1,
            tables = [_this.$table.eq(0)[0], $fixed_table.eq(0)[0]];
        $sorted_th.contextmenu({
            show: { effect: "blind", duration: 0 },
            menu: [
                {title: '< Сделать первой', cmd: 'to-first'},
                {title: 'Сделать последней >', cmd: 'to-last'},
                {title: 'Все колонки', cmd: 'toggle-cols'}
//                {title: 'Скрыть колонку', cmd:'hide-col'}
            ],
            select: function (event, ui) {
                var from = ui.target.closest('th').get(0).cellIndex;
                switch (ui.cmd) {
                    case 'to-first':
                        ChTableHelper.swapColsManyTables(tables, from, 1);
                        _this.$table.floatThead('reflow');
                        _this.ch_form.changeSettings(from, 1);
                        break;
                    case 'to-last':
                        ChTableHelper.swapColsManyTables(tables, from, count);
                        _this.$table.floatThead('reflow');
                        _this.ch_form.changeSettings(from, count);
                        break;
//                    case 'hide-col':
//                        ChTableHelper.hideColsManyTables(tables, [from]);
//                        break;
                    case 'toggle-cols':
                        _this.ch_form.toggleAllCols();
                        break;
                    default :
//                        alert('неизвестная команда')
                        break
                }
            }
        });
    },
    _initDragtable: function () {
        this.ch_form.getFixedTable().dragtable();
    },
    initScript: function () {
        this._initSettings();
        this._initTableSorter();
        this._initResize();
        this._initFloatThead();
        this._initContextMenu();
        this._initDragtable()
        this._initData();
    },
    initAttachmentScript: function () {
        this._initSettings();
        this._initTableSorter();
        this._initResize();
        this._initFloatThead();
        this._initContextMenu();
        this._initDragtable()
    }
}