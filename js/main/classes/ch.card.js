function ChCard($grid_tabs) {
    this.$grid_tabs = $grid_tabs;
    this._save_url = null;
    this._$container = null;
    this._key = null;
    this._view = null;
    this._form_id = null;
    this._$grid_form = null;
    this._container_id = null;
    this._$header = null;
    this._$error_container = null;
}
ChCard.prototype = {
    getHeaderContainer: function () {
        if (this._$header == null) {
            this._$header = this.getContainer().children('header');
        }
        return this._$header;
    },
    getErrorContainer: function () {
        if (this._$error_container == null) {
            this._$error_container = this.getHeaderContainer().children('.card-error');
        }
        return this._$error_container;

    },
    getSaveUrl: function () {
        if (this._save_url == null) {
            this._save_url = this.$grid_tabs.attr('data-save-url');
        }
        return this._save_url;
    },
    getContainer: function () {
        if (this._$container == null) {
            this._$container = this.$grid_tabs.parent('div');
        }
        return this._$container;

    },
    getKey: function () {
        if (this._key == null) {
            //TODO: сменить на дата-кей;
            this._key = this.$grid_tabs.attr('data-pk');
        }
        return this._key;
    },
    getView: function () {
        if (this._view == null) {
            this._view = this.$grid_tabs.attr("data-view");
        }
        return this._view;
    },
    getFormID: function () {
        if (this._form_id == null) {
            this._form_id = this.$grid_tabs.attr('data-form-id');
        }
        return this._form_id;
    },
    /**
     *
     * @returns {ChGridForm}
     */
    getGridForm: function () {
        if (this._$grid_form == null) {
            this._$grid_form = ChObjectStorage.create($('#' + this.getFormID()), 'ChGridForm');
        }
        return this._$grid_form
    },
    getContainerID: function () {
        if (this._container_id == null) {
            this._container_id = this.getContainer().attr('id');
        }
        return this._container_id;
    },
    _closeCard: function () {
        var id = this.getContainerID(),
            $li = Chocolate.$content.find('li[aria-controls=' + id + ']');
        Chocolate.closeTab($li);
    },
    getChangedObj: function () {
        var grid_from = this.getGridForm(),
            pk = this.getKey();
        return grid_from.getChangedObj()[pk];
    },
    getDataObj: function () {
        var grid_from = this.getGridForm(),
            pk = this.getKey();
        return grid_from.getDataObj()[pk];
    },
    _isChanged: function () {
        var change_obj = this.getChangedObj();
        return !$.isEmptyObject(change_obj);
    },
    getActualDataObj: function () {
        return Chocolate.mergeData(this.getDataObj(), this.getChangedObj());
    },
    validate: function (data_obj) {
        var grid_form = this.getGridForm(),
            errors = grid_form.validate(data_obj);
        if ($.isEmptyObject(errors)) {
            return true;
        } else {
            this._showErrors(errors)
            return false;
        }
    },
    _showErrors: function (errors) {
        var pk = this.getKey();
        for (var key in  errors) {
            this.$grid_tabs.find('a[rel=' + errors[key] + '_' + pk + ']')
                .closest('div.card-col')
                .children('label').addClass('card-error');
        }
    },
    _resetErrors: function () {

    },
    _clearChangeObj: function () {
        var changed_obj = this.getGridForm().getChangedObj();
        delete changed_obj[this.getKey()];
    },
    save: function () {
        this._resetErrors();
        if (this._isChanged()) {
            var actualDataObj = this.getActualDataObj();
            if (this.validate(actualDataObj)) {

                var gridForm = this.getGridForm(),
                    url = this.getSaveUrl(),
                    changedData = {},
                    _this = this;

                changedData[this.getKey()] = actualDataObj;
                var data = {
                    jsonChangedData: JSON.stringify(changedData),
                    jsonDeletedData: {}
                };
                $.post(url, data)
                    .done(function (response) {
                        var ch_response = new ChResponse(response);
                        if (ch_response.isSuccess()) {
                            _this._closeCard();
                            ch_response.sendMessage(gridForm.getMessagesContainer());
                            gridForm.refresh();
                        } else {
                            var ch_messages_container = ChObjectStorage.create(_this.getErrorContainer(), 'ChMessagesContainer');
                            ch_response.sendMessage(ch_messages_container);
                        }
                    })
                    .fail(function (response) {
                        alert("error");
                    })
            }
        } else {
            this._closeCard();
        }
    },
    setElementValue: function ($card_element, value, isEdit, text) {
        $card_element.editable('setValue', value)
        if (!isEdit) {
            $card_element.editable('disable')
        }
        //затык для селект 2
        if (typeof(text) != 'undefined' && text !== null && text.length > 0) {
            $card_element.text(text)
        }
    },
    undoChange: function () {
        var pk = this.getKey();
        if (this._isChanged()) {
            var $table = this.getGridForm().getTable(),
                changed_obj = this.getChangedObj();
            jQuery.each(changed_obj, function (i, val) {
                var $gridCell = $table.find(" a[data-pk=" + pk + "][rel$=" + i + "]"),
                    oldValue = $gridCell.attr("data-value");
                $gridCell.editable("setValue", oldValue, true)
            })
        }
        this._closeCard();
        this._clearChangeObj();
    }
};