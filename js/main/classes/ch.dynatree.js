function ChDynatree($elm) {
    this.$elm = $elm;
    this.options = null;
    this.data = [];
    this.auto_complete_data = [];
}
ChDynatree.prototype.init = function (options) {
    this.options = options;
    this.getNodes = function () {
        return options['children'];
    };
    this.getUrl = function () {
        return options['url'];
    };
    this.isExpandNodes = function () {
        return options['expand_nodes'];
    };
    this.isSelectAll = function () {
        return options['select_all'];
    };
    this.isRestoreState = function () {
        return options['restore_state'];
    };
    this.getSeparator = function () {
        return options['separator'];
    };
    this.getRootID = function () {
        return options['root_id'];
    };
    this.getTitle = function () {
        return options['title'];
    };
    this.getColumnTitle = function () {
        return options['column_title'];
    };
    this.getColumnID = function () {
        return options['column_id'];
    };
    this.getColumnParentID = function () {
        return options['column_parent_id'];
    };
}
ChDynatree.prototype.setData = function ($input, $content) {
    var url = this.getUrl(),
        raw_data,
        map = {},
        root_id = this.getRootID();

    if (url) {
        jQuery.ajax({
            type: 'GET',
            url: url,
            async: false,
            success: function (response) {
                var ch_response = new ChResponse(response);
                if (ch_response.isSuccess()) {
                    raw_data = ch_response.getData()
                    jQuery.data($input.get(0), "dialog", $content);
                } else {
                    alert('Ошибка при загрузке данных')
                }
            }
        })
    } else {
        raw_data = this.getNodes();
    }

    for (var i in raw_data) {
        var node = raw_data[i];
        node.title = node.data[this.getColumnTitle()];
        node.key = node.data[this.getColumnID()];
        node.index = i;
        node.icon = false;
        node.parentid = node.data[this.getColumnParentID()];
        node.children = [];
        map[node.key] = node.index; // use map to look-up the parents
        this.auto_complete_data.push({label: node.title, id: node.key});
    }

    for (var i in raw_data) {
        var node = raw_data[i];
        if (typeof(node.parentid) != 'undefined' && node.parentid !== root_id && node.parentid) {
            raw_data[map[node.parentid]].children.push(node);
        } else {
            this.data.push(node);
        }
    }
}
ChDynatree.prototype.dialogEvent = function ($content, $tree, $input, $checkbox, $select) {
    var _this = this;
    $content.dialog({
        resizable: false,
        title: this.getTitle(),
        dialogClass: 'wizard-dialog',
        modal: true,
//        height: 500,
//        width: 700,
        buttons: {
            OK: {
                text: 'OK',
                class: 'wizard-active wizard-next-button',
                click: function (bt, elem) {
                    var selected_nodes = $tree.dynatree("getSelectedNodes");
                    var val = '', select_html = '';
                    var is_select_all = _this.isSelectAll();
                    for (var i in selected_nodes) {
                        var node = selected_nodes[i];
                        if (is_select_all || node.childList == null) {
                            val += node.data.key + _this.getSeparator();
                            select_html += "<option>" + node.data.title + "</option>";
                        }
                    }
                    $input.val(val);
                    $select.html(select_html);
                    $checkbox.children('input').attr('checked', false)
                    $(this).dialog("close");
                }},
            Отмена: {
                text: 'Отмена',
                class: 'wizard-cancel-button',
                click: function () {
                    $checkbox.children('input').attr('checked', false)
                    $(this).dialog("close");
                }
            }
        }
    });
}
ChDynatree.prototype.checkboxClickEvent = function ($checkbox, $tree) {

    $checkbox.on('click', 'input', function () {
        if ($(this).is(':checked')) {
            $tree.dynatree("getRoot").visit(function (node) {
                node.select(true);
            });
        } else {
            $tree.dynatree("getRoot").visit(function (node) {
                node.select(false);
            });
        }
    })

}
ChDynatree.prototype.autoCompleteEvent = function ($search, $tree, $content) {
    var _this = this;
    $search.autocomplete({
        delay: 100,
        source: function (request, response) {
            var searchParam = request.term.toLowerCase();

            var responseArray = _this.auto_complete_data.filter(function (item, i, arr) {
                var source = item.label.toLowerCase();
                if (source.indexOf(searchParam) != -1) {
                    return true
                }
                return false
            });
            $content.find('.node-searched').removeClass('node-searched')
            $content.find("[data-title*='" + searchParam + "']").addClass('node-searched')
            response(responseArray)
        },
        close: function (event, ui) {
            $content.find('.node-searched').removeClass('node-searched')

        },
        select: function (event, ui) {
            var id = ui.item.id
            $content.find('.node-searched').removeClass('node-searched')
            var tree = $tree.dynatree("getTree");
            tree.selectKey(id, true);
            tree.activateKey(id);
        }
    });
}
ChDynatree.prototype.load = function (options) {
    this.init(options);
    var $tree_con = this.$elm.parent(),
        $input = $tree_con.children('input[type=hidden]'),
        $dialog = jQuery.data($input.get(0), "dialog"),
        is_restore_state = this.isRestoreState(),
        $select = $tree_con.children('select');

    if (typeof($dialog) != 'undefined') {
        var default_values = $input.val().split(this.getSeparator());
        if (!is_restore_state) {
            $input.val('');
            $select.html('');
        }
        $dialog.children('.widget-tree').dynatree("getRoot").visit(function (node) {
            if (is_restore_state && $.inArray(node.data.key, default_values) != '-1') {
                node.select(true);
            } else {
                node.select(false);
            }
        });
        $dialog.dialog('open');
    } else {
        var $content = $('<div></div>'),
            $tree = $('<div class="widget-tree"></div>');

        //Необходимо инициализировать данные
        this.setData($input, $content);
        this.options.children = this.data;

        $tree.dynatree(this.options);
        if (this.isExpandNodes()) {
            $tree.dynatree("getRoot").visit(function (node) {
                node.expand(true);
            });
        }
        var $search = $('<input type="text">'),
            $header = $('<div class="widget-header-tree"></div>'),
            $checkbox = $('<span class="tree-checkbox"><input type="checkbox"><span class="tree-checkbox-caption">Выделить все</span></span>');
        $header.append($search);
        $content.prepend($header)
        $content.append($tree)

        this.dialogEvent($content, $tree, $input, $checkbox, $select);
        $content.next().prepend($checkbox)
        this.autoCompleteEvent($search, $tree, $content);
        this.checkboxClickEvent($checkbox, $tree);
    }
}
