/**
 * Класс, представляющий Форму в Шоколаде на клиенте(<form>)
 */
function ChGridForm($form) {
    this.$form = $form;
    this.ch_form_settings = new ChFormSettings(this);
    this._id = null;
    this._view_id = null;
    this._tab_caption = null;
    this._$table = null;
    this._$fixed_table = null;
    this._refresh_url = null;
    this._parent_form_id = null;
    this._ch_messages_container = null;
    this._parent_id = null;
    this._is_ajax_add = null;
    this._$user_grid = null;
    this._user_grid_id = null;
    this._storage = null;
    this._ch_filter_form = null;
    this._$grid_form = null;
    this._parent_view = null;
    this._save_url = null;
    this._parent_pk = null;
    this._is_card_support = null;
    this._$footer = null;
    this._$th = null;
    this._$thead = null;
    this._type = null;
    this._$save_btn = null;
    this.fmChildGridCollection = new FmChildGridCollection();
    this._chCardsCollection = null;
}
ChGridForm.TEMPLATE_TD = '<td class="{class}{class2}"><div class="table-td"><a data-value="{value}" data-pk ="{pk}" rel="{rel}" class="editable"></a></div></td>';
ChGridForm.TEMPLATE_FIRST_TD = '<td class="grid-menu"><span class="card-button" data-id="card-button" title="Открыть карточку"></span>';
/**
 *
 * @returns {FmChildGridCollection}
 */
ChGridForm.prototype.getFmChildGridCollection = function(){
    return this.fmChildGridCollection
};

/**
 *
 * @param chCardsCollection {FmCardsCollection}
 */
ChGridForm.prototype.setFmCardsCollection = function(chCardsCollection){
    this._chCardsCollection = chCardsCollection;
};
/**
 *
 * @returns {null|FmCardsCollection}
 */
ChGridForm.prototype.getFmCardsCollection = function(){
    return this._chCardsCollection;
};
ChGridForm.prototype.getThead = function () {
    if (this._$thead == null) {
        this._$thead = this.getTable().children('thead');
    }
    return this._$thead;
};
ChGridForm.prototype.getType = function () {
    if (this.type == null) {
        this._type = this.$form.children('section').attr('data-id');
    }
    return this._type;
};
ChGridForm.prototype.getTh = function () {
    if (this._$th == null) {
        this._$th = this.getThead().children('tr:first-child').children('th');
    }
    return this._$th;
};
ChGridForm.prototype.hasSettings = function () {
    if ($.isEmptyObject(this.getSettingsObj())) {
        return false;
    }
    return true;
};
ChGridForm.prototype.setDefaultSettings = function () {
    var $th = this.getTh();
    if (this.hasSettings()) {
        var $tr = this.getThead().children('tr'),
            $tr_sorted = $('<tr></tr>'),
            settings_obj = this.getSettingsObj();
        var sorted_columns = settings_obj.sort(function (a, b) {
            if (a.key == 'chocolate-control-column') {
                return -1
            }
            if (b.key == 'chocolate-control-column') {
                return 1
            }
            if (a.weight > b.weight) {
                return 1
            } else {
                return -1
            }
        });

        for (var i in sorted_columns) {
            var column = sorted_columns[i];
            $tr_sorted.append($th.filter('[data-id="' + column.key + '"]'));
        }
        $tr.replaceWith($tr_sorted);
    } else {
        var settings_obj = [];
        $th.each(function (i, elem) {
            if (i == 0) {
                settings_obj[i] = {
                    key: $(elem).attr('data-id'),
                    weight: i,
                    width: '28'
                };
            } else {
                settings_obj[i] = {
                    key: $(elem).attr('data-id'),
                    weight: i,
                    width: ChOptions.settings.defaultColumnsWidth
                }
            }
        })
        this.setSettingsObj(settings_obj)
    }

};
ChGridForm.prototype.getFooter = function () {
    if (this._$footer == null) {
        this._$footer = this.$form.siblings('footer');
    }
    return this._$footer;
};
ChGridForm.prototype.setSettingsObj =  function (setting_obj) {
    var storage = this.getSettingsObj();
    Chocolate.storage.local.settings[this.getView()] = setting_obj
};
ChGridForm.prototype.getColumnWidth = function (index) {
    var settingObj = this.getSettingsObj();
    if ($.isEmptyObject(settingObj)) {
        this.setColumnWidth(index, ChOptions.settings.defaultColumnsWidth)
        return ChOptions.settings.defaultColumnsWidth;
    }
    return settingObj[index].width;
};
ChGridForm.prototype.setColumnWidth = function (index, width) {
    var settingObj = this.getSettingsObj();
    if (!$.isEmptyObject(settingObj)) {
        settingObj[index].width = width;
    }
};
ChGridForm.prototype.changeSettings = function (start_index, end_index) {
    var min_index = 1, setting = this.getSettingsObj();
    if (!$.isEmptyObject(setting)) {
        var key = this.getView(), obj, new_settings = [];
        if (start_index < end_index) {
            for (var i in setting) {
                obj = setting[i];
                if (obj.weight == 0) {
                    new_settings[0] = {key: obj.key, weight: obj.weight, width: obj.width};
                } else {
                    if (obj.weight > start_index && obj.weight <= end_index) {
                        var new_weight = obj.weight - 1;
                        new_settings[new_weight] = {key: obj.key, weight: new_weight, width: obj.width};
                    } else if (obj.weight == start_index) {
                        var new_weight = Math.max(end_index, min_index);
                        new_settings[new_weight] = {key: obj.key, weight: new_weight, width: obj.width};
                    } else {
                        new_settings[obj.weight] = {key: obj.key, weight: obj.weight, width: obj.width};
                    }
                }
            }
        }
        if (start_index > end_index) {
            for (var i in setting) {
                obj = setting[i];
                if (obj.weight == 0) {
                    new_settings[0] = {key: obj.key, weight: obj.weight, width: obj.width};
                } else {
                    if (obj.weight < start_index && obj.weight >= Math.max(end_index, min_index)) {
                        var new_weight = obj.weight + 1;
                        new_settings[new_weight] = {key: obj.key, weight: new_weight, width: obj.width};
                    } else if (obj.weight == start_index) {
                        var new_weight = Math.max(end_index, min_index);
                        new_settings[new_weight] = {key: obj.key, weight: new_weight, width: obj.width};
                    } else {
                        new_settings[obj.weight] = {key: obj.key, weight: obj.weight, width: obj.width};
                    }
                }
            }
        }
        this.setSettingsObj(new_settings)
    }
};
ChGridForm.prototype.getSettingsObj = function () {
    var storage = Chocolate.storage.local.settings,
        key = this.getView();
    if (typeof(storage[key]) == 'undefined') {
        storage[key] = {};
    }
    return storage[key];
};
ChGridForm.prototype.isCardSupport = function () {
    if (this._is_card_support == null) {
        this._is_card_support = this.$form.attr('data-card-support') == '1';
    }
    return this._is_card_support;
};
ChGridForm.prototype.getActiveRowID = function () {
    return this.getActiveRow().attr('data-id');
};
ChGridForm.prototype.isAutoOpenCard = function () {
    return this.getGridPropertiesObj()['autoOpenCard'];
};
ChGridForm.prototype.openCard = function (pk) {
    if (this.isCardSupport()) {
        var view = this.getView(),
            $tabs = Chocolate.$tabs,
            uniqueID = Chocolate._generateTabID(pk, view),
            $a = $tabs.find('li[data-tab-id=\'' + uniqueID + '\']').children('a');
//    console.log(this.getFmCardsCollection().generateTabs(view,pk, viewID))
        /**
         * Если существует уже открытая закладка, переключаемся на нее, а не добавляем дубликат
         */
        if ($a.length == 0) {
            var viewID = this.getID(),
                caption;
            if (Chocolate.isNumeric(pk)) {
                caption = this.getTabCaption() + ' [' + pk + ']';
            } else {
                caption = this.getTabCaption() + '[новая запись]';
            }
            Chocolate.inserted_tab_counter++;
            var tabTemplate = "<li data-view-id = '" + viewID + "' data-tab-id=" + uniqueID + " data-id =\'" + pk + "\' data-view=\'" + view + "\' >" +
                    "<a id ='_tab" + Chocolate.inserted_tab_counter + "' href='#'>#{label}</a><span class='tab-closed fa fa-times' role='presentation'></span>" + "</li>",
                $li = $(tabTemplate.replace(/#\{label\}/g, caption));


            $tabs.children('ul').append($li);
            $tabs.tabs("refresh");
        var _this = this;
            $a = $li.children('a');
            /**
             * @type {ChTab}
             */
            var chTab = ChObjectStorage.create($a, 'ChTab');

                $tabs.tabs({
                    beforeLoad: function( event, ui ) {
                        ui.jqXHR.abort();
                        var html = _this.getFmCardsCollection().generateTabs(view,pk, viewID);
                        ui.panel.html(html)

                    }
                });

            $tabs.tabs({ active: chTab.getIndex()})
            var href = '#' + chTab.getPanelID(),
                $context = $(href)
            ChocolateDraw.drawCard($context)
          //инициализируем вложенные табы
            Chocolate.initTabs($context)
            $a.attr('href', href)
        } else {
            /**
             * @type {ChTab}
             */
            var chTab = ChObjectStorage.create($a, 'ChTab');
            $tabs.tabs({ active: chTab.getIndex() })
        }
    }

};
ChGridForm.prototype.getCallbackID = function () {
    return this.getUserGridID();
};
/**
 * @returns {jQuery}
 */
ChGridForm.prototype.getGridForm = function () {
    if (this._$grid_form == null) {
        this._$grid_form = this.$form.closest('section');
    }
    return this._$grid_form;
};
/**
 * @returns {String}
 */
ChGridForm.prototype.getSaveUrl = function () {
    if (this._save_url == null) {
        this._save_url = this.$form.attr('data-save');
    }
    return this._save_url;
};
ChGridForm.prototype. getPreviewObj = function () {
    var storage = this.getStorage();
    return storage[this.getID()].preview;
};
/**
 * @param $row {jQuery}
 */
ChGridForm.prototype.selectRow = function ($row, group) {
    var $table = this.getTable();
    $table.find('.row-active').removeClass('row-active');
    $row.addClass('row-active');
    if (!group) {
        $table.find('.row-selected').removeClass('row-selected');
    }
    $row.toggleClass('row-selected');
    if($row.hasClass('row-selected')){

    var preview_data = this.getPreviewObj(),
        id = $row.attr('data-id');
    if (typeof(preview_data[id]) != 'undefined') {
        var info = preview_data[id], html = '';
        for (var key in info) {
            html += '<span class="footer-title">';
            html += key + '</span>: <span>';
            html += info[key];
            html += ' </span><span class="footer-separator"></span>';
        }
        this.getGridForm().find('footer div[data-id=info]').html(html);
    }
    }
};
/**
 * @returns {String}
 */
//ChGridForm.prototype.getParentID = function () {
//    if (this._parent_id == null) {
//        var parent_id = this.this.$form.attr('data-parent-id');
//        if (typeof(parent_id) == 'undefined') {
//            parent_id = '';
//        }
//        this._parent_id = parent_id;
//    }
//    return this._parent_id;
//};
/**
 * @returns {String}
 */
ChGridForm.prototype.getParentView = function () {
    if (this._parent_view == null) {
        var parent_form_id = this.getParentFormID();
        if (parent_form_id) {
            this._parent_view = ChObjectStorage.create($('#' + parent_form_id), 'ChGridForm').getView();
        } else {
            this._parent_view = '';
        }
    }
    return this._parent_view;
};
/**
 * @returns {Object}
 */
ChGridForm.prototype.getStorage = function () {
    if (this._storage == null) {
        var form_id = this.getID(),
            storage = Chocolate.storage.session[form_id];
        if (typeof(storage) == 'undefined' || $.isEmptyObject(storage)) {
            Chocolate.storage.session[form_id] = new Object({data: {}, preview: {}, change: {}, deleted: {}, defaultValues: {}, required: {}});
        }
        this._storage = Chocolate.storage.session;
    }
    return this._storage;
};
ChGridForm.prototype.getRequiredObj = function () {
    var storage = this.getStorage();
    return storage[this.getID()].required;
};
ChGridForm.prototype.getDeletedObj= function () {
    var storage = this.getStorage();
    return storage[this.getID()].deleted;
};
ChGridForm.prototype.getChangedObj = function () {
    var storage = this.getStorage();
    return storage[this.getID()].change;
};
ChGridForm.prototype.isHasChange = function () {
    if (this._isAttachmentsModel()) {
        return ChAttachments.isNotEmpty(this.getID()) || !$.isEmptyObject(this.getDeletedObj());
    } else {
        return !$.isEmptyObject(this.getChangedObj()) || !$.isEmptyObject(this.getDeletedObj());
    }
};
ChGridForm.prototype.getGridPropertiesObj = function () {
    var storage = this.getStorage();
    return storage[this.getID()].gridProperties;
};
ChGridForm.prototype.getDataObj = function () {
    var storage = this.getStorage();
    return storage[this.getID()].data;
};
ChGridForm.prototype.getDefaultObj = function () {
    var storage = this.getStorage();
    return storage[this.getID()].defaultValues;
};
ChGridForm.prototype._resetErrors = function () {
    this.$form.find('td.grid-error').removeClass('grid-error');
};
ChGridForm.prototype.save = function () {
    this._resetErrors();
    var user_grid_id = this.getUserGridID(),
        parent_view = this.getParentView(),
        parent_id = this.getParentPK(),
        ch_messages_container = this.getMessagesContainer(),
        _this = this,
        deleted_obj = $.extend({}, this.getDeletedObj());

    if (!this._isAttachmentsModel()) {
        var change_obj = this.getChangedObj(),
            data_obj = this.getDataObj(),
            response_change_obj = {};

        for (var name in change_obj) {
            if (!$.isEmptyObject(change_obj[name])) {
                if (typeof(deleted_obj[name]) == 'undefined') {
                    response_change_obj[name] = Chocolate.mergeData(data_obj[name], change_obj[name]);
                }
            }
        }

        if (!$.isEmptyObject(response_change_obj) && !$.isEmptyObject(deleted_obj)) {
            for (var row_id in deleted_obj) {
                delete response_change_obj[row_id];
            }
        }

        for (var property in deleted_obj) {
            if (!Chocolate.isNumeric(property)) {
                delete deleted_obj[property];
            }
        }

        if (!$.isEmptyObject(response_change_obj) || !$.isEmptyObject(deleted_obj)) {
            //отсекаем изменения в уже удаленных строках, они нам не нужны
            var errors = [];
            for (var index in response_change_obj) {
                var error = this.validate(response_change_obj[index]);
                if (!$.isEmptyObject(error)) {
                    errors[index] = error;
                }
            }

            if ($.isEmptyObject(errors)) {
                var changed_data = JSON.stringify(response_change_obj),
                    deleted_data = JSON.stringify(deleted_obj),
                    url = this.getSaveUrl() + '&parentView=' + parent_view + '&parentID=' + parent_id,
                    data = {
                        jsonChangedData: changed_data,
                        jsonDeletedData: deleted_data
                    };

                $.post(url, data)
                    .done(function (response) {
                        var ch_response = new ChResponse(response);
                        //TODO: вроде здесь сообщения можно удалять;
//                            ch_response.sendMessage(ch_messages_container);
                        if (ch_response.isSuccess()) {
                            _this.getSaveButton().removeClass('active');
                            _this.refresh();
                        } else {
                            ch_response.sendMessage(ch_messages_container);
                        }
                    })
                    .fail(function (response) {
                        ch_messages_container.sendMessage('Возникла непредвиденная ошибка при сохраненнии сетки.', ChResponseStatus.ERROR);
                    })
            } else {
                for (var pk_key in errors) {
                    for (var column_key in errors[pk_key]) {
                        this.$form.find('a[data-pk=' + pk_key + '][rel=' + user_grid_id + '_' + errors[pk_key][column_key] + ']').closest('td').addClass('grid-error')
                    }
                }
                ch_messages_container.sendMessage('Заполните обязательные поля( ошибки подсвечены в сетке).', ChResponseStatus.ERROR)
            }
        } else {
            ch_messages_container.sendMessage('Данные не были изменены.', ChResponseStatus.WARNING)
        }
    } else {
        var form_id = this.getID();
//        if (ChAttachments.isNotEmpty(form_id)) {

            if (ChAttachments.isNotEmpty(form_id)) {
                while (ChAttachments.isNotEmpty(form_id)) {
                    var dev_obj = this.getDefaultObj();
                    var ownerLock = dev_obj['ownerlock'];
                    var file = ChAttachments.pop(form_id);
                    this.$form.fileupload({
                        formData: {FilesTypesID: 4, OwnerLock: ownerLock}
                    });
                    this.$form.fileupload('send', {files: file})
                }

            this.refresh();
            }
//        }
        else {
            if (!$.isEmptyObject(deleted_obj)) {
                Chocolate.saveAttachment(this);
            } else {
                ch_messages_container.sendMessage('Данные не были изменены.', ChResponseStatus.WARNING);
            }
        }
    }

    return false;
};
ChGridForm.prototype.validate = function (data) {
    var requiredFields = this.getRequiredObj(),
        errors = [];
    for (var field in requiredFields) {
        if (typeof(data[field]) == 'undefined' || !data[field]) {
            errors.push(field);
        }
    }
    return errors;
};
/**
 * @returns {ChFilterForm}
 */
ChGridForm.prototype.getFilterForm =  function () {
    if (this._ch_filter_form == null) {
        this._ch_filter_form = ChObjectStorage.create(this.getGridForm().closest('div').find('section[data-id=filters]').find('form'), 'ChFilterForm');
    }
    return this._ch_filter_form;
};
ChGridForm.prototype.getParentPK = function () {
//    console.log(this.$form.attr('data-parent-pk'), this._parent_pk)
    if (this._parent_pk == null) {
        this._parent_pk = this.$form.attr('data-parent-pk');
    }
    return this._parent_pk;
};
ChGridForm.prototype.getSearchData = function () {
    var ch_filter_form = this.getFilterForm();
//    if(!ch_filter_form.$form.length){
//        return [];
//    }
    var filter_data = ch_filter_form.getData(),
        parent_id = this.getParentPK();
    if (parent_id) {
        filter_data.ParentID = parent_id;
    }
    return filter_data;
};
ChGridForm.prototype.restoreData = function () {
    this.initData(this.getDataObj(), this.getPreviewObj());
};
ChGridForm.prototype.initData = function (data, preview) {
    var $table = this.getTable();
    if (this._isAttachmentsModel()) {
        var tmpl_data = {'files': data},
            content = window.tmpl('template-download', tmpl_data);
        content = content.replace(new RegExp('fade', 'g'), 'fade in');
        var $html = $(content);
        $table
            .find('tbody').html($html)
            .trigger("update")
    } else {
        var $html = this.generateRows(data);
        $table.find('tbody').html($html);
        console.time('start2')
        ChEditableCallback.fire($table, this.getCallbackID());
        console.timeEnd('start2');
        $table.trigger("update")
    }
};
ChGridForm.prototype.updateData = function (data, preview) {
    this.initData(data, preview);
    this.updateStorage(data, preview);

};
ChGridForm.prototype._clearDeletedObj = function () {
    var deletedObj = this.getDeletedObj();
    deletedObj = {};
};
ChGridForm.prototype._clearChangedObj = function () {
    if (this._isAttachmentsModel()) {
        ChAttachments.clear(this.getID());
    } else {

        var changeObj = this.getChangedObj();
        for (var name in changeObj) {
            changeObj[name] = {};
        }
    }
    this.getSaveButton().removeClass('active')
};
ChGridForm.prototype.refresh = function () {
    console.log('refresh')
//        console.log(this._isAttachmentsModel());
    var url = this.getRefreshUrl(),
        parent_view = this.getParentView(),
        search_data = this.getSearchData(),
        ch_messages_container = this.getMessagesContainer(),
        _this = this;
    $.ajax({
        url: url + '&ParentView=' + parent_view,
        type: "POST",
        data: search_data,
        success: function (response) {
            var ch_response = new ChSearchResponse(response);
            var type = _this.getType();
            if (ch_response.isSuccess()) {
                if (type == 'map') {

                    var $map = _this.$form.children('section').children('.map');
                    /**
                     * @type {ChMap}
                     */
                    var ch_map = ChObjectStorage.create($map, 'ChMap');
                    ch_map.refreshPoints(ch_response.getData(), ch_messages_container);

                } else if (type == 'canvas') {
                    var $canvas = _this.$form.find('canvas');
                    /**
                     * @type {ChCanvas}
                     */
                    var ch_canvas = ChObjectStorage.create($canvas, 'ChCanvas');
                    var data = ch_response.getData();
//                            console.log(data)
                    _this.updateStorage(data, ch_response.getPreview());
                    var options = new ChCanvasOptions();
                    ch_canvas.refreshData(data, options);
                }
                else {

                    _this.updateData(ch_response.getData(), ch_response.getPreview());
                    _this._clearDeletedObj();
                    _this._clearChangedObj();
                }
            }
        },
        error: function (xhr, status, error) {
            ch_messages_container.sendMessage(xhr.responseText, ChResponseStatus.ERROR)
        }
    })
};
/**
 * @returns {jQuery}
 */
ChGridForm.prototype._getUserGrid = function () {
    if (this._$user_grid == null) {
        this._$user_grid = this.$form.children('section[data-id=grid]').find('div[data-id=user-grid]')
    }
    return this._$user_grid;
};
ChGridForm.prototype.getUserGridID = function () {
    if (this._user_grid_id == null) {
        this._user_grid_id = this._getUserGrid().attr('id');
    }
    return this._user_grid_id;
};
ChGridForm.prototype.getID = function () {
    if (this._id == null) {
        this._id = this.$form.attr('id');
    }
    return this._id;
};
ChGridForm.prototype.getView = function () {
    if (this._view_id == null) {
        this._view_id = this.$form.attr("data-id");
    }
    return this._view_id;
};
ChGridForm.prototype.getTabCaption = function () {
    if (this._tab_caption == null) {
        this._tab_caption = this.$form.attr('data-tab-caption');
    }
    return this._tab_caption;
};
ChGridForm.prototype.getTable = function () {
    if (this._$table == null) {
        this._$table = this._getUserGrid().find('table');
    }
    return this._$table;
};
ChGridForm.prototype.getFixedTable = function () {
    if (this._$fixed_table == null) {
        this._$fixed_table = this.$form.find('section[data-id=grid]').find('table.floatThead-table');
    }
    return this._$fixed_table;
};
ChGridForm.prototype.getRefreshUrl = function () {
    if (this._refresh_url == null) {
        this._refresh_url = this.$form.attr('data-refresh-url')
    }
    return this._refresh_url
};
ChGridForm.prototype.getSaveButton = function () {
    if (this._$save_btn == null) {
        this._$save_btn = this.$form.find('menu').children('.menu-button-save');
    }
    return this._$save_btn;
};
ChGridForm.prototype.getParentFormID = function () {
    if (this._parent_form_id == null) {
        var parent_id = this.$form.attr('data-parent-id');
        if (typeof(parent_id) == 'undefined') {
            parent_id = '';
        }
        this._parent_form_id = parent_id;
    }
    return this._parent_form_id
};
/**
 * @returns {ChMessagesContainer}
 */
ChGridForm.prototype.getMessagesContainer = function () {
    if (this._ch_messages_container == null) {
        this._ch_messages_container = ChObjectStorage.create(this.$form.find(".messages-container"), 'ChMessagesContainer');
    }
    return this._ch_messages_container;
};
ChGridForm.prototype.isAjaxAdd = function () {
    if (this._is_ajax_add == null) {
        this._is_ajax_add = this.$form.attr("data-ajax-add");
    }
    return this._is_ajax_add;
};
/**
 * Преобразует коллекцию объектов jQuery в упорядоченный список.
 * @returns {Array}
 * @private
 */
ChGridForm.prototype._getChGridColumns = function () {
    var $fixed_table = this.getFixedTable(),
        $column_headers = $fixed_table.find('thead').eq(0).find('tr').eq(0).find('th'),
        sorting_columns = [],
        count_fixed_column = 1;

    $column_headers.each(function (i) {
        if (i >= count_fixed_column) {
            var ch_column = ChObjectStorage.create($($column_headers[i]), 'ChGridColumnHeader');
            sorting_columns.push(ch_column);
        }
    })
    return sorting_columns;
};
ChGridForm.prototype._getColumnTemplates = function () {
    var columns = this._getChGridColumns(),
        templates = [];
    for (var key in columns) {
        /**
         * @type {ChGridColumnHeader}
         */
        var column = columns[key],
            column_key = column.getKey(),
            template = column.getTemplate();

        templates[column_key] = template;
    }
    return templates;
};
/**
 * Добавляет строку в таблицу и возвращает id добавленной строки;
 * @param data
 * @returns {*}
 */
ChGridForm.prototype.addRow = function (data) {
    var templates = this._getColumnTemplates(),
        $table = this.getTable();

    if (typeof(data['id']) == 'undefined') {
        Chocolate.inserted_row_counter++;
        var id = 'ch_row' + Chocolate.inserted_row_counter;
        data['id'] = id;
    }
    var grid_properties = this.getGridPropertiesObj();
    var $row = $(this.generateRow(templates, data, grid_properties));
    $table
        .find('tbody').prepend($row)
        .trigger('addRows', [$row, false])
    $row.addClass('grid-row-changed');
    //            .trigger("update");
    ChEditableCallback.fire($row, this.getCallbackID());
    var row_id = data['id'],
        change_obj = this.getChangedObj(),
        data_obj = this.getDataObj();
    change_obj[row_id] = jQuery.extend({}, data);
    data_obj[row_id] = {};
    if (this.isAutoOpenCard()) {
        this.openCard(row_id);
    }
    return row_id;
};
ChGridForm.prototype._isAttachmentsModel = function () {
    return this.getView().indexOf(Chocolate.attachment_view) != -1 ;
};
ChGridForm.prototype.generateRow = function (templates, data, grid_properties) {
    var style = '';
    if (grid_properties.colorColumnName != null && typeof(data[grid_properties.colorColumnName]) != 'undefined') {
        var decColor = parseInt(data[grid_properties.colorColumnName], 10),
            hexColor = decColor.toString(16);

        if (hexColor.length < 6) {
            while (hexColor.length < 6) {
                hexColor += '0' + hexColor;
            }
        }
        var R = hexColor.charAt(4) + hexColor.charAt(5),
            G = hexColor.charAt(2) + hexColor.charAt(3),
            B = hexColor.charAt(0) + hexColor.charAt(1);
        style = 'style="background:#' + R + G + B + '"';
    }
    var id_class = '';
    if (grid_properties.colorKey != null && typeof(data[grid_properties.colorKey]) != 'undefined' && data[grid_properties.colorKey] != null && data[grid_properties.colorKey].length > 0) {
        id_class = ' td-red';
    }
    var id = data['id'],
        row = '<tr data-id="' + id + '"' + style + '>' + ChGridForm.TEMPLATE_FIRST_TD,
        user_grid_id = this.getUserGridID(),
        is_numeric_key = Chocolate.isNumeric(id);
    for (var key in templates) {
        var value = '';
        if (typeof(data[key]) != 'undefined' && (key != 'id' || is_numeric_key )) {
            value = data[key];
        }
        var class2 = '';
        if (key == 'id') {
            class2 = id_class;
        }
        var rel = user_grid_id + '_' + key;
        if (value) {

            value = value.replace(/"/g, '&quot;')
        }
        row += templates[key].replace(/\{pk\}/g, id)
            .replace(/\{rel\}/g, rel)
            .replace(/\{value\}/g, value)
            .replace(/\{class2\}/g, class2)
    }
    row += '</tr>'

    return row;
};
ChGridForm.prototype.generateRows = function (data) {
    var templates = this._getColumnTemplates(),
        grid_properties = this.getGridPropertiesObj(),
        count = 0,
        stringBuilder = [];
    for (var i in data) {
        count++;
        if (count > 500) {
            break;
        }
        stringBuilder.push(this.generateRow(templates, data[i], grid_properties));
    }
    return stringBuilder.join('');

};
ChGridForm.prototype.getSelectedRows = function () {
    var rows = [];
    this.getTable().find('.row-selected').each(function () {
        rows.push($(this));
    })
    return rows
};
ChGridForm.prototype.removeRows = function ($rows) {
    var lng = $rows.length;
    var deleted_obj = this.getDeletedObj();
    for (var i = 0; i < lng; i++) {
        deleted_obj[$rows[i].attr('data-id')] = true;
        $rows[i].remove();
    }
    this.getTable().trigger("update");
    this.getSaveButton().addClass('active');
};
ChGridForm.prototype.removeRow = function ($table_cell) {
    var $table = this.getTable(),
        $tr = $table_cell.closest('tr'),
        id = $tr.attr('data-id'),
        deleted_obj = this.getDeletedObj();
    deleted_obj[id] = true;
    $tr.remove();
    $table.trigger("update");
    this.getSaveButton().addClass('active');
};
ChGridForm.prototype.saveInStorage = function (data, preview, default_values, required_fields, grid_properties) {
    var storage = this.getStorage();
    storage[this.getID()] = {data: data, preview: preview, change: {}, deleted: {}, defaultValues: default_values, required: required_fields, gridProperties: grid_properties};
};
ChGridForm.prototype.updateStorage = function (data, preview) {
    var storage = this.getStorage();
    storage[this.getID()].data = data;
    storage[this.getID()].preview = preview;
    storage[this.getID()].change = {};
    storage[this.getID()].deleted = {};
};

ChGridForm.prototype.toggleColls = function(cssClass, $thList){

    var hiddenClass = cssClass,
        positions = [],
        $th = $thList,
        $fixedTable = this.getFixedTable(),
        $table = this.getTable();
    var  tables = [$table.eq(0)[0], $fixedTable.eq(0)[0]];
    var sum = 0;
    var curWidth =$table.width();
    var newWidth;
    $th.each(function(i){
        positions.push($(this).get(0).cellIndex);
        sum+=$(this).outerWidth();
    })
    if($fixedTable.hasClass(hiddenClass)){
        ChTableHelper.showColsManyTables(tables, positions);
        newWidth = curWidth+sum;
    $fixedTable.removeClass(hiddenClass);

    }else{
        ChTableHelper.hideColsManyTables(tables, positions)
        newWidth = curWidth-sum;
    $fixedTable.addClass(hiddenClass);
    }
    $table.width(newWidth)
    $fixedTable.width(newWidth)
    $table.floatThead('reflow');
};

ChGridForm.prototype.toggleAllCols = function(){
    var hiddenClass = ChOptions.classes.hiddenAllColsTable,
        $th = this.getFixedTable().find('['+ChOptions.classes.allowHideColumn+']');
    this.toggleColls(hiddenClass, $th);
};
ChGridForm.prototype.toggleSystemCols = function(){
    var hiddenClass = ChOptions.classes.hiddenSystemColsTable,
        $th = this.getFixedTable().find('th').filter(function(index){
            return $.inArray($(this).attr('data-id'), ChOptions.settings.systemCols) !== -1;
        });
    this.toggleColls(hiddenClass, $th);
};
ChGridForm.prototype.getActiveRow = function(){
  return this.getTable().find('.row-active');
};
