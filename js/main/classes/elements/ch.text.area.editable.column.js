/**
 * @param $elem {jQuery}
 * @constructor
 */
function ChTextAreaEditableColumn($elem) {
    ChEditable.apply(this, arguments);
}
ChTextAreaEditableColumn.prototype = Object.create(ChEditable.prototype);
ChTextAreaEditableColumn.prototype.create = function (context, e, allow_edit, name, caption) {
    if (!allow_edit) {
        $(context).unbind('click')
    }
    var $elem = this.$elem, $cell = $elem.parent(),
        ch_column = new ChGridColumnBody($elem),
        $text_modal = $('<a class="grid-textarea"></a>'),
        _this = this;
    $text_modal.appendTo($cell.closest('section'));
    var $btn = $elem.find('div.grid-modal-open');
    if ($btn.length == 0) {
        $btn = $("<div class='grid-modal-open'></div>").appendTo($cell);

        $btn.on('click', function (e) {
            Chocolate.setFocus();
            if (typeof($text_modal.attr('data-init')) == 'undefined') {
                $text_modal.editable({type: 'textarea', mode: 'popup', onblur: 'ignore', savenochange: false, title: _this.getTitle(ch_column.getID(), caption)});
                $text_modal.on('save', function (e, params) {
                    if (allow_edit) {
                        if (typeof(params.newValue) != 'undefined') {
                            ch_column.setChangedValue(name, params.newValue);
                            $elem.editable("setValue", params.newValue);
                            $text_modal.empty();
                        }
                    } else {
                        return false;
                    }
                })
                $text_modal.attr('data-init', 1);
            }

            var value = $elem.editable('getValue')[name];
            value = value.toString()
            $text_modal.editable('setValue', value);
            $text_modal.editable('show');
            if (!allow_edit) {
                $text_modal.next('div').find('textarea').attr('readonly', 'true')
            }
            e.preventDefault()
            return false
        })
    }

};
