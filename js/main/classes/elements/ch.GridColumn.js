function ChGridColumn($elem ){
    this.$elem = $elem;
}

ChGridColumn.prototype.init = function(e, view, caption, url, editable ){
    editable.disable();
    var ch_column = new ChGridColumnBody(this.$elem),
        parent_id = ch_column.getID(),
        isNew = !Chocolate.isNumeric(parent_id),
        /**
         *
         * @type {ChGridForm}
         */
            ch_form = ChObjectStorage.create(this.$elem.closest('form'), 'ChGridForm'),
        filters_data = {filters:{ParentID: parent_id}},
        parent_view_id = ch_form.getID(),
        parent_view = ch_form.getView(),
        tab_id = Chocolate.createChildGridTabID(parent_id, view, parent_view),
        jTabs = Chocolate.$tabs;
    ;
    this.$elem.parents("td").on("click", function(){
        var template = ch_form.getFmChildGridCollection().getCardTemplate(view, parent_view, isNew);
        var jTab = jTabs.find("[aria-controls=\'"+tab_id+"\']");
        if(jTab.length == 0) {
            var caption2 = caption + ' [' + parent_id + ']';
            Chocolate.addTab(tab_id, caption2);
            var lastTabIndex = jTabs.tabs("length");
            jTabs.tabs({ active: lastTabIndex -1});
            if(template == null){


                $.ajax({
                    method: 'GET',
                    url:  url,
                    cache: false,
                    data: {
                        view: view,
                        jsonFilters:JSON.stringify(filters_data),
                        ParentView: parent_view,
                        parentViewID: parent_view_id
                    },
                    success: function (response) {

                        var ch_response = new ChGridResponse(response);
                        if(ch_response.isSuccess()){
                            var jContainer = $("<div id="+ tab_id+ "></div>")
                            jTabs.append(jContainer)
                            var data =ch_response.getData();
                            jContainer.html(data.replace(Chocolate.PK_KEY_REG_EXP, parent_id))
                            jTabs.tabs("refresh");
                            ch_form.getFmChildGridCollection().setCardTemplate(view, parent_view, data, isNew);
                        }else{
                            alert(ch_response.getStatusMsg);
                        }

                    },
                    error: function (xhr, status, error) {
                        alert(xhr.responseText);
                    }
                });
            }else{
                var data = template.replace(Chocolate.PK_KEY_REG_EXP, parent_id);
                var jContainer = $("<div id="+ tab_id+ "></div>");
                jTabs.append(jContainer);
                jContainer.html(data)
            }
        }else{
            jTabs.tabs("select", tab_id)
        }
    });
}