
/**
 * @param $elem {jQuery}
 * @constructor
 */
function ChEditable($elem) {
    this.$elem = $elem;
}
ChEditable.prototype = {
    getTitle: function (pk, caption) {
        if (Chocolate.isNumeric(pk)) {
            return caption + ' [' + pk + ']'
        } else {
            return caption;
        }
    }
}