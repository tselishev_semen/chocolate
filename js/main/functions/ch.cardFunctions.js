var chCardFunction = {
    select2AjaxInitSelection: function(element, callback, sql){
        //TODO: не сделана подствека анимации в случаи долгой загрузки
        var $elem =  element.data().select2.opts.element.closest('.table-td').children('a'),
            elem = $elem.get(0),
            data = jQuery.data(elem,'data-loaded'),
            cardElement = new ChCardElement($elem),
            bindSql = bindingService.bindFromData(sql, cardElement.getCard().getDataObj());

        if(typeof(data) =='undefined'){
            data = [];
            $.ajax(
                MajesticVars.EXECUTE_URL,
                {
                    async: false,
                    data: {sql: bindSql},
                    success: function(reponse){
                        var chResponse = new ChGridResponse(reponse),
                            resData =  chResponse.getData();
                        for(var i in resData){
                            data.push({
                                id: resData[i].id,
                                text:resData[i].data.name
                            });
                        }
                        jQuery.data(elem,'data-loaded', data)
                    },
                    error: function(){
                        jQuery.data(elem,'data-loaded', data)
                    }
                }
            );
        }
        var result = [];
        $(element.val().split(",")).each(function () {
            var id = this;
            if (id.length) {
                var text = "";
                $(data).each(function () {
                    if (this.id == id) {
                        text = this.text;
                        return false;
                    }
                });
                result.push({id: id, text: text});
            }
        });
        callback(result);



    },
    select2AjaxDataFunc: function(query, $context){
        var elem = $context[0].element.closest('.table-td').children('a').get(0),
            data = {results: []};

        $.each(jQuery.data(elem,'data-loaded'), function(){
            if(query.term.length == 0 || this.text.toUpperCase().indexOf(query.term.toUpperCase()) >= 0 ){
                data.results.push({id: this.id, text: this.text });
            }
        });
        query.callback(data);
    },
    defaultValidateFunc: function ($context, value) {
        var $error = $context.closest(".card-col").children("label");
        if ($.trim(value) == "") {
            $error.addClass("card-error");
        } else {
            $error.removeClass("card-error");
        }
    },
    select2ColumnSaveFunction: function (e, params, name) {
        var jCell = $(e.target),
            chColumn = new ChGridColumnBody(jCell),
            new_value = "";
        if (params.newValue.length > 0 && params.newValue[0].indexOf(",") != -1) {
            //отсекаем первый элемент, тк это какой- то левак
            delete params.newValue[0];
        }
        for (var i in params.newValue) {
            new_value += params.newValue[i] + "|";
        }

        jCell.editable("setValue", params.newValue);
        chColumn.setChangedValue(name, new_value);
    },
    select2ColumnInitFunction: function (element, callback) {
        var data = [],
            sql_data = element.data().select2.opts.data;
        $(element.val().split(",")).each(function (i) {
            if (this != "") {
                var option_name = '';
                for (var i in sql_data) {
                    var el = sql_data[i];
                    if (el.id == this) {
                        option_name = el.text;
                        break;
                    }
                }
                data.push({id: this, text: option_name });
            }
        });

        callback(data);
    },
    defaultSaveFunc: function (e, params, name) {
        var $target = $(e.target);
        /**
         *
         * @type {ChCardElement}
         */
        ChObjectStorage.create($target, 'ChCardElement')
            .setChangedValue(name, params.newValue)
            .setChangedValueInGrid(name, params.newValue, $target.text());
    },
    dateSaveFunc: function (e, params, name) {
        var $target = $(e.target);
        /**
         *
         * @type {ChCardElement}
         */
        var dtValue = params.newValue.format(ChOptions.settings.formatDate);
        ChObjectStorage.create($target, 'ChCardElement')
            .setChangedValue(name, dtValue)
            .setChangedValueInGrid(name, params.newValue);
    },
    textAreaInitFunc: function (e, editable, attribute, isAllowEdit, caption, isNeedFormat, context) {
        var jCell = $(context);
        /**
         *
         * @type {ChCardElement}
         */
        var chCardElement = ChObjectStorage.create(jCell, 'ChCardElement'),
            chCard = chCardElement.getCard(),
            value = chCard.getActualDataObj()[attribute];
        if (isNeedFormat && value) {
            value = Chocolate.formatNumber(value);
        }
//        console.log(value, value ===null)
        if (value === null) {
            value = '';
        }
        ChCardInitCallback.add(function () {
            chCard.setElementValue(jCell, value, isAllowEdit);
            var ch_column = new ChTextAreaEditableCard(editable['$element']);
            ch_column.create(context, e, isAllowEdit, attribute, caption, isNeedFormat);
        });
    },
    checkBoxDisplayFunction: function (value, $context) {
        if (typeof(value) != 'undefined' && value) {
            $context.text('да');
        } else {
            $context.text('нет');
        }
    },
    checkBoxInitFunction: function ($context, attribute, isAllowEdit) {
        /**
         *
         * @type {ChCardElement}
         */
        var chCardElement = ChObjectStorage.create($context, 'ChCardElement');
        var chCard = chCardElement.getCard(),
            value = chCard.getActualDataObj()[attribute];
        $context.unbind('click');
        if (isAllowEdit) {
            $context.on('click', function () {
                var val = $context.editable('getValue');
                if ($.isEmptyObject(val)) {
                    val = 1
                } else {
                    val = +!val[attribute]
                }
                $context.editable('setValue', val);
                chCardElement
                    .setChangedValue(attribute, val)
                    .setChangedValueInGrid(attribute, val, $context.text());
            })
        }
        ChCardInitCallback.add(function () {
            chCard.setElementValue($context, value, isAllowEdit);
        })
        ;
    },
    dateInitFunction: function ($context, attribute, isAllowEdit) {
        /**
         *
         * @type {ChCardElement}
         */
        var chCardElement = ChObjectStorage.create($context, 'ChCardElement');
        var chCard = chCardElement.getCard(),
            value = chCard.getActualDataObj()[attribute],
            dtValue;

        if (value && typeof(value) == 'string') {
            value = value.replace(/\./g, '/')
//            var ss = new Date(value);
//             dtValue = new Date(ss.format('yyyy.mm.dd'));
            dtValue = new Date(value);
//            console.log(value, dtValue, ss)
        } else {
            dtValue = value;
        }
//        console.log(dtValue, attribute)
        ChCardInitCallback.add(function () {
            chCard.setElementValue($context, dtValue, isAllowEdit);
        })

    },
    select2SaveFunction: function (e, params, attribute) {

        var $context = $(e.target);
        /**
         *
         * @type {ChCardElement}
         */
        var chCardElement = ChObjectStorage.create($context, 'ChCardElement');

        var new_value = "";
        for (var i in params.newValue) {
            if (params.newValue[i]) {
                new_value += params.newValue[i] + "|";
            }
        }
        $context.editable('setValue', new_value);

        chCardElement
            .setChangedValue(attribute, new_value)
            .setChangedValueInGrid(attribute, params.newValue, $context.text());
    },
    select2GetParentDataSource: function (element, name) {
        var chCardElement = ChObjectStorage.create(element, "ChCardElement");
        return chCardElement.getParentElement(name).data().editable.options.source;
    },
    select2GetDataSource: function (formID, pk, name) {
        return $('#' + formID).find("a[data-pk=" + pk + "][rel$=" + name + "]").data().editable.options.source;
    },
    select2InitSelectionFunction: function (element, callback, name) {
        var parentData = chCardFunction.select2GetParentDataSource(element, name);
        var data = [];
        $(element.val().split(",")).each(function () {
            var id = this;
            if (id.length) {
                var text = "";
                $(parentData).each(function () {
                    if (this.id == id) {
                        text = this.text;
                        return false;
                    }
                });
                data.push({id: id, text: text});
            }
        });
        callback(data);
    },
    select2ShownFunction: function (editable) {
        if (typeof(editable) != "undefined") {
            editable.formOptions.input.postrender = function () {
                editable.$form.find(".select2-search-field input").focus()
            }
        }
    },
    select2SortFunction: function (results, container, query) {
        if (term != "") {
            var term = query.term.toLowerCase();
            results = results.sort(function (a, b) {
                if (a.text.toLowerCase().indexOf(term) == 0) {
                    return -1;
                }
                if (b.text.toLowerCase().indexOf(term) == 0) {
                    return 1
                }
                return 0;
            })
        }
        return results;
    },
    select2InitFunction: function ($context, attribute, isAllowEdit, titleKey, editable, caption) {
        /**
         *
         * @type {ChCardElement}
         */
        var chCardElement = ChObjectStorage.create($context, 'ChCardElement');
        var chCard = chCardElement.getCard(),
            actualDataObj = chCard.getActualDataObj(),
            changeObj = chCard.getChangedObj(),
            value = actualDataObj[attribute],
            html;
        if (typeof(value) == 'undefined' || value === null) {
            value = '';
        }
        var prepareValue = value.split('|');
        if (typeof(changeObj) == 'undefined') {
            html = actualDataObj[titleKey];
        } else {
            var data = chCardFunction.select2GetParentDataSource($context, attribute);
            html = '';
            $(data).each(function () {
                if ($.inArray(this.id, prepareValue) != -1) {
                    html += this.text + '/';
                    return false;
                }
            });
        }
        ChCardInitCallback.add(function () {
            chCard.setElementValue($context, prepareValue, isAllowEdit, html);
            chFunctions.select2ColumnInitFunc($context, caption )
        });
    },
    selectInitFunction: function ($context, attribute, isAllowEdit) {
        /**
         *
         * @type {ChCardElement}
         */
        var chCardElement = ChObjectStorage.create($context, 'ChCardElement');
        var chCard = chCardElement.getCard(),
            value = chCard.getActualDataObj()[attribute];

        ChCardInitCallback.add(function () {
            chCard.setElementValue($context, value, isAllowEdit);
        });
    },
    multimediaInitFunction: function (pk, sql, formID, url, id) {
        /**
         *
         * @type {ChGridForm}
         */
        var chGridForm = ChObjectStorage.create($('#' + formID), 'ChGridForm'),
            sql = bindingService.binFromParentData(sql, chGridForm.getDataObj()[pk]);
        $.get(MajesticVars.EXECUTE_URL, {sql: sql})
            .done(function (response) {
                var chResponse = new ChResponse(response);
                var data = chResponse.getData();
                if (data.length) {
                    var $context = $('#' + id), imagesHtml = '';
                    for (var i in data) {
                        var imageUrl = url + '?fileID=' + data[i].id;
                        if (i == 0) {
                            imagesHtml += '<a class="fancybox multimedia-main-image" rel="gallery"><img src="' + imageUrl + '"></img></a>';
                        } else {
                            imagesHtml += '<a class="fancybox multimedia-image" rel="gallery" style="display:none"><img src="' + imageUrl + '"></img></a>';
                        }
                    }
                    $context.append(imagesHtml)
                    $context.find('.fancybox').fancybox({
                        live: false,
                        maxWidth: 800,
                        maxHeight: 600,
                        closeClick: false,
                        openEffect: 'none',
                        closeEffect: 'none'
                    });
                }
            })
            //TODO: обработчик ошибки при получении данных
            .fail(function () {
            })

    }
}