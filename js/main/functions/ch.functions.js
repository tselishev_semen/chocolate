var chFunctions = {
//    validateColumn: function (value) {
//        if ($.trim(value) == "") {
//            return "Это поле обязательно для заполнения";
//        }
//
//    },
    select2ColumnInitFunc: function($context, caption){
        var chEditable = new ChEditable($context);
       $context.attr('data-original-title', chEditable.getTitle($context.attr('data-pk'), caption));

    },
    selectColumnInitFunc: function($context, isAllowEdit){
        if(!isAllowEdit){ $context.unbind('click')}
    },
    defaultColumnSaveFunc: function(e, params, name){
        var chColumn = new ChGridColumnBody($(e.target));
        chColumn.setChangedValue(name, params.newValue);
    },
    dateColumnInitFunction:function($context, isAllowEdit){
        if(!isAllowEdit){ $context.unbind('click').unbind('mouseenter')}
    },
    dateColumnSaveFunction: function(e, params, name){
        var chColumn = new ChGridColumnBody($(e.target));
        chColumn.setChangedValue(name, params.newValue.format(ChOptions.settings.formatDate));
    },
    treeOnQuerySelect: function (flag, node) {
        if (node.childList == null) {
            return true;
        } else {
            for (var i in node.childList) {
                node.childList[i].select(flag);
            }
        }
        return true
    },
    initPrintActions: function (id, jsonPrintActions) {
        var $actionButton = $('#' + id);
        /**
         *
         * @type {ChGridForm}
         */
        var chForm = ChObjectStorage.create($actionButton.closest('form'), 'ChGridForm'),
            rexExp = new RegExp('\[IdList\]');

        $actionButton.contextmenu({
            show: { effect: "blind", duration: 0 },
            menu: json_parse(jsonPrintActions),
            select: function (event, ui) {
                var url = ui.cmd;
                if (rexExp.test(url)) {
                    var idList = '',
                        rows = chForm.getSelectedRows(),
                        lng = rows.length;
                    for (var i = 0; i < lng; i++) {
                        idList += rows[i].attr('data-id') + ' ';
                    }
                    url = url.replace(/\[IdList\]/g, idList);
                }
                window.open(url);
            }
        })
    },
    initActions: function (id, jsonActions) {
        var $actionButton = $('#' + id);
        /**
         *
         * @type {ChGridForm}
         */
        var chForm = ChObjectStorage.create($actionButton.closest('form'), 'ChGridForm');
        $actionButton.contextmenu({
            show: { effect: "blind", duration: 0 },
            menu: json_parse(jsonActions),
            select: function (event, ui) {
                switch (ui.cmd) {
                    case 'window.print':
                        window.print();
                        break;
                    case 'ch.export2excel':
                        var data = {
                            data: $.extend(true, chForm.getDataObj(), chForm.getChangedObj()),
                            view: chForm.getView(),
                            settings: chForm.getSettingsObj()
                        };
                        $.fileDownload(
                            ChOptions.urls.export2excel,
                            {
                                httpMethod: "POST",
                                data: data
                            }
                        );
                        break;
                    case 'ch.settings':
                        var $dialog = $('<div></div>'),
                            $content = $('<div class="grid-settings"></div>'),
                            $autoUpdate = $('<div class="setting-item"><span class="setting-caption">Автоматические обновление данных(раз в 100 секунд)</span></div>'),
                            $input = $('<input type="checkbox">')
                        /**
                         *
                         * @type {ChFormSettings}
                         */
                        var chFormSettings = chForm.ch_form_settings;
                        if (chFormSettings.isAutoUpdate()) {
                            $input.attr('checked', 'checked');
                        }

                        $autoUpdate.append($input);
                        $content.append($autoUpdate)
                        $dialog.append($content)
                        $dialog.dialog({
                            resizable: false,
                            title: 'Настройки',
                            dialogClass: 'wizard-dialog',
                            modal: true,
//                            height: 500,
//                            width: 700,
                            buttons: {
                                OK: {
                                    text: 'OK',
                                    class: 'wizard-active',
                                    click: function (bt, elem) {
                                        chFormSettings.setAutoUpdate($input.is(':checked'));
                                        $(this).dialog("close");
                                        $(this).remove();
                                    }},
                                Отмена: {
                                    text: 'Отмена',
                                    class: 'wizard-cancel-button',
                                    click: function () {
                                        $(this).dialog("close");
                                        $(this).remove();
                                    }
                                }

                            }
                        });
                        $dialog.dialog('open')
                        break;

                }
            }
        });
    },
    initGrid: function (jsonData, jsonPreview, jsonDefault, jsonRequired, jsonGridProperties, formID, header, headerImg, jsonCardCollection) {
        /**
         * @type {ChGridForm}
         */
        var chForm = ChObjectStorage.create($('#' + formID), 'ChGridForm');
        chForm.saveInStorage(
            json_parse(jsonData, Chocolate.parse),
            json_parse(jsonPreview),
            json_parse(jsonDefault),
            json_parse(jsonRequired),
            json_parse(jsonGridProperties)
        );
        chForm.setFmCardsCollection(
            new FmCardsCollection(header, headerImg, json_parse(jsonCardCollection, Chocolate.parse))
        );
    },
    initCardGrid: function (jsonDefault, jsonRequired, jsonGridProperties, formID, header, headerImg, jsonCardCollection, view, parentView, parentID, sql) {
        /**
         * @type {ChGridForm}
         */
        var chForm = ChObjectStorage.create($('#' + formID), 'ChGridForm');
        chForm.saveInStorage(
            {},
            {},
            json_parse(jsonDefault),
            json_parse(jsonRequired),
            json_parse(jsonGridProperties)
        );

        chForm.setFmCardsCollection(
            new FmCardsCollection(header, headerImg, json_parse(jsonCardCollection, Chocolate.parse))
        );

//        var url =ChOptions.urls.gridExecute + '?view='+encodeURIComponent(view)
//            + '&parentView='+ encodeURIComponent(parentView)
//            + '&parentID=' + parentID;
        var params = {
            sql: sql,
            view: view,
            parentView: parentView,
            parentID: parentID
        };
        var ajaxTask = new ChAjaxTaks(formID, 'ChGridForm', params);
        chAjaxQueue.enqueue(ajaxTask);
//        var data ={sql:sql};
//        $.post(url, data).done(function(response){
//            var ch_response = new ChSearchResponse(response);
//            chForm.updateData(ch_response.getData(), ch_response.getPreview());
//        })
    }
}