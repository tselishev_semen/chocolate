
/**
 * Объект, сохраняющий функции обратного вызова при инициализации карточки. После вызова - очищается
 */
var ChCardInitCallback = {
    callbacks: $.Callbacks(''),
    fireOnce: function ($context) {
        this.callbacks.fire($context);
        this._clear();
    },
    _clear: function () {
        this.callbacks.empty();
    },
    add: function (callback) {
        this.callbacks.add(callback);
    }
};
