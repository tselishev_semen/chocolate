var ChocolateEvents = {
    createEventsHandlers: function () {
        this.initAjaxAnimation();
        this.initCloseTabs();
        this.initGridActions();
        this.initOpeningCardColumn();
        this.initSelectableRow();
        this.initUpdateAttachment();
        this.initDefaultGridMenu();
        this.initAttachmentGridMenu();
        this.initCardCancel();
        this.initAddRowButton();
        this.initSaveGridButton();
        this.initRefreshGridButton();
        this.initCardSaveButton();
        this.initTabActivate();
        this.initResizeWindow();
        this.initTaskWizard();
        this.initCopyright();
        this.disableFilters();
        this.filterOnEnter();
        this.initMenuItems();
        this.downloadAttachments();
        this.initToggleSystemCols();

    },
    initToggleSystemCols: function(){
        Chocolate.$tabs.on('click', '.menu-button-toggle',function() {
            ChObjectStorage.create($(this).closest('form'), 'ChGridForm').toggleSystemCols();
        })
    },
    downloadAttachments: function () {
        Chocolate.$content.on('click', '.attachment-name a', function () {
            return false;
        });
        var download = function download() {
            window.open($(this).attr('href'), '_self');
            return false;

        }
        Chocolate.$content.on('click', '.attachment-name a', $.debounce(2000, true, download));
    },
    initMenuItems: function () {
        $('#footer').on('click', '.link a', function (e) {
            Chocolate.openForm($(this).attr('href'));
            e.preventDefault();
        })
        $('#header').on('click', 'a', function (e) {
            var url = $(this).attr('href');
            if (url != '#') {
                $.post(url, []).done(function (response) {
                    Chocolate.$tabs.append(response)
                })
            }
            e.preventDefault();
        });
    },
    filterOnEnter: function () {
        Chocolate.$content.on('keydown', 'input[type=text].filter', function (e) {
            var enterCode = 13;
            if (e.keyCode == enterCode) {
                /**
                 *
                 * @type {ChGridForm}
                 */
                var ch_form = ChObjectStorage.create($(this).closest('.section-filters').next('.section-grid').children('form'), 'ChGridForm')
                ch_form.refresh();
            }
        })
    },
    initResizeWindow: function () {
        Chocolate.$window.on('resize', $.debounce(300, false, function () {
            ChocolateDraw.clearReflowedTabs();
            ChocolateDraw.reflowTab(ChObjectStorage.create(Chocolate.getActiveTabObj(), 'ChTab'));
        }));
    },
    initTabActivate: function () {
        Chocolate.$tabs.on('click', 'ul.ui-tabs-nav>li>a', function () {
            ChocolateDraw.reflowTab(ChObjectStorage.create(Chocolate.getActiveTabObj(), 'ChTab'));
        });
    },
    initAjaxAnimation: function () {
        var $spinner = $('#fadingBarsG');
        $(document).ajaxStart(function () {
                if (!$spinner.is(':visible')) {
                    $spinner.show();
                }
            }
        )
            .ajaxStop(function () {
                $spinner.hide();
            })
            .ajaxError(function (e) {
                $spinner.hide();
            })
    },
    initCloseTabs: function () {
        Chocolate.$tabs.on('click', '.tab-closed', function () {
            Chocolate.closeTab($(this))
        });
    },
    initGridActions: function () {
        Chocolate.$tabs.on('click', '.menu-button-print, .menu-button-action', function (e) {
//            console.log('click to open')
            $(this).contextmenu('open', $(this))
        })
    },
    initOpeningCardColumn: function () {
        Chocolate.$tabs.on('dblclick', 'span[data-id=card-button]', function (e) {
            var $cell = $(this),
//                ch_form = new ChGridForm($cell.closest('form')),
                ch_form = ChObjectStorage.create($cell.closest('form'), 'ChGridForm'),
                ch_column = new ChGridColumnBody($cell);
            ch_form.openCard(ch_column.getID());
        });
    },
    initSelectableRow: function () {
        Chocolate.$tabs.on('click', 'tr', function (e) {
                var chForm = ChObjectStorage.create($(this).closest('form'), 'ChGridForm');
            chForm.selectRow($(this), e.ctrlKey || e.shiftKey)

        });
        Chocolate.$tabs.on('keydown', '.tablesorter', function (e) {
                var upKey = 38, downKey = 40, delKey = 46;
            if(e.keyCode == delKey){
                /**
                 *
                 * @type {ChGridForm}
                 */
                var chForm = ChObjectStorage.create($(this).closest('form'), 'ChGridForm');
                chForm.removeRows(chForm.getSelectedRows());
                return false;
            }
            if((e.ctrlKey || e.shiftKey) &&(e.keyCode == upKey || e.keyCode == downKey)){
                /**
                 *
                 * @type {ChGridForm}
                 */
            var chForm = ChObjectStorage.create($(this).closest('form'), 'ChGridForm');
            var $activeRow = chForm.getActiveRow(),
                $nextRow;
                if(e.keyCode == downKey){
                    $nextRow = $activeRow.next();
                }else{
                    $nextRow = $activeRow.prev();
                }
                if($nextRow.length){
                    chForm.selectRow($nextRow, true);
                }
            }

        })
    },
    initUpdateAttachment: function () {
        $(document).on('change', 'input[data-input-id=chocolate-upload-hidden]', function (e) {
            if (typeof($(e.target).attr("parent-id")) == "undefined" || $(e.target).attr("parent-id") == '') {
                Chocolate.inserted_row_counter++;
                $(e.target).attr("parent-id", 'chocolate_' + Chocolate.inserted_row_counter);
            }
            Chocolate.updateAttachment($(e.target))
        })
    },
    initDefaultGridMenu: function () {
        Chocolate.$tabs.contextmenu({
            delegate: "span.card-button",
            show: { effect: "blind", duration: 0 },
            menu: [
//                {title: "Открыть карточку", cmd: "open-card", uiIcon: "ui-icon-newwin"},
                {title: "Удалить", cmd: "delete", uiIcon: "ui-icon-trash"}
            ],
            select: function (event, ui) {
                var $target = ui.target
                switch (ui.cmd) {
                    case 'delete':
                        var ch_form = ChObjectStorage.create($target.closest('form'), 'ChGridForm');
//                            new ChGridForm($target.closest('form'));

                        ch_form.removeRows(ch_form.getSelectedRows());
                        break;
//                    case 'open-card':
//                        var ch_form = ChObjectStorage.create($target.closest('form'), 'ChGridForm'),
//                            ch_column = new ChGridColumnBody($target);
////                            ch_form = new ChGridForm($target.closest('form')),
//                        ch_form.openCard(ch_column.getID());
//                        break;
                    default :
                        alert('неизвестная команда')
                        break
                }
            }
        });
    },
    initAttachmentGridMenu: function () {
        $(document).contextmenu({
            delegate: "td.attachment-grid-menu",
            show: { effect: "blind", duration: 0 },
            menu: [
                {title: "Обновить вложение", cmd: "attachment-update", uiIcon: "ui-icon-newwin"},
                {title: "Удалить", cmd: "delete", uiIcon: "ui-icon-trash"}

            ],
            select: function (event, ui) {
                var $target = ui.target
                switch (ui.cmd) {
                    case 'delete':
//                        var ch_form = new ChGridForm($target.closest('form'));
                        var ch_form = ChObjectStorage.create($target.closest('form'), 'ChGridForm');
                        ch_form.removeRow($target);
                        break;
                    case 'attachment-update':
                        var tr = $target.closest('tr')
                        var update_button = tr.find('input[data-input-id=chocolate-upload-hidden]')
                        update_button.trigger('click')
                        break;
                    default :
                        alert('неизвестная команда')
                        break
                }
            }
        });
    },
    initCardCancel: function () {
        Chocolate.$tabs.on('click', 'input[data-id=card-cancel]', function () {
            var ch_card = ChObjectStorage.create($(this).closest('div[data-id=grid-tabs]'), 'ChCard');
//                new ChCard($(this).closest('div[data-id=grid-tabs]'));
            ch_card.undoChange();
        })
    },
    initAddRowButton: function () {
        Chocolate.$tabs.on('click', '.menu-button-add', function (e) {
            var ch_form = ChObjectStorage.create($(this).closest('form'), 'ChGridForm');
            if (ch_form.isAjaxAdd()) {
                var url = ChOptions.urls.addRow,
                    view = ch_form.getView();
                $.ajax({
                    method: "GET",
                    url: url,
                    cache: false,
                    data: {view: view},
                    success: function (response) {
                        //TODO: возможно существуют ошибки, не проверялось
                        var ch_response = new ChResponse(response);
                        if (ch_response.isSuccess()) {
                            ch_form.addRow(ch_response.getData());
                        } else {
                            ch_response.sendMessage(ch_form.getMessagesContainer());
                        }
                    },
                    error: function (xhr, status, error) {
                        ch_form.getMessagesContainer().sendMessage(xhr.responseText, ChResponseStatus.ERROR);
                    }
                })
            } else {
                var form_id = ch_form.getID(),
                    data = jQuery.extend({}, ch_form.getDefaultObj()),
                    row_id = ch_form.addRow(data);

            }
        })
    },
    initSaveGridButton: function () {
        Chocolate.$tabs.on('click', '.menu-button-save', function () {
            var ch_form = ChObjectStorage.create($(this).closest('form'), 'ChGridForm');
//                new ChGridForm($(this).closest('form'));
            ch_form.save();
        });
    },
    initCardSaveButton: function () {
        Chocolate.$tabs.on('click', '.card-save', function (e) {

            Chocolate.setFocus();
            /**
             *
             * @type {ChCard}
             */
            var chCard = ChObjectStorage.create($(this).closest('div[data-id=grid-tabs]'), 'ChCard');
            chCard.save();
        })
    },
    initRefreshGridButton: function () {
        Chocolate.$tabs.on('click', '.menu-button-refresh', function (e) {
            var chForm = ChObjectStorage.create($(this).closest('form'), 'ChGridForm');
            if (chForm.isHasChange()) {
                var $dialog = $('<div>Сохранить изменения?</div>');
                $dialog.dialog({
                    title: 'Апельсин',
                    dialogClass: 'wizard-dialog',
                    resizable: false,
                    height: 140,
                    modal: true,
                    buttons: {
                        'Да': function () {
                            $(this).dialog("close");
                            chForm.save();
                        },
                        'Нет': function () {
                            $(this).dialog("close");
                            chForm.refresh();
                        },
                        'Отмена': function () {
                            $(this).dialog("close");
                        }
                    },
                    create:function () {
                        var $buttons =$(this).siblings('div').find("button")
                            $buttons.first()
                            .addClass("wizard-next-button")
                                .nextAll().
                                addClass('wizard-cancel-button');
                    }
                });
            } else {
                chForm.refresh();
            }
        });

    },
    initTaskWizard: function () {
        $('body').on('click', 'section[data-id=header] a[href$=TasksWizard]', function () {
//            var ch_form = new ChGridForm($(this).closest('section').parent().find("section[data-id=grid-form]>form"));
            var ch_from = ChObjectStorage.create($(this).closest('section').parent().find("section[data-id=grid-form]>form"), 'ChGridForm');
//                new ChGridForm($(this).closest('section').parent().find("section[data-id=grid-form]>form"));
            var service_step = new SelectServiceTaskStep(),
                executor_step = new SelectExecutorsTaskStep(),
                description_step = new SelectDescriptionTaskStep(),
                task_wizard = new TaskWizard(ch_from);
            task_wizard.enqueue(service_step);
            task_wizard.enqueue(executor_step);
            task_wizard.enqueue(description_step);
            task_wizard.run();
            return false;
        })
    },
    initCopyright: function () {
        $(window).on('keydown', function (e) {
            var escape_key = 8;
            if (e.keyCode == escape_key && !(e.target.tagName == 'INPUT' || e.target.tagName == 'TEXTAREA')) {
                e.preventDefault();
            }
        })
        $('body').on('keydown', 'textarea', function (e) {

            var f4_code = 115;
            if (e.keyCode == f4_code) {
                var $elem = $(e.target),
                    date = new Date(),
                    copyright = ' ' + Chocolate.getUserName() + ' ' + date.format("dd.mm.yyyy HH:MM") + ' ';
                $elem.insertAtCaret(copyright);
                e.preventDefault();
                return false
            }
        })
    },
    disableFilters: function () {
        Chocolate.$content.on('click', '.section-filters div>label', function (e) {
            $(this).siblings('select, input').prop("disabled", function (i, val) {
                return !val;
            });
        })
    }
}
