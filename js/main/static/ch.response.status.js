/**
 * Статусы ответов с шоколада (Response.php)
 */
var ChResponseStatus = {
    SUCCESS: 0,
    ERROR: 1,
    WARNING: 2
};