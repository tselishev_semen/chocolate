var Chocolate = {
    storage: new ObjectStorage(),
    getActiveTabObj: function () {
        return Chocolate.$tabs.children('ul').children('li.ui-tabs-active').children('a');
    },
    isNumeric: function (string) {
        return isFinite(+string);
    },
    setFocus: function () {
        Chocolate.$content.trigger('click');
    },
    _initStorage: function () {
        var settings = {};
        var grid_settings = {};
        if (typeof(this.storage.local.settings) != 'undefined') {
            settings = $.extend({}, this.storage.local.settings);
        }
        if (typeof(this.storage.local.grid_settings) != 'undefined') {
            grid_settings = $.extend({}, this.storage.local.grid_settings);
        }
        this.storage.session = {};
        this.storage.local.settings = settings;
        this.storage.local.grid_settings = grid_settings;
    },
    $window: null,
    $tabs: null,
    $header: null,
    $footer: null,
    $pagewrap: null,
    $content: null,
    formatNumber: function(number){
        return number.replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g, "\$1 ");
    },
    init: function () {
        this.$window = $(window);
        this.$tabs = $('#tabs');
        this.$header = $('#header');
        this.$footer = $('#footer');
        this.$content = $('#content');
        this.$pagewrap = $('#pagewrap');
//        this.storage = new ObjectStorage();
        this._initStorage()
    },
    eng2rus: function (str) {
        replacer = {
            "q": "й", "w": "ц", "e": "у", "r": "к", "t": "е", "y": "н", "u": "г",
            "i": "ш", "o": "щ", "p": "з", "[": "х", "]": "ъ", "a": "ф", "s": "ы",
            "d": "в", "f": "а", "g": "п", "h": "р", "j": "о", "k": "л", "l": "д",
            ";": "ж", "'": "э", "z": "я", "x": "ч", "c": "с", "v": "м", "b": "и",
            "n": "т", "m": "ь", ",": "б", ".": "ю", "/": "."
        };
        for (i = 0; i < str.length; i++) {
            if (replacer[ str[i].toLowerCase() ] != undefined) {
                if (str[i] == str[i].toLowerCase()) {
                    replace = replacer[ str[i].toLowerCase() ];
                } else if (str[i] == str[i].toUpperCase()) {
                    replace = replacer[ str[i].toLowerCase() ].toUpperCase();
                }
                str = str.replace(str[i], replace);
            }
        }
        return str;

    },
    PK_KEY_REG_EXP: /00pk00/g,
    attachment_view: 'attachments.xml',
    inserted_row_counter: 1,
    inserted_tab_counter: 1,
    addTab: function (href, name) {
        Chocolate.inserted_tab_counter++;
        var id = '_tab' + Chocolate.inserted_tab_counter;
        var template_link = '<a id="' + id + '" href="#{href}"><span>{name}</span></a><span class="tab-closed fa fa-times" role="presentation"></span>',
            $tab = $('<li>' + template_link.replace(/\{href\}/g, href).replace(/\{name\}/g, name) + '</li>');
//        Chocolate.inserted_tab_counter++;
        Chocolate.$tabs.children('ul').append($tab)
        Chocolate.$tabs.tabs()
        Chocolate.$tabs.tabs('refresh');
        return $tab;
    },
    getNewID: function(){
        Chocolate.inserted_tab_counter++ ;
        return 'choco' + Chocolate.inserted_tab_counter;
    },
    _generateTabID: function (id, view) {
        return 'card_' + view + '_' + id;
    },
    addTabAndSetActive: function(id, name){
            var tab_index = Chocolate.addTab(id, name).index(),
                $tabs = $('#tabs');
            $tabs.tabs({ active: tab_index })
            $tabs.tabs("refresh");

    },
    initTabs: function ($context) {
        $context.attr('data-type', 'chocolate-card');
        $context.find('div[data-id=grid-tabs]').tabs({
//            load: function (event, ui) {
////                console.log('load')
//                ChCardInitCallback.fireOnce();
//            },
            beforeLoad: function (event, ui) {
                var card = $(this)
//                console.log('before load')
                var li_id = card.parent("div").attr("id")
                var viewID = $("li[aria-controls=" + li_id + "]").attr("data-view-id")
                var tabID = $(ui.tab).attr('data-id');
                var view = card.attr("data-view");
                var pk = card.attr("data-pk");

//                ui.ajaxSettings.url = card.attr("data-url");
//                ui.ajaxSettings.url += "&pk=" + pk + "&tabID=" + encodeURIComponent(tabID) + "&viewID=" + viewID;

                /**
                 * Предотвращает обновление данных, когда данные уже закешированы
                 */
                if (ui.tab.data("loaded")) {

                } else {
                    /**
                     *
                     * @type {ChCard}
                     */
                    var chCard = ChObjectStorage.create(card,'ChCard')
                    var fmCardCollection = chCard.getGridForm().getFmCardsCollection();
                    var isNumeric = +!Chocolate.isNumeric(pk);
                    var template = fmCardCollection.getCardTemplate(tabID, isNumeric);
                    if(template == null){

                        $.get(
                                card.attr("data-url")+ "&tabID=" + encodeURIComponent(tabID) + "&viewID=" + viewID + "&pk=" + pk
                    ).done(function(data){
                                fmCardCollection.setCardTemplate(tabID, data, isNumeric)
//                                console.log(data)
                                data = data.replace(Chocolate.PK_KEY_REG_EXP, pk)
                                ui.panel.html(data)
                                ChCardInitCallback.fireOnce();
                                ChocolateDraw.drawCardPanel(ui.panel, $context)
                                /**
                                 *
                                 * @type {ChTab}
                                 */
                                var chTab =ChObjectStorage.create(Chocolate.getActiveTabObj(), 'ChTab')
                                ChocolateDraw.reflowTab(chTab)
                                ui.tab.data("loaded", 1)
                            })
                    }else{
                        var data = template.replace(Chocolate.PK_KEY_REG_EXP, pk)
//                        console.log(data)
                        ui.panel.html(data)
                        ChCardInitCallback.fireOnce();
                        ChocolateDraw.drawCardPanel(ui.panel, $context)

                        /**
                         *
                         * @type {ChTab}
                         */
                        var chTab =ChObjectStorage.create(Chocolate.getActiveTabObj(), 'ChTab')
                        ChocolateDraw.reflowTab(chTab)
                        ui.tab.data("loaded", 1)


                    }

                }


                event.preventDefault();
                return;


            },
            cache: true
        });
    },
    closeTab: function ($target) {
        var li = $target.closest("li");
        var tab_index = li.attr('tabindex');
        var panelId = li.remove()
            .attr("aria-controls");
        $("#" + panelId).remove();
        var $tab_list = $("#tabs");
        $tab_list.tabs({ active: tab_index + 1 });
        $tab_list.tabs("refresh");
        ChObjectStorage.garbageCollection();
        console.log('close')
    },
    updateAttachment: function ($table_cell) {
        console.log('updateAttach')
        var name = $table_cell.attr('name')
        var parent_id = $table_cell.attr('parent-id')
        var parent_data_id = $table_cell.attr('parent-data-id')
        var data_id = $table_cell.attr('data-id')
        var $input = $('[data-id="' + data_id + '"][name="' + name + '"][parent-data-id="' + parent_data_id + '"][parent-id="' + parent_id + '"]')
        var $tr = $input.closest('tr')
        var $form = $input.closest('form')
        var id = $tr.attr('data-id')
        console.log($input.parent().html())
        $form.fileupload('add', {
            fileInput: $input
        });
        console.log($form.find($input))
//        var ch_form = ChObjectStorage.create($form, 'ChGridForm');
        var ch_from = ChObjectStorage.create($form, 'ChGridForm');
//            new ChGridForm($form);
        ch_from.removeRow($input)
    },
    mergeData: function (original_data, changed_data) {
        return  $.extend(false, original_data, changed_data);
    },
    /**
     *
     * @param ch_form {ChGridForm}
     */
    saveAttachment: function (ch_form) {
        console.log('sa')
        var ch_messages_container = ch_form.getMessagesContainer(),
            deleted_obj = $.extend({}, ch_form.getDeletedObj());
        var deleted_data = JSON.stringify(deleted_obj),
            url = ch_form.getSaveUrl() + '&parentView=' + ch_form.getParentView() + '&parentID=' + ch_form.getParentPK(),
            data = {
                jsonChangedData: {},
                jsonDeletedData: deleted_data
            },
            _this = ch_form;
        if (!$.isEmptyObject(deleted_data)) {

            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                async: false
            })
                .done(function (response) {
                    var ch_response = new ChResponse(response);
                    if (ch_response.isSuccess()) {
                        _this._clearChangedObj();
                        _this._clearDeletedObj()
                        _this.refresh();
                    }
                    ch_response.sendMessage(ch_messages_container);
                })
                .fail(function (response) {
                    ch_messages_container.sendMessage('Возникла непредвиденная ошибка при сохранении вложений.', ChResponseStatus.ERROR);
                })

        }

    },
    parse: function (key, val) {
        if (typeof(val) == 'string') {
            return decodeURIComponent(val)
        } else {
            return val;
        }
    },
    createChildGridTabID: function (parent_id, view, parent_view) {
        return parent_id + "_" + parent_view.replace('.', '_') + '_' + view.replace('.', '_');

    },
    openForm:function(url){
        if (url != '#') {
            $.post(url, []).done(function (response) {
                Chocolate.$tabs.append(response)
            })
        }
    },
    setUser: function (name) {
        Chocolate.storage.session.user = {
            name: name
        }
    },
    getUserName: function () {
        return Chocolate.storage.session.user.name;
    },
    initNavSearch: function(jsonData){
        var autocomplete_data = json_parse(jsonData, Chocolate.parse);
        $('#nav-search').autocomplete({
            delay: 100,
            select: function (event, ui) {
                Chocolate.openForm(ui.item.url);
            },
            source: function (request, response) {
                var searchParam = Chocolate.eng2rus(request.term.toLowerCase())
                function outputItem(item, i, arr) {
                    var source = item.value.toLowerCase();
                    if (source.indexOf(searchParam) != -1) {
                        return true
                    }
                return false
                }
                response(autocomplete_data.filter(outputItem))
            }
        })
    }

};
