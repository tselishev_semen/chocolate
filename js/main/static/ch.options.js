var ChOptions = {
    tablesorter: {
        output: "с {startRow} по {endRow} ({totalRows})",
        size: 40
    },
    settings: {
        defaultColumnsWidth: '150',
        defaultAutoUpdateMS: 100000,
        formatDate: 'yyyy.mm.dd HH:MM:ss',
        systemCols: ['lastmodifier', 'lastmodifydate', 'insdate', 'username']
    },
    classes: {
      allowHideColumn :'data-col-hide',
      hiddenAllColsTable: 'ch-hide',
      hiddenSystemColsTable: 'ch-hide-system'
    },
    urls:{
        addRow: '/grid/insertRow',
        export2excel: '/majestic/export2excel',
//        gridExecute: '/majestic/gridExecute',
        queueExecute: '/majestic/queueExecute'
    }
}