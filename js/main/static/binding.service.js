var bindingService ={
    binFromParentData: function(sql, data){
        var bindingSql = sql;
        var parentID = data.id;
        bindingSql = sql.replace(/\[parentid\]/i, parentID);
        return bindingSql;

    },
    bindFromData: function(sql, data){
        var bindingSql = sql.replace(/\[id\]/i,  data.id);
        return bindingSql;
    }
}