/**
 * Объект, сохраняющий все функции обратного вызова для Grid.
 */
var ChEditableCallback = {
    callbacks: [],
    fire: function ($context, id) {
        if (this._hasCallbacks(id)) {
            this.callbacks[id].fire($context);
        }
    },
    remove: function (id) {
        if (this._hasCallbacks(id)) {
            delete this.callbacks[id];
        }
    },
    _hasCallbacks: function (id) {
        if (typeof(this.callbacks[id]) == 'undefined') {
            return false;
        } else {
            return true;
        }
    },
    add: function (callback, id) {
        if (!this._hasCallbacks(id)) {
            this.callbacks[id] = $.Callbacks();
        }
        this.callbacks[id].add(callback);
    }
};