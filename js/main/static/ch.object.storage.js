var ChObjectStorage = {
    _object_storage: [],
    getByID: function (id) {
        if (typeof(this._object_storage[id]) != 'undefined') {
            return this._object_storage[id];
        }
        return null;
    },
    _set: function (id, obj) {
        this._object_storage[id] = obj;
    },
    create: function ($jQuery, object_class) {
        var id = $jQuery.attr('id');
        if (!id) {
            $jQuery.uniqueId();
            id = $jQuery.attr('id')
        }
        var obj = this.getByID(id);
        if (!obj) {
            obj = new window[object_class]($jQuery);
            this._set(id, obj);
        }
        return obj;
    },
    garbageCollection: function(){
        for(var id in this._object_storage){
           if($('#'+id).length == 0){
               delete this._object_storage[id];
               delete Chocolate.storage.session[id];
           }
        }
    }

}