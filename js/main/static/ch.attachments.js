var ChAttachments = {
    files: [],
    isSet: function (id) {
        if (typeof(this.files[id]) == 'undefined') {
            return false;
        }
        return true;
    },
    clear: function (id) {
        delete this.files[id];
    },
    isNotEmpty: function (id) {
        if (this.isSet(id) && this.files[id].length > 0) {
            return true;
        }
        return false;
    },
    isEmpty: function (id) {
        return !this.isNotEmpty(id);
    },
    push: function (id, file) {
        if (!this.isSet(id)) {
            this.files[id] = [];
        }
        this.files[id].push(file);
    },
    pop: function (id) {
        if (this.isNotEmpty(id)) {
            return this.files[id].pop();
        }
        return null;
    }
};