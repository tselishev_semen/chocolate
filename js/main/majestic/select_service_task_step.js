function SelectServiceTaskStep() {
    MajesticWizardMethod.apply(this, arguments);
}
SelectServiceTaskStep.prototype = Object.create(MajesticWizardMethod.prototype);
SelectServiceTaskStep.prototype.run = function (mjWizard) {
    var _this = this;
    //Строим дерево
    jQuery.get(
        MajesticVars.EXECUTE_URL,
        {cache: true, sql: 'Tasks.ServicesGet'},
        function (response) {
            var ch_response = new ChResponse(response);
            if (ch_response.isSuccess()) {
                var nodes = ch_response.getData(),
                    map = {}, node, roots = [], autocomplete_data = [];
                for (var i in nodes) {
                    node = nodes[i];
                    node.title = node.data.name;
                    node.key = i;
                    node.icon = false;
                    node.parentid = node.data.parentid;
                    node.description = node.data.description;
                    node.usersidlist = node.data.usersidlist;
                    delete node.data;
                    node.children = [];
                    map[node.id] = i; // use map to look-up the parents
                    if (node.parentid !== null) {
                        autocomplete_data.push({label: node.title, id: node.key})
                        nodes[map[node.parentid]].children.push(node);
                    } else {

                        roots.push(node);
                    }
                }
                var $content = $('<div></div>');
                var $tree = $('<div class="widget-tree"></div>');
                $tree.dynatree({
                    children: roots,
                    selectMode: 1,
                    onQueryActivate: function (flag, node) {
                        if (node.data.children.length == 0) {
                            return true;
                        } else {
                            return false
                        }
                    },
                    onRender: function (node, nodeSpan) {
                        if (node.data.children.length == 0) {

                            $(nodeSpan).attr("data-id", node.data.id);
                            $(nodeSpan).attr("data-title", node.data.title.toLowerCase());
                        }
                    },
                    onActivate: function (node) {
                        var desc = node.data.description,
                            useridlist = node.data.usersidlist;
                        mjWizard.description = desc;
                        mjWizard.usersidlist = useridlist;
                        var $span = $(node.span),
                            $nxt_btn = $span.closest('div.ui-widget').find('button.wizard-next-button.wizard-no-active');
                        $nxt_btn.removeClass('wizard-no-active').addClass('wizard-active');
                    }
                });
                $tree.dynatree("getRoot").visit(function (node) {
                    node.expand(true);
                });
                var $search = $('<input type="text">');
                var $header = $('<div class="widget-header-tree"></div>')

//                $header.prepend('<div class="widget-steps-container"><div class="widget-step">Первый</div></div>');
                $header.append($search);
                $content.prepend($header)
                $content.append($tree)

                openWizardDialog($content, mjWizard, _this, false, 'Выберите тип поручения '+mjWizard.getStepCaption());
                $search.autocomplete({
                    delay: 100,
                    source: function (request, response) {
                        var searchParam = Chocolate.eng2rus(request.term.toLowerCase())

                        function outputItem(item, i, arr) {
                            var source = item.label.toLowerCase();
                            if (source.indexOf(searchParam) != -1) {
                                return true
                            }
                            else {
                                return false
                            }
                        }

                        var responseArray = autocomplete_data.filter(outputItem);
                        $content.find('.node-searched').removeClass('node-searched')
                        $content.find('[data-title*=\'' + searchParam + '\']').addClass('node-searched')
                        response(responseArray)
                    },
                    close: function (event, ui) {
                        $content.find('.node-searched').removeClass('node-searched')

                    },
                    select: function (event, ui) {
                        var id = ui.item.id
                        var $elem = $content.find('[data-id=' + id + ']')
                        $content.find('.node-searched').removeClass('node-searched')
                        $tree.dynatree("getTree").activateKey(id)

                    }
                });
            }
            else {
                alert("Произошла ошибка при открытии мастера поручений. Обратитесь к разработчикам.")
            }
        }
    ).
        fail(function () {
            alert("Произошла ошибка при открытии мастера поручений. Обратитесь к разработчикам.")
        })
}