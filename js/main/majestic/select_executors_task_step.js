
function SelectExecutorsTaskStep() {
    MajesticWizardMethod.apply(this, arguments);
}
SelectExecutorsTaskStep.prototype = Object.create(MajesticWizardMethod.prototype);
SelectExecutorsTaskStep.prototype.run = function (mjWizard) {
    var _this = this;
    jQuery.get(
        MajesticVars.EXECUTE_URL,
        {cache: true, sql: 'tasks.uspGetUsersListForTasksUsers'},
        function (response) {
            var ch_response = new ChResponse(response);
            var data = ch_response.getData();
            var select2_data = [];
            var init_data = [];
            var idlist = mjWizard.usersidlist.split('|')
            for (var i in data) {
                var id = data[i].id,
                    name = data[i].data.name;
                if (idlist.indexOf(id) != -1) {
                    init_data.push({
                        id: id,
                        text: name
                    })
                }
                select2_data.push({
                    id: id,
                    text: name
                })
            }
            var $content = $('<div><div class="widget-header"><div class="widget-titles">Выберите исполнителей</div><div class="widget-titles-content">Пожалуйста, выберите исполнителей вашего поручения</div></div></div>'),
                $select = $('<div class="wizard-select2"></div>');
            $content.append($select)
            openWizardDialog($content, mjWizard, _this, true, 'Выберните исполнителей '+mjWizard.getStepCaption());
            $select.select2({
                multiple: true,
                data: select2_data
            });
            $select.select2('data', init_data)
            $select.on('change', function (e) {
                var val = e.val.join('|') + '|';
                mjWizard.usersidlist = val;
            })
        }

    )
}