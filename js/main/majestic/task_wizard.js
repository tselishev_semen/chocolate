
/**
 * Мастер создание поручений
 * @param ch_form {ChGridForm}
 */
function TaskWizard(ch_form) {
    MajesticWizard.apply(this, arguments);
    this.ch_form = ch_form;
    this.usersidlist = null;
    this.description = null;
}
TaskWizard.prototype = Object.create(MajesticWizard.prototype);
TaskWizard.prototype.cancel = function () {
//    alert("Создаем строку")
};
TaskWizard.prototype.done = function () {
//    console.log(this.description)
//    console.log(this.usersidlist)
    var data = jQuery.extend({},this.ch_form.getDefaultObj());
    data.usersidlist = this.usersidlist;
    data.description = this.description;

    this.ch_form.addRow(data)
//    alert("Создаем строку")
};
TaskWizard.prototype.openStep = function () {

}
TaskWizard.prototype.getStepCaption = function () {
    return '( Шаг ' + this.getCurrentStep() + ' из ' + this.getStepsCount() + ')';
}