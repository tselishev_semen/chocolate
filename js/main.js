/**
 * Локальное хранилище
 */
var ObjectStorage = function ObjectStorage(name, duration) {
    var self,
        name = name || '_objectStorage',
        defaultDuration = 15000;
    if (ObjectStorage.instances[ name ]) {
        self = ObjectStorage.instances[ name ];
        self.duration = duration || self.duration;
    } else {
        self = this;
        self._name = name;
        self.duration = duration || defaultDuration;
        self._init();
        ObjectStorage.instances[ name ] = self;
    }
    return self;
};
ObjectStorage.instances = {};
ObjectStorage.prototype = {
    // type == local || session
    _save: function (type) {
        var stringified = JSON.stringify(this[ type ]),
            storage = window[ type + 'Storage' ];
        if (storage.getItem(this._name) !== stringified) {
            storage.setItem(this._name, stringified);
        }
    },
    _get: function (type) {
        this[ type ] = JSON.parse(window[ type + 'Storage' ].getItem(this._name)) || {};
    },
    _init: function () {
        var self = this;
        self._get('local');
        self._get('session');
        (function callee() {
            self.timeoutId = setTimeout(function () {
                self._save('local');
                callee();
            }, self._duration);
        })();
        window.addEventListener('beforeunload', function () {
            self._save('local');
            self._save('session');
        });
    },
    timeoutId: null,
    local: {},
    session: {}
};
;var ChOptions = {
    tablesorter: {
        output: "с {startRow} по {endRow} ({totalRows})",
        size: 40
    },
    settings: {
        defaultColumnsWidth: '150',
        defaultAutoUpdateMS: 100000,
        formatDate: 'yyyy.mm.dd HH:MM:ss',
        systemCols: ['lastmodifier', 'lastmodifydate', 'insdate', 'username']
    },
    classes: {
      allowHideColumn :'data-col-hide',
      hiddenAllColsTable: 'ch-hide',
      hiddenSystemColsTable: 'ch-hide-system'
    },
    urls:{
        addRow: '/grid/insertRow',
        export2excel: '/majestic/export2excel',
//        gridExecute: '/majestic/gridExecute',
        queueExecute: '/majestic/queueExecute'
    }
};var ChObjectStorage = {
    _object_storage: [],
    getByID: function (id) {
        if (typeof(this._object_storage[id]) != 'undefined') {
            return this._object_storage[id];
        }
        return null;
    },
    _set: function (id, obj) {
        this._object_storage[id] = obj;
    },
    create: function ($jQuery, object_class) {
        var id = $jQuery.attr('id');
        if (!id) {
            $jQuery.uniqueId();
            id = $jQuery.attr('id')
        }
        var obj = this.getByID(id);
        if (!obj) {
            obj = new window[object_class]($jQuery);
            this._set(id, obj);
        }
        return obj;
    },
    garbageCollection: function(){
        for(var id in this._object_storage){
           if($('#'+id).length == 0){
               delete this._object_storage[id];
               delete Chocolate.storage.session[id];
           }
        }
    }

};var ChTableHelper = {
    tableElemIndex: {
        head: '0',
        body: '1',
        foot: '2'
    },
    tbodyRegex: /(tbody|TBODY)/,
    theadRegex: /(thead|THEAD)/,
    tfootRegex: /(tfoot|TFOOT|fthfoot)/,
    getCells: function (elem, index) {
        var ei = this.tableElemIndex,
            tds = {
                //store where the cells came from
                'semantic': {
                    '0': [],//head throws error if ei.head or ei['head']
                    '1': [],//body
                    '2': []//footer
                },
                //keep a ref in a flat array for easy access
                'array': []
            },
        //cache regex, reduces looking up the chain
            tbodyRegex = this.tbodyRegex,
            theadRegex = this.theadRegex,
        //reduce looking up the chain, dont do it for the foot think thats more overhead since not many tables have a tfoot
            tdsSemanticBody = tds.semantic[ei.body],
            tdsSemanticHead = tds.semantic[ei.head];

        //check does this col exsist
        if (index <= -1 || typeof elem.rows[0].cells[index] == undefined) {
        }

        var count = 0;
        for (var i = 0, length = elem.rows.length; i < length; i++) {

            var td = elem.rows[i].cells[index];

            //if the row has no cells dont error out;
            if (td == undefined) {
                continue;
            }
            var parentNodeName = td.parentNode.parentNode.nodeName;
            tds.array.push(td);
            //faster to leave out ^ and $ in the regular expression
            if (tbodyRegex.test(parentNodeName)) {

                tdsSemanticBody.push(td);

            } else if (theadRegex.test(parentNodeName)) {

                tdsSemanticHead.push(td);

            } else if (this.tfootRegex.test(parentNodeName)) {

                tds.semantic[ei.foot].push(td);
            }

            count = i;
        }

        var colgroup = elem.getElementsByTagName('colgroup')
        if (typeof  colgroup != 'undefined') {
            var lng2 = colgroup.length
            for (var i = 0, length = lng2; i < length; i++) {
                var col = colgroup[i].getElementsByTagName('col')
                var td = col[index]
                tds.array.push(td);

            }
        }

        var fthtd = elem.getElementsByTagName('fthfoot')[0]
        if (typeof  fthtd != 'undefined') {

            var footer = fthtd.getElementsByTagName('fthrow');
            var lng = footer.length
            for (var i = 0, length = lng; i < length; i++) {
                var fthtd = footer[i].getElementsByTagName('fthtd')
                var td = fthtd[index]
                tds.array.push(td);

            }
        }
        return tds;
    },
    swapCells: function (a, b) {
        a.parentNode.insertBefore(b, a);
    },
    swapColsManyTables: function (tables, from, to) {
        var _this = this;
        tables.forEach(function (elem, i, arr) {
            _this.swapCols(elem, from, to);
        })
    },
    swapCols: function (elem, from, to) {
        var currentColumnCollection = this.getCells(elem, from).array;
        if (from < to) {
            for (var i = from; i < to; i++) {
                var row2 = this.getCells(elem, i + 1);
                for (var j = 0, length = row2.array.length; j < length; j++) {
                    this.swapCells(currentColumnCollection[j], row2.array[j]);
                }
            }
        }
        else if (from != to) {
            for (var i = from; i > to; i--) {
                var row2 = this.getCells(elem, i - 1);
                for (var j = 0, length = row2.array.length; j < length; j++) {
                    this.swapCells(row2.array[j], currentColumnCollection[j]);
                }
            }
        }
    },
    hideColsManyTables: function (tables, positions) {
        var _this = this;
        tables.forEach(function (elem, i, arr) {
            _this.hideCols(elem, positions);
        })
    },
    hideCols: function (table, positions) {
        this._setCellDisplay(table, positions, 'none');
    },
    _setCellDisplay: function (table, positions, display) {
        var _this = this;
        positions.forEach(function (pos) {
            var cellsCollection = _this.getCells(table, pos);
            cellsCollection.array.forEach(function (cell) {
                cell.style.display = display;
            })
        })
    },
    showCols: function (table, positions) {
        this._setCellDisplay(table, positions, '');
    },
    showColsManyTables: function (tables, positions) {
        var _this = this;
        tables.forEach(function (elem, i, arr) {
            _this.showCols(elem, positions);
        })
    }
}

var test = function test() {
    var $table = $('table');
    var tablecol = [];
    tablecol.push($table.get(0), $table.get(1));
    var positions = [4, 5]
    ChTableHelper.hideColsManyTables(tablecol, positions)
};/**
 * Объект, сохраняющий все функции обратного вызова для Grid.
 */
var ChEditableCallback = {
    callbacks: [],
    fire: function ($context, id) {
        if (this._hasCallbacks(id)) {
            this.callbacks[id].fire($context);
        }
    },
    remove: function (id) {
        if (this._hasCallbacks(id)) {
            delete this.callbacks[id];
        }
    },
    _hasCallbacks: function (id) {
        if (typeof(this.callbacks[id]) == 'undefined') {
            return false;
        } else {
            return true;
        }
    },
    add: function (callback, id) {
        if (!this._hasCallbacks(id)) {
            this.callbacks[id] = $.Callbacks();
        }
        this.callbacks[id].add(callback);
    }
};;
/**
 * Объект, сохраняющий функции обратного вызова при инициализации карточки. После вызова - очищается
 */
var ChCardInitCallback = {
    callbacks: $.Callbacks(''),
    fireOnce: function ($context) {
        this.callbacks.fire($context);
        this._clear();
    },
    _clear: function () {
        this.callbacks.empty();
    },
    add: function (callback) {
        this.callbacks.add(callback);
    }
};
;var ChAttachments = {
    files: [],
    isSet: function (id) {
        if (typeof(this.files[id]) == 'undefined') {
            return false;
        }
        return true;
    },
    clear: function (id) {
        delete this.files[id];
    },
    isNotEmpty: function (id) {
        if (this.isSet(id) && this.files[id].length > 0) {
            return true;
        }
        return false;
    },
    isEmpty: function (id) {
        return !this.isNotEmpty(id);
    },
    push: function (id, file) {
        if (!this.isSet(id)) {
            this.files[id] = [];
        }
        this.files[id].push(file);
    },
    pop: function (id) {
        if (this.isNotEmpty(id)) {
            return this.files[id].pop();
        }
        return null;
    }
};;/**
 * Статусы ответов с шоколада (Response.php)
 */
var ChResponseStatus = {
    SUCCESS: 0,
    ERROR: 1,
    WARNING: 2
};;/**
 * Created by tselishchev on 03.06.14.
 */
function ChAjaxTaks(id, chObjectClass, params) {
    this.id = id;
    this.chObjectClass = chObjectClass;
    this.params = params;
}
var chAjaxQueue = {
    queue: [],
    /**
     *
     * @param chAjaxTaks {ChAjaxTaks}
     */
    enqueue: function (chAjaxTaks) {
        this.queue.push(chAjaxTaks);
    },
    dequeue: function () {
        var data = {}, i = 0;
        while(this.queue.length){
            i++;
            /**
             * @type {ChAjaxTaks}
             */
            var ajaxTask = this.queue.shift();
            data[i] ={
                id: ajaxTask.id,
                type: ajaxTask.chObjectClass,
                params: ajaxTask.params
            };
        }
        return data;
    },
    send: function () {
        var data = this.dequeue();
        if (Object.keys(data).length) {

            $.post(ChOptions.urls.queueExecute, {package:data}).done(function (response) {
                var ch_response = new ChPackageResponse(response);
                ch_response.applyResponse();
//                console.log(response);
//            chForm.updateData(ch_response.getData(), ch_response.getPreview());
            }).fail(function () {
                    alert('fail');
                })
        }
    }

};function ChDynatree($elm) {
    this.$elm = $elm;
    this.options = null;
    this.data = [];
    this.auto_complete_data = [];
}
ChDynatree.prototype.init = function (options) {
    this.options = options;
    this.getNodes = function () {
        return options['children'];
    };
    this.getUrl = function () {
        return options['url'];
    };
    this.isExpandNodes = function () {
        return options['expand_nodes'];
    };
    this.isSelectAll = function () {
        return options['select_all'];
    };
    this.isRestoreState = function () {
        return options['restore_state'];
    };
    this.getSeparator = function () {
        return options['separator'];
    };
    this.getRootID = function () {
        return options['root_id'];
    };
    this.getTitle = function () {
        return options['title'];
    };
    this.getColumnTitle = function () {
        return options['column_title'];
    };
    this.getColumnID = function () {
        return options['column_id'];
    };
    this.getColumnParentID = function () {
        return options['column_parent_id'];
    };
}
ChDynatree.prototype.setData = function ($input, $content) {
    var url = this.getUrl(),
        raw_data,
        map = {},
        root_id = this.getRootID();

    if (url) {
        jQuery.ajax({
            type: 'GET',
            url: url,
            async: false,
            success: function (response) {
                var ch_response = new ChResponse(response);
                if (ch_response.isSuccess()) {
                    raw_data = ch_response.getData()
                    jQuery.data($input.get(0), "dialog", $content);
                } else {
                    alert('Ошибка при загрузке данных')
                }
            }
        })
    } else {
        raw_data = this.getNodes();
    }

    for (var i in raw_data) {
        var node = raw_data[i];
        node.title = node.data[this.getColumnTitle()];
        node.key = node.data[this.getColumnID()];
        node.index = i;
        node.icon = false;
        node.parentid = node.data[this.getColumnParentID()];
        node.children = [];
        map[node.key] = node.index; // use map to look-up the parents
        this.auto_complete_data.push({label: node.title, id: node.key});
    }

    for (var i in raw_data) {
        var node = raw_data[i];
        if (typeof(node.parentid) != 'undefined' && node.parentid !== root_id && node.parentid) {
            raw_data[map[node.parentid]].children.push(node);
        } else {
            this.data.push(node);
        }
    }
}
ChDynatree.prototype.dialogEvent = function ($content, $tree, $input, $checkbox, $select) {
    var _this = this;
    $content.dialog({
        resizable: false,
        title: this.getTitle(),
        dialogClass: 'wizard-dialog',
        modal: true,
//        height: 500,
//        width: 700,
        buttons: {
            OK: {
                text: 'OK',
                class: 'wizard-active wizard-next-button',
                click: function (bt, elem) {
                    var selected_nodes = $tree.dynatree("getSelectedNodes");
                    var val = '', select_html = '';
                    var is_select_all = _this.isSelectAll();
                    for (var i in selected_nodes) {
                        var node = selected_nodes[i];
                        if (is_select_all || node.childList == null) {
                            val += node.data.key + _this.getSeparator();
                            select_html += "<option>" + node.data.title + "</option>";
                        }
                    }
                    $input.val(val);
                    $select.html(select_html);
                    $checkbox.children('input').attr('checked', false)
                    $(this).dialog("close");
                }},
            Отмена: {
                text: 'Отмена',
                class: 'wizard-cancel-button',
                click: function () {
                    $checkbox.children('input').attr('checked', false)
                    $(this).dialog("close");
                }
            }
        }
    });
}
ChDynatree.prototype.checkboxClickEvent = function ($checkbox, $tree) {

    $checkbox.on('click', 'input', function () {
        if ($(this).is(':checked')) {
            $tree.dynatree("getRoot").visit(function (node) {
                node.select(true);
            });
        } else {
            $tree.dynatree("getRoot").visit(function (node) {
                node.select(false);
            });
        }
    })

}
ChDynatree.prototype.autoCompleteEvent = function ($search, $tree, $content) {
    var _this = this;
    $search.autocomplete({
        delay: 100,
        source: function (request, response) {
            var searchParam = request.term.toLowerCase();

            var responseArray = _this.auto_complete_data.filter(function (item, i, arr) {
                var source = item.label.toLowerCase();
                if (source.indexOf(searchParam) != -1) {
                    return true
                }
                return false
            });
            $content.find('.node-searched').removeClass('node-searched')
            $content.find("[data-title*='" + searchParam + "']").addClass('node-searched')
            response(responseArray)
        },
        close: function (event, ui) {
            $content.find('.node-searched').removeClass('node-searched')

        },
        select: function (event, ui) {
            var id = ui.item.id
            $content.find('.node-searched').removeClass('node-searched')
            var tree = $tree.dynatree("getTree");
            tree.selectKey(id, true);
            tree.activateKey(id);
        }
    });
}
ChDynatree.prototype.load = function (options) {
    this.init(options);
    var $tree_con = this.$elm.parent(),
        $input = $tree_con.children('input[type=hidden]'),
        $dialog = jQuery.data($input.get(0), "dialog"),
        is_restore_state = this.isRestoreState(),
        $select = $tree_con.children('select');

    if (typeof($dialog) != 'undefined') {
        var default_values = $input.val().split(this.getSeparator());
        if (!is_restore_state) {
            $input.val('');
            $select.html('');
        }
        $dialog.children('.widget-tree').dynatree("getRoot").visit(function (node) {
            if (is_restore_state && $.inArray(node.data.key, default_values) != '-1') {
                node.select(true);
            } else {
                node.select(false);
            }
        });
        $dialog.dialog('open');
    } else {
        var $content = $('<div></div>'),
            $tree = $('<div class="widget-tree"></div>');

        //Необходимо инициализировать данные
        this.setData($input, $content);
        this.options.children = this.data;

        $tree.dynatree(this.options);
        if (this.isExpandNodes()) {
            $tree.dynatree("getRoot").visit(function (node) {
                node.expand(true);
            });
        }
        var $search = $('<input type="text">'),
            $header = $('<div class="widget-header-tree"></div>'),
            $checkbox = $('<span class="tree-checkbox"><input type="checkbox"><span class="tree-checkbox-caption">Выделить все</span></span>');
        $header.append($search);
        $content.prepend($header)
        $content.append($tree)

        this.dialogEvent($content, $tree, $input, $checkbox, $select);
        $content.next().prepend($checkbox)
        this.autoCompleteEvent($search, $tree, $content);
        this.checkboxClickEvent($checkbox, $tree);
    }
}
;function ChMap($map) {
    this.$map = $map;
    this.map = new ymaps.Map(this.$map.attr('id'), {
        center: [59.94, 30.31],
        zoom: 7
    });
    this._objects = null;
}
ChMap.prototype = {
    init: function (ymaps, encoded_data) {
        var ch_tab = ChObjectStorage.create(Chocolate.getActiveTabObj(), 'ChTab');
        ChocolateDraw.reflowTab(ch_tab);
        var points = json_parse(encoded_data, Chocolate.parse)
        this.map.controls
            // Кнопка изменения масштаба.
            .add('zoomControl', { left: 5, top: 5 })
            // Список типов карты
            .add('typeSelector')
            // Стандартный набор кнопок
            .add('mapTools', { left: 35, top: 5 });
        this.setPoints(points);

    },
    /**
     *
     * @param points
     * @param ch_messages_container {ChMessagesContainer}
     */
    refreshPoints: function (points, ch_messages_container) {
        var map = this.map;
        if (this._objects) {
            map.geoObjects.remove(this._objects);
        }
        var count = this.setPoints(points)
        ch_messages_container._sendSuccessMessage('Всего объектов на карте: ' + count, 0)
    },
    setPoints: function (points) {
        var map = this.map;
        var myGeoObjects = [];
        for (var i in points) {
            //TODO: передать на арбузные свойства
            var point = points[i];
            var lat = point.latitude,
                long = point.longitude,
                header = 'Договор № ' + point.contractid,
                body = point.address;
            if (lat && long) {
                myGeoObjects.push(new ymaps.GeoObject({
//                    geometry: {type: "Point", coordinates: [long, lat]}
                        geometry: {type: "Point", coordinates: [lat, long]},
                        properties: {
                            hintContent: body,
                            balloonContentHeader: header,
                            balloonContentBody: body,
                            clusterCaption: header
                        }
                    },
                    {
                        preset: 'twirl#violetIcon'
                    }
                ));
            }
        }

        var clusterer = new ymaps.Clusterer(
            {
                clusterDisableClickZoom: true,
                preset: 'twirl#invertedVioletClusterIcons'
            }
        );
        clusterer.add(myGeoObjects);
        this._objects = clusterer;
        map.geoObjects.add(clusterer);
        return myGeoObjects.length;

    }
};/**
 * Класс отвечающий за закладки в шоколаде(как в карточке так и глобальные)
 * @param $a {jQuery}
 * @constructor
 */
function ChTab($a) {
    this.$a = $a;
}
ChTab.prototype._id = null;
ChTab.prototype._panel_id = null;
ChTab.prototype._$li = null;
ChTab.prototype._$panel = null;
ChTab.prototype._is_card_type_panel = null;
ChTab.prototype._$card_content = null;
ChTab.prototype._$ul = null;
ChTab.prototype.getID = function () {
    if (this._id == null) {
        this._id = this.$a.attr('id');
    }
    return this._id;

};
/**
 * @returns {jQuery}
 */
ChTab.prototype.getCardContent = function () {
    this._$card_content = this.getPanel().find('div.card-content');
    return this._$card_content;
};
/**
 * @returns {boolean}
 */
ChTab.prototype.isCardTypePanel = function () {
    if (this._is_card_type_panel == null) {
        var type = this.getPanel().attr('data-type');
        if (type == 'chocolate-card') {
            this._is_card_type_panel = true;
        } else {
            this._is_card_type_panel = false;
        }
    }
    return this._is_card_type_panel;
};
/**
 * @returns {jQuery}
 */
ChTab.prototype.getLi = function () {
    if (this._$li == null) {
        this._$li = this.$a.parent();
    }
    return this._$li;
};
/**
 * @returns {string}
 */
ChTab.prototype.getPanelID = function () {
    if (this._panel_id == null) {
        this._panel_id = this.getLi().attr('aria-controls');
    }
    return  this._panel_id;
};
/**
 * @returns {jQuery}
 */
ChTab.prototype.getPanel = function () {
    if (this._$panel == null) {
        this._$panel = $('#' + this.getPanelID());
    }
    return this._$panel;
};
/**
 * @returns {jQuery}
 */
ChTab.prototype.getListContainer = function () {
    if (this._$ul == null) {
        this._$ul = this.$a.closest('ul');
    }
    return this._$ul;
}
/**
 * @returns {integer|string}
 */
ChTab.prototype.getIndex = function () {
    return this.$a.closest('ul').find('li').index(this.$a.parent());
};;
/**
 * Класс, отвечащий за отображение сообщений на клиенте.
 */
function ChMessagesContainer($message_container) {
    this.$message_container = $message_container;
}
ChMessagesContainer.prototype = {
    sendMessage: function (status_msg, status_code) {
        switch (status_code) {
            case ChResponseStatus.ERROR:
                this._sendErrorMessage(status_msg);
                break;
            case ChResponseStatus.SUCCESS:
                this._sendSuccessMessage(status_msg, 5000);
                break;
            case ChResponseStatus.WARNING:
                this._sendWarningMessage(status_msg)
                break;
            default:
                this._sendErrorMessage(status_msg);
                break;
        }
    },
    _sendSuccessMessage: function (status_msg, duration) {
        this._appendMessage('<div class="grid-message"><div class="alert in alert-block fade alert-success">' + status_msg + '</div></div>', duration);
    },
    _appendMessage: function (html, duration) {
        var $message = this.$message_container;
        $message.html(html);
        if (duration) {

            setTimeout(function () {
                $message.html('')
            }, duration);
        }
    },
    _sendErrorMessage: function (status_msg) {
        this._appendMessage('<div class="grid-message"><div class="alert in alert-block fade alert-error">' + status_msg + '</div></div>', 5000);
    },
    _sendWarningMessage: function (status_msg) {
        this._appendMessage('<div class="grid-message"><div class="alert in alert-block fade alert-warning">' + status_msg + '</div></div>', 5000);
    }
};;/**
 * Класс - обертка на js для класса Response.php.
 */
function ChResponse(json_data) {
    this.data = json_parse(json_data, Chocolate.parse);
}

ChResponse.prototype = {
    getData: function () {
        return this.data['data'];
    },
    getStatusCode: function () {
        return this.data['status_code'];
    },
    getStatusMsg: function () {
        return this.data['status_msg'];
    },
    hasError: function () {
        var status_code = this.getStatusCode();
        if (status_code == ChResponseStatus.ERROR) {
            return true;
        }
        return false;
    },
    isSuccess: function () {
        return !this.hasError();
    },
    /**
     * @param ch_message_container {ChMessagesContainer}
     */
    sendMessage: function (ch_message_container) {
        ch_message_container.sendMessage(this.getStatusMsg(), this.getStatusCode());
    }
};function ChGridResponse(json_data) {
    this.data = JSON.parse(json_data);
}
ChGridResponse.prototype = Object.create(ChResponse.prototype);;
function ChSearchResponse(json_data) {
    ChResponse.apply(this, arguments);
}
ChSearchResponse.prototype = Object.create(ChResponse.prototype);
ChSearchResponse.prototype.getPreview = function () {
    return this.data['preview'];
};function ChPackageResponse(json_data) {
    ChResponse.apply(this, arguments);
}
ChPackageResponse.prototype = Object.create(ChResponse.prototype);
ChPackageResponse.prototype.applyResponse = function(){
    this.getData().forEach(function(item){
        var id = item['id'];
        var type = item['type'];
        var data = item['data'];
        var preview = item['preview'];
        if(type == 'ChGridForm'){
            /**
             *
             * @type {ChGridForm}
             */
            var chForm =ChObjectStorage.create($('#' + id), type);
            chForm.updateData(data, preview);
        }
    })
};/**
 * Класс, отвечающий за инициализацию скриптов таблицы(Resort, resize, pagination, dragtable, scrolling
 */
function ChTable($table) {
    this.$table = $table;
    /**
     * @type {ChGridForm}
     */
    this.ch_form = ChObjectStorage.create($table.closest('form'), 'ChGridForm');
}
ChTable.prototype = {
    _initTableSorter: function () {
        var $footer = this.ch_form.getFooter(),
            $page_display = $footer.find("span.pagedisplay");
        this.$table.tablesorter(
            {
                widgets: ["zebra", "filter"],
                headers: {
                    0: { sorter: false}
                },
                widgetOptions: {
                    filter_hideEmpty: false,
                    savePages: false
                }
            })
//            .tablesorterPager({
//                container: $footer,
//                cssPageDisplay: $page_display,
//                output: ChOptions.tablesorter.output,
//                size: ChOptions.tablesorter.size
//            });
    },
    _initResize: function () {
        var $table = this.$table,
            $headers = this.ch_form.getTh().filter(function (i) {
                return i > 0
            }).children('div'),
            start_width = 0,
            ch_form = ChObjectStorage.create($table.closest('form'), 'ChGridForm');

        $headers.each(function (i) {
            var $column_resize, $body_resize;
            $(this).resizable({
                handles: 'e',
                containment: "parent",
                distance: 1,
                stop: function (event, ui) {
                    try {
                        var index = ui.element.parent().get(0).cellIndex,
                            ui_width = ui.size.width,
                            $fixed_table = ch_form.getFixedTable();

                        ch_form.setColumnWidth(index, ui_width);

                        $table.children("colgroup").children("col").eq(index).width(ui_width);
                        $fixed_table.children("colgroup").children("col").eq(index).width(ui_width);
                        ui.element.width(ui_width);

                        var end_width = ui.element.width(),
                            delta_width = end_width - start_width;

                        if (delta_width < 0) {
                            var table_width = $fixed_table.get(0).offsetWidth + delta_width;
                            $fixed_table.width(table_width);
                            $table.width(table_width);
                        }
                    } catch (e) {
                        console.log(e)
                    } finally {
                        $column_resize.remove();
                        $body_resize.remove();
                    }
                },
                resize: function (event, ui) {
                    var position = ui.element.offset();
                    $column_resize.css({ left: position.left + ui.size.width});
                    $body_resize.css({ left: position.left + ui.size.width});
                },
                start: function (event, ui) {
                    var position = ui.element.offset(),
                        header_height = 24,
                        real_height = $table.closest("div").height() - header_height,
                        visible_height = $table.height() - header_height;

                    $column_resize = $("<span>", { class: "column-resize column-resize-header"})
                        .css({
                            top: position.top,
                            left: position.left
                        });
                    $body_resize = $("<span>", { class: "column-resize column-resize-body"})
                        .css({
                            top: position.top + header_height,
                            left: position.left,
                            height: Math.min(visible_height, real_height)
                        });

                    Chocolate.$content.append($column_resize).append($body_resize)
                    start_width = ui.element.width();
                }
            });
        })
    },
    _initFloatThead: function () {
//        console.log('777')
        var _this = this;
        _this.$table.floatThead({
            scrollContainer: function ($table) {
                return _this.ch_form._getUserGrid();
            }
        });
    },
    _initData: function () {
        this.ch_form.restoreData();
    },
    _initSettings: function () {
        this.ch_form.setDefaultSettings();
        if (this.ch_form.ch_form_settings.isAutoUpdate()) {
            this.ch_form.ch_form_settings.startAutoUpdate();
        }
    },
    _initContextMenu: function () {
        var $fixed_table = this.ch_form.getFixedTable(),
            $th = $fixed_table.children('thead').find('th'),
            $sorted_th = $th.filter(function (i) {
                return i > 0
            }),
            _this = this,
            count = $th.length - 1,
            tables = [_this.$table.eq(0)[0], $fixed_table.eq(0)[0]];
        $sorted_th.contextmenu({
            show: { effect: "blind", duration: 0 },
            menu: [
                {title: '< Сделать первой', cmd: 'to-first'},
                {title: 'Сделать последней >', cmd: 'to-last'},
                {title: 'Все колонки', cmd: 'toggle-cols'}
//                {title: 'Скрыть колонку', cmd:'hide-col'}
            ],
            select: function (event, ui) {
                var from = ui.target.closest('th').get(0).cellIndex;
                switch (ui.cmd) {
                    case 'to-first':
                        ChTableHelper.swapColsManyTables(tables, from, 1);
                        _this.$table.floatThead('reflow');
                        _this.ch_form.changeSettings(from, 1);
                        break;
                    case 'to-last':
                        ChTableHelper.swapColsManyTables(tables, from, count);
                        _this.$table.floatThead('reflow');
                        _this.ch_form.changeSettings(from, count);
                        break;
//                    case 'hide-col':
//                        ChTableHelper.hideColsManyTables(tables, [from]);
//                        break;
                    case 'toggle-cols':
                        _this.ch_form.toggleAllCols();
                        break;
                    default :
//                        alert('неизвестная команда')
                        break
                }
            }
        });
    },
    _initDragtable: function () {
        this.ch_form.getFixedTable().dragtable();
    },
    initScript: function () {
        this._initSettings();
        this._initTableSorter();
        this._initResize();
        this._initFloatThead();
        this._initContextMenu();
        this._initDragtable()
        this._initData();
    },
    initAttachmentScript: function () {
        this._initSettings();
        this._initTableSorter();
        this._initResize();
        this._initFloatThead();
        this._initContextMenu();
        this._initDragtable()
    }
};
function ChFilterForm($filter_form) {
    this.$form = $filter_form;
}
ChFilterForm.prototype = {
    getData: function () {
        var data = this.$form.serializeArray(),
            result = {};
        $.each(data, function (i, element) {
            var value = element.value,
                name = element.name;
//            console.log(name, value)
            if (name.slice(-2) == '[]') {
                name = name.slice(0, name.length - 2)
                if (typeof(result[name]) == 'undefined') {
                    result[name] = '';
                }
                result[name] += value + '|'
            } else {
                if (value != '') {
                    result[name] = value
                }
            }
        });
//        console.log(result)
        return result;
    }
}
;
/**
 * Класс, представляющий столбец таблицы
 */
function ChGridColumnHeader($cell) {
    this._key = null;
    this.$cell = $cell;
    this._is_changed = null;
    this._is_grid_type = null;
    this._class = null;
}
ChGridColumnHeader.prototype = {
    getKey: function () {
        if (this._key == null) {
            this._key = this.$cell.attr('data-id');
        }
        return this._key;
    },
    isChanged: function () {
        if (this._is_changed == null) {
            if (this.$cell.attr('data-changed') == 0) {
                this._is_changed = false;
            } else {
                this._is_changed = true;
            }
        }
        return this._is_changed;
    },
    isNotChanged: function () {
        return !this.isChanged();
    },
    isGridType: function () {
        if (this._is_grid_type == null) {
            if (this.$cell.attr('data-grid-button') == 1) {
                this._is_grid_type = true;
            } else {
                this._is_grid_type = false;
            }
        }
        return  this._is_grid_type;
    },
    getClass: function () {
        if (this._class == null) {
            var class_name = '';
            if (this.isNotChanged()) {
                class_name += 'not-changed';
            }
            if (this.isGridType()) {
                class_name += ' grid-button';
            }
            this._class = class_name;

        }
        return this._class;

    },
    getTemplate: function () {
        return ChGridForm.TEMPLATE_TD.replace(/\{class\}/g, this.getClass());
    }
};
function ChGridColumnBody($cell) {
    this.$cell = $cell;
    this._$row = null;
    this._id = null;
    this._ch_form = null;
}
ChGridColumnBody.prototype = {
    getID: function () {
        if (this._id == null) {
            this._id = this.getRow().attr("data-id");
        }
        return  this._id;
    },
    getRow: function () {
        if (this._$row == null) {
            this._$row = this.$cell.closest('tr');
        }
        return this._$row;
    },
    /**
     * @returns {ChGridForm}
     */
    getChForm: function () {
        if (this._ch_form == null) {
            this._ch_form = ChObjectStorage.create(this.$cell.closest('form'), 'ChGridForm');
        }
        return this._ch_form;
    },
    setChangedValue: function (name, val2storage) {
        var changed_obj = this.getChForm().getChangedObj();
        if (typeof(changed_obj[this.getID()]) == "undefined") {
            changed_obj[this.getID()] = {};
        }
        var row_obj = changed_obj[this.getID()];
        row_obj[name] = val2storage;
        this.getRow().addClass('grid-row-changed');
        this.getChForm().getSaveButton().addClass('active');
    }
}
;function ChCardElement($elem) {
    this.$elem = $elem;
    this._ch_card = null;
    this._ch_grid_form = null;
}
ChCardElement.prototype._id = null;
ChCardElement.prototype.getID = function () {
    if (this._id == null) {
        this._id = this.getCard().getKey();
    }
    return this._id;
};
/**
 * @returns {ChCard}
 */
ChCardElement.prototype.getCard = function () {
    if (this._ch_card == null) {
        this._ch_card = ChObjectStorage.create(this.$elem.closest('div[data-id=grid-tabs]'), 'ChCard');
    }
    return this._ch_card
};
/**
 *
 * @returns {ChGridForm}
 */
ChCardElement.prototype.getGridForm = function () {
    if (this._ch_grid_form == null) {
        this._ch_grid_form = this.getCard().getGridForm();
    }
    return this._ch_grid_form;
};
/**
 *
 * @param name
 * @param val2storage
 * @returns {ChCardElement}
 */
ChCardElement.prototype.setChangedValue = function (name, val2storage) {
    var changed_obj = this.getGridForm().getChangedObj();
    if (typeof(changed_obj[this.getID()]) == "undefined") {
        changed_obj[this.getID()] = {};
    }
    //TODO: добавить подсветку строк если надо
    changed_obj[this.getID()][name] = val2storage;
    return this;
};
/**
 *
 * @param name
 * @param value
 * @param visibleText
 * @returns {ChCardElement}
 */
ChCardElement.prototype.setChangedValueInGrid = function (name, value, visibleText) {
    var parentElem = this.getParentElement(name);
    parentElem.html(visibleText);
    parentElem.editable("setValue", value);
    return this;
};
ChCardElement.prototype.getParentElement =function(name){
    return this.getGridForm().$form.find("a[data-pk=" + this.getID() + "][rel$=" + name + "]");
};;function ChGridColumn($elem ){
    this.$elem = $elem;
}

ChGridColumn.prototype.init = function(e, view, caption, url, editable ){
    editable.disable();
    var ch_column = new ChGridColumnBody(this.$elem),
        parent_id = ch_column.getID(),
        isNew = !Chocolate.isNumeric(parent_id),
        /**
         *
         * @type {ChGridForm}
         */
            ch_form = ChObjectStorage.create(this.$elem.closest('form'), 'ChGridForm'),
        filters_data = {filters:{ParentID: parent_id}},
        parent_view_id = ch_form.getID(),
        parent_view = ch_form.getView(),
        tab_id = Chocolate.createChildGridTabID(parent_id, view, parent_view),
        jTabs = Chocolate.$tabs;
    ;
    this.$elem.parents("td").on("click", function(){
        var template = ch_form.getFmChildGridCollection().getCardTemplate(view, parent_view, isNew);
        var jTab = jTabs.find("[aria-controls=\'"+tab_id+"\']");
        if(jTab.length == 0) {
            var caption2 = caption + ' [' + parent_id + ']';
            Chocolate.addTab(tab_id, caption2);
            var lastTabIndex = jTabs.tabs("length");
            jTabs.tabs({ active: lastTabIndex -1});
            if(template == null){


                $.ajax({
                    method: 'GET',
                    url:  url,
                    cache: false,
                    data: {
                        view: view,
                        jsonFilters:JSON.stringify(filters_data),
                        ParentView: parent_view,
                        parentViewID: parent_view_id
                    },
                    success: function (response) {

                        var ch_response = new ChGridResponse(response);
                        if(ch_response.isSuccess()){
                            var jContainer = $("<div id="+ tab_id+ "></div>")
                            jTabs.append(jContainer)
                            var data =ch_response.getData();
                            jContainer.html(data.replace(Chocolate.PK_KEY_REG_EXP, parent_id))
                            jTabs.tabs("refresh");
                            ch_form.getFmChildGridCollection().setCardTemplate(view, parent_view, data, isNew);
                        }else{
                            alert(ch_response.getStatusMsg);
                        }

                    },
                    error: function (xhr, status, error) {
                        alert(xhr.responseText);
                    }
                });
            }else{
                var data = template.replace(Chocolate.PK_KEY_REG_EXP, parent_id);
                var jContainer = $("<div id="+ tab_id+ "></div>");
                jTabs.append(jContainer);
                jContainer.html(data)
            }
        }else{
            jTabs.tabs("select", tab_id)
        }
    });
};
/**
 * Обертка над локальным хранилищем данных
 * @param ch_grid_form {ChGridForm}
 * @constructor
 */
function ChFormSettings(ch_grid_form) {
    this.ch_grid_form = ch_grid_form;
    this.auto_update_inerval_id = null;
    this.startAutoUpdate = function () {
        if (this.auto_update_inerval_id == null) {
            var _this = this;
            this.auto_update_inerval_id = setInterval(function () {
                console.log('intreval')
                if (!ch_grid_form.isHasChange()) {
                    console.log('allowupdate')
                    _this.ch_grid_form.refresh();
                }
            }, ChOptions.settings.defaultAutoUpdateMS)
        }
    };
    this._stopAutoUpdate = function () {
        if (this.auto_update_inerval_id != null) {
            clearInterval(this.auto_update_inerval_id);
        }
    };
    this._getStorage = function () {
        if (typeof(Chocolate.storage.local.grid_settings[ch_grid_form.getView()]) == 'undefined') {
            Chocolate.storage.local.grid_settings[ch_grid_form.getView()] = {}
        }
        return Chocolate.storage;
    };
}
ChFormSettings.prototype.isAutoUpdate = function () {
    var auto_update = this._getStorage().local.grid_settings[this.ch_grid_form.getView()].auto_update;
    if (typeof(auto_update) != 'undefined' && auto_update) {
        return true;
    }
    return false
}
/**
 *
 * @param value {boolean}
 */
ChFormSettings.prototype.setAutoUpdate = function (value) {
    var storage = this._getStorage();
    storage.local.grid_settings[this.ch_grid_form.getView()].auto_update = value
//    console.log(  storage.local.grid_settings[this.ch_grid_form.getView()].auto_update)
    if (value) {
        this.startAutoUpdate();
    } else {
        this._stopAutoUpdate();
    }
}
;/**
 * Класс, представляющий Форму в Шоколаде на клиенте(<form>)
 */
function ChGridForm($form) {
    this.$form = $form;
    this.ch_form_settings = new ChFormSettings(this);
    this._id = null;
    this._view_id = null;
    this._tab_caption = null;
    this._$table = null;
    this._$fixed_table = null;
    this._refresh_url = null;
    this._parent_form_id = null;
    this._ch_messages_container = null;
    this._parent_id = null;
    this._is_ajax_add = null;
    this._$user_grid = null;
    this._user_grid_id = null;
    this._storage = null;
    this._ch_filter_form = null;
    this._$grid_form = null;
    this._parent_view = null;
    this._save_url = null;
    this._parent_pk = null;
    this._is_card_support = null;
    this._$footer = null;
    this._$th = null;
    this._$thead = null;
    this._type = null;
    this._$save_btn = null;
    this.fmChildGridCollection = new FmChildGridCollection();
    this._chCardsCollection = null;
}
ChGridForm.TEMPLATE_TD = '<td class="{class}{class2}"><div class="table-td"><a data-value="{value}" data-pk ="{pk}" rel="{rel}" class="editable"></a></div></td>';
ChGridForm.TEMPLATE_FIRST_TD = '<td class="grid-menu"><span class="card-button" data-id="card-button" title="Открыть карточку"></span>';
/**
 *
 * @returns {FmChildGridCollection}
 */
ChGridForm.prototype.getFmChildGridCollection = function(){
    return this.fmChildGridCollection
};

/**
 *
 * @param chCardsCollection {FmCardsCollection}
 */
ChGridForm.prototype.setFmCardsCollection = function(chCardsCollection){
    this._chCardsCollection = chCardsCollection;
};
/**
 *
 * @returns {null|FmCardsCollection}
 */
ChGridForm.prototype.getFmCardsCollection = function(){
    return this._chCardsCollection;
};
ChGridForm.prototype.getThead = function () {
    if (this._$thead == null) {
        this._$thead = this.getTable().children('thead');
    }
    return this._$thead;
};
ChGridForm.prototype.getType = function () {
    if (this.type == null) {
        this._type = this.$form.children('section').attr('data-id');
    }
    return this._type;
};
ChGridForm.prototype.getTh = function () {
    if (this._$th == null) {
        this._$th = this.getThead().children('tr:first-child').children('th');
    }
    return this._$th;
};
ChGridForm.prototype.hasSettings = function () {
    if ($.isEmptyObject(this.getSettingsObj())) {
        return false;
    }
    return true;
};
ChGridForm.prototype.setDefaultSettings = function () {
    var $th = this.getTh();
    if (this.hasSettings()) {
        var $tr = this.getThead().children('tr'),
            $tr_sorted = $('<tr></tr>'),
            settings_obj = this.getSettingsObj();
        var sorted_columns = settings_obj.sort(function (a, b) {
            if (a.key == 'chocolate-control-column') {
                return -1
            }
            if (b.key == 'chocolate-control-column') {
                return 1
            }
            if (a.weight > b.weight) {
                return 1
            } else {
                return -1
            }
        });

        for (var i in sorted_columns) {
            var column = sorted_columns[i];
            $tr_sorted.append($th.filter('[data-id="' + column.key + '"]'));
        }
        $tr.replaceWith($tr_sorted);
    } else {
        var settings_obj = [];
        $th.each(function (i, elem) {
            if (i == 0) {
                settings_obj[i] = {
                    key: $(elem).attr('data-id'),
                    weight: i,
                    width: '28'
                };
            } else {
                settings_obj[i] = {
                    key: $(elem).attr('data-id'),
                    weight: i,
                    width: ChOptions.settings.defaultColumnsWidth
                }
            }
        })
        this.setSettingsObj(settings_obj)
    }

};
ChGridForm.prototype.getFooter = function () {
    if (this._$footer == null) {
        this._$footer = this.$form.siblings('footer');
    }
    return this._$footer;
};
ChGridForm.prototype.setSettingsObj =  function (setting_obj) {
    var storage = this.getSettingsObj();
    Chocolate.storage.local.settings[this.getView()] = setting_obj
};
ChGridForm.prototype.getColumnWidth = function (index) {
    var settingObj = this.getSettingsObj();
    if ($.isEmptyObject(settingObj)) {
        this.setColumnWidth(index, ChOptions.settings.defaultColumnsWidth)
        return ChOptions.settings.defaultColumnsWidth;
    }
    return settingObj[index].width;
};
ChGridForm.prototype.setColumnWidth = function (index, width) {
    var settingObj = this.getSettingsObj();
    if (!$.isEmptyObject(settingObj)) {
        settingObj[index].width = width;
    }
};
ChGridForm.prototype.changeSettings = function (start_index, end_index) {
    var min_index = 1, setting = this.getSettingsObj();
    if (!$.isEmptyObject(setting)) {
        var key = this.getView(), obj, new_settings = [];
        if (start_index < end_index) {
            for (var i in setting) {
                obj = setting[i];
                if (obj.weight == 0) {
                    new_settings[0] = {key: obj.key, weight: obj.weight, width: obj.width};
                } else {
                    if (obj.weight > start_index && obj.weight <= end_index) {
                        var new_weight = obj.weight - 1;
                        new_settings[new_weight] = {key: obj.key, weight: new_weight, width: obj.width};
                    } else if (obj.weight == start_index) {
                        var new_weight = Math.max(end_index, min_index);
                        new_settings[new_weight] = {key: obj.key, weight: new_weight, width: obj.width};
                    } else {
                        new_settings[obj.weight] = {key: obj.key, weight: obj.weight, width: obj.width};
                    }
                }
            }
        }
        if (start_index > end_index) {
            for (var i in setting) {
                obj = setting[i];
                if (obj.weight == 0) {
                    new_settings[0] = {key: obj.key, weight: obj.weight, width: obj.width};
                } else {
                    if (obj.weight < start_index && obj.weight >= Math.max(end_index, min_index)) {
                        var new_weight = obj.weight + 1;
                        new_settings[new_weight] = {key: obj.key, weight: new_weight, width: obj.width};
                    } else if (obj.weight == start_index) {
                        var new_weight = Math.max(end_index, min_index);
                        new_settings[new_weight] = {key: obj.key, weight: new_weight, width: obj.width};
                    } else {
                        new_settings[obj.weight] = {key: obj.key, weight: obj.weight, width: obj.width};
                    }
                }
            }
        }
        this.setSettingsObj(new_settings)
    }
};
ChGridForm.prototype.getSettingsObj = function () {
    var storage = Chocolate.storage.local.settings,
        key = this.getView();
    if (typeof(storage[key]) == 'undefined') {
        storage[key] = {};
    }
    return storage[key];
};
ChGridForm.prototype.isCardSupport = function () {
    if (this._is_card_support == null) {
        this._is_card_support = this.$form.attr('data-card-support') == '1';
    }
    return this._is_card_support;
};
ChGridForm.prototype.getActiveRowID = function () {
    return this.getActiveRow().attr('data-id');
};
ChGridForm.prototype.isAutoOpenCard = function () {
    return this.getGridPropertiesObj()['autoOpenCard'];
};
ChGridForm.prototype.openCard = function (pk) {
    if (this.isCardSupport()) {
        var view = this.getView(),
            $tabs = Chocolate.$tabs,
            uniqueID = Chocolate._generateTabID(pk, view),
            $a = $tabs.find('li[data-tab-id=\'' + uniqueID + '\']').children('a');
//    console.log(this.getFmCardsCollection().generateTabs(view,pk, viewID))
        /**
         * Если существует уже открытая закладка, переключаемся на нее, а не добавляем дубликат
         */
        if ($a.length == 0) {
            var viewID = this.getID(),
                caption;
            if (Chocolate.isNumeric(pk)) {
                caption = this.getTabCaption() + ' [' + pk + ']';
            } else {
                caption = this.getTabCaption() + '[новая запись]';
            }
            Chocolate.inserted_tab_counter++;
            var tabTemplate = "<li data-view-id = '" + viewID + "' data-tab-id=" + uniqueID + " data-id =\'" + pk + "\' data-view=\'" + view + "\' >" +
                    "<a id ='_tab" + Chocolate.inserted_tab_counter + "' href='#'>#{label}</a><span class='tab-closed fa fa-times' role='presentation'></span>" + "</li>",
                $li = $(tabTemplate.replace(/#\{label\}/g, caption));


            $tabs.children('ul').append($li);
            $tabs.tabs("refresh");
        var _this = this;
            $a = $li.children('a');
            /**
             * @type {ChTab}
             */
            var chTab = ChObjectStorage.create($a, 'ChTab');

                $tabs.tabs({
                    beforeLoad: function( event, ui ) {
                        ui.jqXHR.abort();
                        var html = _this.getFmCardsCollection().generateTabs(view,pk, viewID);
                        ui.panel.html(html)

                    }
                });

            $tabs.tabs({ active: chTab.getIndex()})
            var href = '#' + chTab.getPanelID(),
                $context = $(href)
            ChocolateDraw.drawCard($context)
          //инициализируем вложенные табы
            Chocolate.initTabs($context)
            $a.attr('href', href)
        } else {
            /**
             * @type {ChTab}
             */
            var chTab = ChObjectStorage.create($a, 'ChTab');
            $tabs.tabs({ active: chTab.getIndex() })
        }
    }

};
ChGridForm.prototype.getCallbackID = function () {
    return this.getUserGridID();
};
/**
 * @returns {jQuery}
 */
ChGridForm.prototype.getGridForm = function () {
    if (this._$grid_form == null) {
        this._$grid_form = this.$form.closest('section');
    }
    return this._$grid_form;
};
/**
 * @returns {String}
 */
ChGridForm.prototype.getSaveUrl = function () {
    if (this._save_url == null) {
        this._save_url = this.$form.attr('data-save');
    }
    return this._save_url;
};
ChGridForm.prototype. getPreviewObj = function () {
    var storage = this.getStorage();
    return storage[this.getID()].preview;
};
/**
 * @param $row {jQuery}
 */
ChGridForm.prototype.selectRow = function ($row, group) {
    var $table = this.getTable();
    $table.find('.row-active').removeClass('row-active');
    $row.addClass('row-active');
    if (!group) {
        $table.find('.row-selected').removeClass('row-selected');
    }
    $row.toggleClass('row-selected');
    if($row.hasClass('row-selected')){

    var preview_data = this.getPreviewObj(),
        id = $row.attr('data-id');
    if (typeof(preview_data[id]) != 'undefined') {
        var info = preview_data[id], html = '';
        for (var key in info) {
            html += '<span class="footer-title">';
            html += key + '</span>: <span>';
            html += info[key];
            html += ' </span><span class="footer-separator"></span>';
        }
        this.getGridForm().find('footer div[data-id=info]').html(html);
    }
    }
};
/**
 * @returns {String}
 */
//ChGridForm.prototype.getParentID = function () {
//    if (this._parent_id == null) {
//        var parent_id = this.this.$form.attr('data-parent-id');
//        if (typeof(parent_id) == 'undefined') {
//            parent_id = '';
//        }
//        this._parent_id = parent_id;
//    }
//    return this._parent_id;
//};
/**
 * @returns {String}
 */
ChGridForm.prototype.getParentView = function () {
    if (this._parent_view == null) {
        var parent_form_id = this.getParentFormID();
        if (parent_form_id) {
            this._parent_view = ChObjectStorage.create($('#' + parent_form_id), 'ChGridForm').getView();
        } else {
            this._parent_view = '';
        }
    }
    return this._parent_view;
};
/**
 * @returns {Object}
 */
ChGridForm.prototype.getStorage = function () {
    if (this._storage == null) {
        var form_id = this.getID(),
            storage = Chocolate.storage.session[form_id];
        if (typeof(storage) == 'undefined' || $.isEmptyObject(storage)) {
            Chocolate.storage.session[form_id] = new Object({data: {}, preview: {}, change: {}, deleted: {}, defaultValues: {}, required: {}});
        }
        this._storage = Chocolate.storage.session;
    }
    return this._storage;
};
ChGridForm.prototype.getRequiredObj = function () {
    var storage = this.getStorage();
    return storage[this.getID()].required;
};
ChGridForm.prototype.getDeletedObj= function () {
    var storage = this.getStorage();
    return storage[this.getID()].deleted;
};
ChGridForm.prototype.getChangedObj = function () {
    var storage = this.getStorage();
    return storage[this.getID()].change;
};
ChGridForm.prototype.isHasChange = function () {
    if (this._isAttachmentsModel()) {
        return ChAttachments.isNotEmpty(this.getID()) || !$.isEmptyObject(this.getDeletedObj());
    } else {
        return !$.isEmptyObject(this.getChangedObj()) || !$.isEmptyObject(this.getDeletedObj());
    }
};
ChGridForm.prototype.getGridPropertiesObj = function () {
    var storage = this.getStorage();
    return storage[this.getID()].gridProperties;
};
ChGridForm.prototype.getDataObj = function () {
    var storage = this.getStorage();
    return storage[this.getID()].data;
};
ChGridForm.prototype.getDefaultObj = function () {
    var storage = this.getStorage();
    return storage[this.getID()].defaultValues;
};
ChGridForm.prototype._resetErrors = function () {
    this.$form.find('td.grid-error').removeClass('grid-error');
};
ChGridForm.prototype.save = function () {
    this._resetErrors();
    var user_grid_id = this.getUserGridID(),
        parent_view = this.getParentView(),
        parent_id = this.getParentPK(),
        ch_messages_container = this.getMessagesContainer(),
        _this = this,
        deleted_obj = $.extend({}, this.getDeletedObj());

    if (!this._isAttachmentsModel()) {
        var change_obj = this.getChangedObj(),
            data_obj = this.getDataObj(),
            response_change_obj = {};

        for (var name in change_obj) {
            if (!$.isEmptyObject(change_obj[name])) {
                if (typeof(deleted_obj[name]) == 'undefined') {
                    response_change_obj[name] = Chocolate.mergeData(data_obj[name], change_obj[name]);
                }
            }
        }

        if (!$.isEmptyObject(response_change_obj) && !$.isEmptyObject(deleted_obj)) {
            for (var row_id in deleted_obj) {
                delete response_change_obj[row_id];
            }
        }

        for (var property in deleted_obj) {
            if (!Chocolate.isNumeric(property)) {
                delete deleted_obj[property];
            }
        }

        if (!$.isEmptyObject(response_change_obj) || !$.isEmptyObject(deleted_obj)) {
            //отсекаем изменения в уже удаленных строках, они нам не нужны
            var errors = [];
            for (var index in response_change_obj) {
                var error = this.validate(response_change_obj[index]);
                if (!$.isEmptyObject(error)) {
                    errors[index] = error;
                }
            }

            if ($.isEmptyObject(errors)) {
                var changed_data = JSON.stringify(response_change_obj),
                    deleted_data = JSON.stringify(deleted_obj),
                    url = this.getSaveUrl() + '&parentView=' + parent_view + '&parentID=' + parent_id,
                    data = {
                        jsonChangedData: changed_data,
                        jsonDeletedData: deleted_data
                    };

                $.post(url, data)
                    .done(function (response) {
                        var ch_response = new ChResponse(response);
                        //TODO: вроде здесь сообщения можно удалять;
//                            ch_response.sendMessage(ch_messages_container);
                        if (ch_response.isSuccess()) {
                            _this.getSaveButton().removeClass('active');
                            _this.refresh();
                        } else {
                            ch_response.sendMessage(ch_messages_container);
                        }
                    })
                    .fail(function (response) {
                        ch_messages_container.sendMessage('Возникла непредвиденная ошибка при сохраненнии сетки.', ChResponseStatus.ERROR);
                    })
            } else {
                for (var pk_key in errors) {
                    for (var column_key in errors[pk_key]) {
                        this.$form.find('a[data-pk=' + pk_key + '][rel=' + user_grid_id + '_' + errors[pk_key][column_key] + ']').closest('td').addClass('grid-error')
                    }
                }
                ch_messages_container.sendMessage('Заполните обязательные поля( ошибки подсвечены в сетке).', ChResponseStatus.ERROR)
            }
        } else {
            ch_messages_container.sendMessage('Данные не были изменены.', ChResponseStatus.WARNING)
        }
    } else {
        var form_id = this.getID();
//        if (ChAttachments.isNotEmpty(form_id)) {

            if (ChAttachments.isNotEmpty(form_id)) {
                while (ChAttachments.isNotEmpty(form_id)) {
                    var dev_obj = this.getDefaultObj();
                    var ownerLock = dev_obj['ownerlock'];
                    var file = ChAttachments.pop(form_id);
                    this.$form.fileupload({
                        formData: {FilesTypesID: 4, OwnerLock: ownerLock}
                    });
                    this.$form.fileupload('send', {files: file})
                }

            this.refresh();
            }
//        }
        else {
            if (!$.isEmptyObject(deleted_obj)) {
                Chocolate.saveAttachment(this);
            } else {
                ch_messages_container.sendMessage('Данные не были изменены.', ChResponseStatus.WARNING);
            }
        }
    }

    return false;
};
ChGridForm.prototype.validate = function (data) {
    var requiredFields = this.getRequiredObj(),
        errors = [];
    for (var field in requiredFields) {
        if (typeof(data[field]) == 'undefined' || !data[field]) {
            errors.push(field);
        }
    }
    return errors;
};
/**
 * @returns {ChFilterForm}
 */
ChGridForm.prototype.getFilterForm =  function () {
    if (this._ch_filter_form == null) {
        this._ch_filter_form = ChObjectStorage.create(this.getGridForm().closest('div').find('section[data-id=filters]').find('form'), 'ChFilterForm');
    }
    return this._ch_filter_form;
};
ChGridForm.prototype.getParentPK = function () {
//    console.log(this.$form.attr('data-parent-pk'), this._parent_pk)
    if (this._parent_pk == null) {
        this._parent_pk = this.$form.attr('data-parent-pk');
    }
    return this._parent_pk;
};
ChGridForm.prototype.getSearchData = function () {
    var ch_filter_form = this.getFilterForm();
//    if(!ch_filter_form.$form.length){
//        return [];
//    }
    var filter_data = ch_filter_form.getData(),
        parent_id = this.getParentPK();
    if (parent_id) {
        filter_data.ParentID = parent_id;
    }
    return filter_data;
};
ChGridForm.prototype.restoreData = function () {
    this.initData(this.getDataObj(), this.getPreviewObj());
};
ChGridForm.prototype.initData = function (data, preview) {
    var $table = this.getTable();
    if (this._isAttachmentsModel()) {
        var tmpl_data = {'files': data},
            content = window.tmpl('template-download', tmpl_data);
        content = content.replace(new RegExp('fade', 'g'), 'fade in');
        var $html = $(content);
        $table
            .find('tbody').html($html)
            .trigger("update")
    } else {
        var $html = this.generateRows(data);
        $table.find('tbody').html($html);
        console.time('start2')
        ChEditableCallback.fire($table, this.getCallbackID());
        console.timeEnd('start2');
        $table.trigger("update")
    }
};
ChGridForm.prototype.updateData = function (data, preview) {
    this.initData(data, preview);
    this.updateStorage(data, preview);

};
ChGridForm.prototype._clearDeletedObj = function () {
    var deletedObj = this.getDeletedObj();
    deletedObj = {};
};
ChGridForm.prototype._clearChangedObj = function () {
    if (this._isAttachmentsModel()) {
        ChAttachments.clear(this.getID());
    } else {

        var changeObj = this.getChangedObj();
        for (var name in changeObj) {
            changeObj[name] = {};
        }
    }
    this.getSaveButton().removeClass('active')
};
ChGridForm.prototype.refresh = function () {
    console.log('refresh')
//        console.log(this._isAttachmentsModel());
    var url = this.getRefreshUrl(),
        parent_view = this.getParentView(),
        search_data = this.getSearchData(),
        ch_messages_container = this.getMessagesContainer(),
        _this = this;
    $.ajax({
        url: url + '&ParentView=' + parent_view,
        type: "POST",
        data: search_data,
        success: function (response) {
            var ch_response = new ChSearchResponse(response);
            var type = _this.getType();
            if (ch_response.isSuccess()) {
                if (type == 'map') {

                    var $map = _this.$form.children('section').children('.map');
                    /**
                     * @type {ChMap}
                     */
                    var ch_map = ChObjectStorage.create($map, 'ChMap');
                    ch_map.refreshPoints(ch_response.getData(), ch_messages_container);

                } else if (type == 'canvas') {
                    var $canvas = _this.$form.find('canvas');
                    /**
                     * @type {ChCanvas}
                     */
                    var ch_canvas = ChObjectStorage.create($canvas, 'ChCanvas');
                    var data = ch_response.getData();
//                            console.log(data)
                    _this.updateStorage(data, ch_response.getPreview());
                    var options = new ChCanvasOptions();
                    ch_canvas.refreshData(data, options);
                }
                else {

                    _this.updateData(ch_response.getData(), ch_response.getPreview());
                    _this._clearDeletedObj();
                    _this._clearChangedObj();
                }
            }
        },
        error: function (xhr, status, error) {
            ch_messages_container.sendMessage(xhr.responseText, ChResponseStatus.ERROR)
        }
    })
};
/**
 * @returns {jQuery}
 */
ChGridForm.prototype._getUserGrid = function () {
    if (this._$user_grid == null) {
        this._$user_grid = this.$form.children('section[data-id=grid]').find('div[data-id=user-grid]')
    }
    return this._$user_grid;
};
ChGridForm.prototype.getUserGridID = function () {
    if (this._user_grid_id == null) {
        this._user_grid_id = this._getUserGrid().attr('id');
    }
    return this._user_grid_id;
};
ChGridForm.prototype.getID = function () {
    if (this._id == null) {
        this._id = this.$form.attr('id');
    }
    return this._id;
};
ChGridForm.prototype.getView = function () {
    if (this._view_id == null) {
        this._view_id = this.$form.attr("data-id");
    }
    return this._view_id;
};
ChGridForm.prototype.getTabCaption = function () {
    if (this._tab_caption == null) {
        this._tab_caption = this.$form.attr('data-tab-caption');
    }
    return this._tab_caption;
};
ChGridForm.prototype.getTable = function () {
    if (this._$table == null) {
        this._$table = this._getUserGrid().find('table');
    }
    return this._$table;
};
ChGridForm.prototype.getFixedTable = function () {
    if (this._$fixed_table == null) {
        this._$fixed_table = this.$form.find('section[data-id=grid]').find('table.floatThead-table');
    }
    return this._$fixed_table;
};
ChGridForm.prototype.getRefreshUrl = function () {
    if (this._refresh_url == null) {
        this._refresh_url = this.$form.attr('data-refresh-url')
    }
    return this._refresh_url
};
ChGridForm.prototype.getSaveButton = function () {
    if (this._$save_btn == null) {
        this._$save_btn = this.$form.find('menu').children('.menu-button-save');
    }
    return this._$save_btn;
};
ChGridForm.prototype.getParentFormID = function () {
    if (this._parent_form_id == null) {
        var parent_id = this.$form.attr('data-parent-id');
        if (typeof(parent_id) == 'undefined') {
            parent_id = '';
        }
        this._parent_form_id = parent_id;
    }
    return this._parent_form_id
};
/**
 * @returns {ChMessagesContainer}
 */
ChGridForm.prototype.getMessagesContainer = function () {
    if (this._ch_messages_container == null) {
        this._ch_messages_container = ChObjectStorage.create(this.$form.find(".messages-container"), 'ChMessagesContainer');
    }
    return this._ch_messages_container;
};
ChGridForm.prototype.isAjaxAdd = function () {
    if (this._is_ajax_add == null) {
        this._is_ajax_add = this.$form.attr("data-ajax-add");
    }
    return this._is_ajax_add;
};
/**
 * Преобразует коллекцию объектов jQuery в упорядоченный список.
 * @returns {Array}
 * @private
 */
ChGridForm.prototype._getChGridColumns = function () {
    var $fixed_table = this.getFixedTable(),
        $column_headers = $fixed_table.find('thead').eq(0).find('tr').eq(0).find('th'),
        sorting_columns = [],
        count_fixed_column = 1;

    $column_headers.each(function (i) {
        if (i >= count_fixed_column) {
            var ch_column = ChObjectStorage.create($($column_headers[i]), 'ChGridColumnHeader');
            sorting_columns.push(ch_column);
        }
    })
    return sorting_columns;
};
ChGridForm.prototype._getColumnTemplates = function () {
    var columns = this._getChGridColumns(),
        templates = [];
    for (var key in columns) {
        /**
         * @type {ChGridColumnHeader}
         */
        var column = columns[key],
            column_key = column.getKey(),
            template = column.getTemplate();

        templates[column_key] = template;
    }
    return templates;
};
/**
 * Добавляет строку в таблицу и возвращает id добавленной строки;
 * @param data
 * @returns {*}
 */
ChGridForm.prototype.addRow = function (data) {
    var templates = this._getColumnTemplates(),
        $table = this.getTable();

    if (typeof(data['id']) == 'undefined') {
        Chocolate.inserted_row_counter++;
        var id = 'ch_row' + Chocolate.inserted_row_counter;
        data['id'] = id;
    }
    var grid_properties = this.getGridPropertiesObj();
    var $row = $(this.generateRow(templates, data, grid_properties));
    $table
        .find('tbody').prepend($row)
        .trigger('addRows', [$row, false])
    $row.addClass('grid-row-changed');
    //            .trigger("update");
    ChEditableCallback.fire($row, this.getCallbackID());
    var row_id = data['id'],
        change_obj = this.getChangedObj(),
        data_obj = this.getDataObj();
    change_obj[row_id] = jQuery.extend({}, data);
    data_obj[row_id] = {};
    if (this.isAutoOpenCard()) {
        this.openCard(row_id);
    }
    return row_id;
};
ChGridForm.prototype._isAttachmentsModel = function () {
    return this.getView().indexOf(Chocolate.attachment_view) != -1 ;
};
ChGridForm.prototype.generateRow = function (templates, data, grid_properties) {
    var style = '';
    if (grid_properties.colorColumnName != null && typeof(data[grid_properties.colorColumnName]) != 'undefined') {
        var decColor = parseInt(data[grid_properties.colorColumnName], 10),
            hexColor = decColor.toString(16);

        if (hexColor.length < 6) {
            while (hexColor.length < 6) {
                hexColor += '0' + hexColor;
            }
        }
        var R = hexColor.charAt(4) + hexColor.charAt(5),
            G = hexColor.charAt(2) + hexColor.charAt(3),
            B = hexColor.charAt(0) + hexColor.charAt(1);
        style = 'style="background:#' + R + G + B + '"';
    }
    var id_class = '';
    if (grid_properties.colorKey != null && typeof(data[grid_properties.colorKey]) != 'undefined' && data[grid_properties.colorKey] != null && data[grid_properties.colorKey].length > 0) {
        id_class = ' td-red';
    }
    var id = data['id'],
        row = '<tr data-id="' + id + '"' + style + '>' + ChGridForm.TEMPLATE_FIRST_TD,
        user_grid_id = this.getUserGridID(),
        is_numeric_key = Chocolate.isNumeric(id);
    for (var key in templates) {
        var value = '';
        if (typeof(data[key]) != 'undefined' && (key != 'id' || is_numeric_key )) {
            value = data[key];
        }
        var class2 = '';
        if (key == 'id') {
            class2 = id_class;
        }
        var rel = user_grid_id + '_' + key;
        if (value) {

            value = value.replace(/"/g, '&quot;')
        }
        row += templates[key].replace(/\{pk\}/g, id)
            .replace(/\{rel\}/g, rel)
            .replace(/\{value\}/g, value)
            .replace(/\{class2\}/g, class2)
    }
    row += '</tr>'

    return row;
};
ChGridForm.prototype.generateRows = function (data) {
    var templates = this._getColumnTemplates(),
        grid_properties = this.getGridPropertiesObj(),
        count = 0,
        stringBuilder = [];
    for (var i in data) {
        count++;
        if (count > 500) {
            break;
        }
        stringBuilder.push(this.generateRow(templates, data[i], grid_properties));
    }
    return stringBuilder.join('');

};
ChGridForm.prototype.getSelectedRows = function () {
    var rows = [];
    this.getTable().find('.row-selected').each(function () {
        rows.push($(this));
    })
    return rows
};
ChGridForm.prototype.removeRows = function ($rows) {
    var lng = $rows.length;
    var deleted_obj = this.getDeletedObj();
    for (var i = 0; i < lng; i++) {
        deleted_obj[$rows[i].attr('data-id')] = true;
        $rows[i].remove();
    }
    this.getTable().trigger("update");
    this.getSaveButton().addClass('active');
};
ChGridForm.prototype.removeRow = function ($table_cell) {
    var $table = this.getTable(),
        $tr = $table_cell.closest('tr'),
        id = $tr.attr('data-id'),
        deleted_obj = this.getDeletedObj();
    deleted_obj[id] = true;
    $tr.remove();
    $table.trigger("update");
    this.getSaveButton().addClass('active');
};
ChGridForm.prototype.saveInStorage = function (data, preview, default_values, required_fields, grid_properties) {
    var storage = this.getStorage();
    storage[this.getID()] = {data: data, preview: preview, change: {}, deleted: {}, defaultValues: default_values, required: required_fields, gridProperties: grid_properties};
};
ChGridForm.prototype.updateStorage = function (data, preview) {
    var storage = this.getStorage();
    storage[this.getID()].data = data;
    storage[this.getID()].preview = preview;
    storage[this.getID()].change = {};
    storage[this.getID()].deleted = {};
};

ChGridForm.prototype.toggleColls = function(cssClass, $thList){

    var hiddenClass = cssClass,
        positions = [],
        $th = $thList,
        $fixedTable = this.getFixedTable(),
        $table = this.getTable();
    var  tables = [$table.eq(0)[0], $fixedTable.eq(0)[0]];
    var sum = 0;
    var curWidth =$table.width();
    var newWidth;
    $th.each(function(i){
        positions.push($(this).get(0).cellIndex);
        sum+=$(this).outerWidth();
    })
    if($fixedTable.hasClass(hiddenClass)){
        ChTableHelper.showColsManyTables(tables, positions);
        newWidth = curWidth+sum;
    $fixedTable.removeClass(hiddenClass);

    }else{
        ChTableHelper.hideColsManyTables(tables, positions)
        newWidth = curWidth-sum;
    $fixedTable.addClass(hiddenClass);
    }
    $table.width(newWidth)
    $fixedTable.width(newWidth)
    $table.floatThead('reflow');
};

ChGridForm.prototype.toggleAllCols = function(){
    var hiddenClass = ChOptions.classes.hiddenAllColsTable,
        $th = this.getFixedTable().find('['+ChOptions.classes.allowHideColumn+']');
    this.toggleColls(hiddenClass, $th);
};
ChGridForm.prototype.toggleSystemCols = function(){
    var hiddenClass = ChOptions.classes.hiddenSystemColsTable,
        $th = this.getFixedTable().find('th').filter(function(index){
            return $.inArray($(this).attr('data-id'), ChOptions.settings.systemCols) !== -1;
        });
    this.toggleColls(hiddenClass, $th);
};
ChGridForm.prototype.getActiveRow = function(){
  return this.getTable().find('.row-active');
};
;function ChCard($grid_tabs) {
    this.$grid_tabs = $grid_tabs;
    this._save_url = null;
    this._$container = null;
    this._key = null;
    this._view = null;
    this._form_id = null;
    this._$grid_form = null;
    this._container_id = null;
    this._$header = null;
    this._$error_container = null;
}
ChCard.prototype = {
    getHeaderContainer: function () {
        if (this._$header == null) {
            this._$header = this.getContainer().children('header');
        }
        return this._$header;
    },
    getErrorContainer: function () {
        if (this._$error_container == null) {
            this._$error_container = this.getHeaderContainer().children('.card-error');
        }
        return this._$error_container;

    },
    getSaveUrl: function () {
        if (this._save_url == null) {
            this._save_url = this.$grid_tabs.attr('data-save-url');
        }
        return this._save_url;
    },
    getContainer: function () {
        if (this._$container == null) {
            this._$container = this.$grid_tabs.parent('div');
        }
        return this._$container;

    },
    getKey: function () {
        if (this._key == null) {
            //TODO: сменить на дата-кей;
            this._key = this.$grid_tabs.attr('data-pk');
        }
        return this._key;
    },
    getView: function () {
        if (this._view == null) {
            this._view = this.$grid_tabs.attr("data-view");
        }
        return this._view;
    },
    getFormID: function () {
        if (this._form_id == null) {
            this._form_id = this.$grid_tabs.attr('data-form-id');
        }
        return this._form_id;
    },
    /**
     *
     * @returns {ChGridForm}
     */
    getGridForm: function () {
        if (this._$grid_form == null) {
            this._$grid_form = ChObjectStorage.create($('#' + this.getFormID()), 'ChGridForm');
        }
        return this._$grid_form
    },
    getContainerID: function () {
        if (this._container_id == null) {
            this._container_id = this.getContainer().attr('id');
        }
        return this._container_id;
    },
    _closeCard: function () {
        var id = this.getContainerID(),
            $li = Chocolate.$content.find('li[aria-controls=' + id + ']');
        Chocolate.closeTab($li);
    },
    getChangedObj: function () {
        var grid_from = this.getGridForm(),
            pk = this.getKey();
        return grid_from.getChangedObj()[pk];
    },
    getDataObj: function () {
        var grid_from = this.getGridForm(),
            pk = this.getKey();
        return grid_from.getDataObj()[pk];
    },
    _isChanged: function () {
        var change_obj = this.getChangedObj();
        return !$.isEmptyObject(change_obj);
    },
    getActualDataObj: function () {
        return Chocolate.mergeData(this.getDataObj(), this.getChangedObj());
    },
    validate: function (data_obj) {
        var grid_form = this.getGridForm(),
            errors = grid_form.validate(data_obj);
        if ($.isEmptyObject(errors)) {
            return true;
        } else {
            this._showErrors(errors)
            return false;
        }
    },
    _showErrors: function (errors) {
        var pk = this.getKey();
        for (var key in  errors) {
            this.$grid_tabs.find('a[rel=' + errors[key] + '_' + pk + ']')
                .closest('div.card-col')
                .children('label').addClass('card-error');
        }
    },
    _resetErrors: function () {

    },
    _clearChangeObj: function () {
        var changed_obj = this.getGridForm().getChangedObj();
        delete changed_obj[this.getKey()];
    },
    save: function () {
        this._resetErrors();
        if (this._isChanged()) {
            var actual_data_obj = this.getActualDataObj();
            if (this.validate(actual_data_obj)) {

                var grid_from = this.getGridForm(),
                    url = this.getSaveUrl(),
                    changed_data = {},
                    _this = this;

                changed_data[this.getKey()] = actual_data_obj;
                var data = {
                    jsonChangedData: JSON.stringify(changed_data),
                    jsonDeletedData: {}
                }
                $.post(url, data)
                    .done(function (response) {
                        var ch_response = new ChResponse(response);
                        if (ch_response.isSuccess()) {
                            _this._clearChangeObj();
                            _this._closeCard();
                            ch_response.sendMessage(grid_from.getMessagesContainer());
                            grid_from.refresh();
                        } else {
                            var ch_messages_container = ChObjectStorage.create(_this.getErrorContainer(), 'ChMessagesContainer');
                            ch_response.sendMessage(ch_messages_container);
                        }
                    })
                    .fail(function (response) {
                        alert("error");
                    })
            }
        } else {
            this._closeCard();
        }
    },
    setElementValue: function ($card_element, value, isEdit, text) {
        $card_element.editable('setValue', value)
        if (!isEdit) {
            $card_element.editable('disable')
        }
        //затык для селект 2
        if (typeof(text) != 'undefined' && text!==null && text.length > 0) {
            $card_element.text(text)
        }
    },
    undoChange: function () {
        var pk = this.getKey();
        if (this._isChanged()) {
            var $table = this.getGridForm().getTable(),
                changed_obj = this.getChangedObj();
            jQuery.each(changed_obj, function (i, val) {
                var $gridCell = $table.find(" a[data-pk=" + pk + "][rel$=" + i + "]"),
                    oldValue = $gridCell.attr("data-value");
                $gridCell.editable("setValue", oldValue, true)
            })
        }
        this._closeCard();
        this._clearChangeObj();
    }
};;var ChocolateDraw = {
    reflowedTabs: [],
    /**
     * @param ch_tab {ChTab}
     */
    _isNeedReflow: function (ch_tab) {
        if (jQuery.inArray(ch_tab.getID(), this.reflowedTabs) != -1) {
            return false;
        }
        return true;
    },
    /**
     * @param ch_tab {ChTab}
     */
    clearReflowedTab: function (ch_tab) {
        var index = jQuery.inArray(ch_tab.getID(), this.reflowedTabs)
        if (jQuery.inArray(ch_tab.getID(), this.reflowedTabs) != -1) {
            delete this.reflowedTabs[index];
        }

    },
    clearReflowedTabs: function () {
        this.reflowedTabs = [];
    },
    /**
     * @param ch_tab {ChTab}
     */
    reflowTab: function (ch_tab) {
        if (ch_tab.isCardTypePanel()) {
            var $context = ch_tab.getPanel();
            if (this._isNeedReflow(ch_tab)) {
                this.drawCard($context)
                this.reflowedTabs.push(ch_tab.getID());
            }
            var ch_card_tab = ChObjectStorage.create($context.find('li.ui-tabs-active').children('a'), 'ChTab');
            if (this._isNeedReflow(ch_card_tab)) {
//                console.log('CARRR')
                var $panel = ch_card_tab.getPanel(),
                    _this = this;
                this.drawCardPanel($panel, $context);
                this.drawCardControls(ch_card_tab.getCardContent());
                $panel.find('div.card-grid').each(function () {
                    var $card_col = $(this).parent();
                    _this.drawCardGrid($card_col)
                    $card_col.find('div[data-id=user-grid]').find('table').each(function(){
                        $(this).floatThead('reflow');
                    })
                })
                this.reflowedTabs.push(ch_card_tab.getID());
            }
        } else {
            if (this._isNeedReflow(ch_tab)) {
                //Для сеток
                var $context = ch_tab.getPanel();
                this.drawGrid($context)
                $context.find('div[data-id=user-grid]').find('table').each(function(){
                    $(this).floatThead('reflow');
                })
                this.reflowedTabs.push(ch_tab.getID());

            }
        }
    },
    _drawContent: function ($context) {
        var windows_height = Chocolate.$window.height(),
            header_height = Chocolate.$header.height(),
            footer_height = Chocolate.$footer.height(),
            pagewrap_height = windows_height - header_height - footer_height;

        $(Chocolate.$pagewrap, Chocolate.$content, Chocolate.$tabs).height(pagewrap_height);
        var $tab_list = Chocolate.$tabs.children('ul').eq(0),
            page_content_delta = $context.outerHeight() - $context.height(),
            page_content_height = pagewrap_height - $tab_list.outerHeight(true) - page_content_delta;

        $context.height(page_content_height);
        return page_content_height;
    },
    _drawGridForm: function ($context) {
        var $content_header = $context.find('section[data-id=header]'),
            $content_filters = $context.find('section[data-id=filters]'),
            $content_grid_form = $context.find('section[data-id=grid-form]'),
            content_grid_form_height = $context.height() - $content_header.outerHeight(true) - $content_filters.outerHeight(true);
        $content_grid_form.height(content_grid_form_height)

        var $form = $content_grid_form.children('form'),
            $footer = $content_grid_form.children('footer'),
            form_height = content_grid_form_height - $footer.outerHeight(true);
        $form.height(form_height);

        var $menu = $form.find('menu'),
            $grid_section = $form.children('section'),
            grid_section_height = form_height - $menu.outerHeight(true);
        $grid_section.height(grid_section_height)
        if ($grid_section.attr('data-id') == 'map') {
            var ch_map = ChObjectStorage.create($grid_section.children('.map'), 'ChMap');
            ch_map.map.container.fitToViewport();

        } else {


            var $user_grid = $grid_section.find('div[data-id=user-grid]');
            $user_grid.height(grid_section_height);
        }
//        $user_grid.children('table').width($user_grid.width())
    },
    /**
     * Полностью рисует сетку, расположенную не в карточке. Можно использовать при ресайзинге.
     * @param $context
     */
    drawGrid: function ($context) {
        var $card_tabs = $context.parent()
        if ($card_tabs.attr('data-id') != 'grid-tabs') {
            /**
             * Перерисовываем внешние контейнеры
             */
            this._drawContent($context)
            /**
             *  Перерисовываем саму сетку.
             */
            this._drawGridForm($context)
        }
    },
    /**
     * Рисует когнтейнер карточки.
     * @param $context
     */
    drawCard: function ($context) {
        this._drawContent($context)
        var $header = $context.children('header'),
            $content = $context.children('div[data-id=grid-tabs]'),
            content_height = $context.height() - $header.outerHeight(true);
        $content.height(content_height)
    },
    /**
     * Рисует одну из панелей в карточке.
     */
    drawCardPanel: function ($panel, $context) {
        var $grid_tabs = $context.children('div[data-id=grid-tabs]'),
            $tab_list = $grid_tabs.children('ul'),
            delta_height = $panel.outerHeight(true) - $panel.height(),
            panel_height = $grid_tabs.height() - $tab_list.outerHeight(true) - delta_height;

        $panel.height(panel_height);
    },
    /**
     * Рисует контролы в карточке. Точнее отрисовывает контейнер в котором они лежат.
     * @param $card_content
     */
    drawCardControls: function ($card_content) {
//        console.log($card_content)
        var $container = $card_content.parent(),
            button_height = 25,
            card_height = $container.height() - button_height - 10;

        $card_content.height(card_height)
        var $card_action_buttons = $container.children('.card-action-button');
        $card_action_buttons.height(button_height)

    },
    /**
     * Рисуем сетку в карточке
     * @param $card_col
     */
    drawCardGrid: function ($card_col) {
        /**
         * Прорисовываем контейнер для сетки
         */
        var $card_grid = $card_col.find('div.card-grid'),
            card_grid_height = $card_grid.height(),
            $section = $card_grid.children('section');
        $section.height(card_grid_height)
        /**
         * Рисуем саму сетку.
         */
        this._drawGridForm($section)
    }

};function ChCanvasOptions() {
//    this.rows = rows;
//    this.colls = colls;
    this.cell_width = 150;
    this.cell_height = 100;

};
/**
 * @param $canvas {jQuery}
 * @constructor
 */
function ChCanvas($canvas) {
    this.$canvas = $canvas;
    //todo: вынести в арбуз
    this.x_key = 'prenumber';
    this.y_key = 'floor';

}
ChCanvas.prototype.prepareData = function (data) {
    var y_key = this.y_key, x_key = this.x_key;
    var data2storage = [];
    for (var i in data) {
        var y = parseInt(data[i][y_key], 10);
        if (typeof(data2storage[y]) == 'undefined') {
            data2storage[y] = [];
        }
        var x = parseInt(data[i][x_key], 10);
        data2storage[y][x] = data[i];
    }
    data2storage = data2storage.reverse()
    data2storage.forEach(function (value, index) {
        data = value.sort(function (a, b) {
            var a_index = parseInt(a[x_key], 10);
            var b_index = parseInt(b[x_key], 10);
            return a_index > b_index ? 1 : -1;
        })

        data2storage[index] = data;
    })
//    console.log(data2storage)
    return data2storage;
}
/**
 * Прорисовка начинает
 * @param data array
 * @param options {ChCanvasOptions}
 */
ChCanvas.prototype.refreshData = function (data, options) {
    data = this.prepareData(data);
    var
//        columns = options.colls,
//        rows = options.rows,
        cell_height = options.cell_height,
        cell_width = options.cell_width;

    var floor_width = 30;
    var canvas = this.$canvas.get(0),
        ctx = canvas.getContext('2d');

    ctx.strokeStyle = '#ededed'; // меняем цвет рамки
    //горизонтальные линии
    var _this = this;

    function wrapText(context, text, marginLeft, marginTop, maxWidth, lineHeight)
    {
        var words = text.split(" ");
        var countWords = words.length;
        var line = "";
        for (var n = 0; n < countWords; n++) {
            var testLine = line + words[n] + " ";
            var testWidth = context.measureText(testLine).width;
            if (testWidth > maxWidth) {
                context.fillText(line, marginLeft, marginTop);
                line = words[n] + " ";
                marginTop += lineHeight;
            }
            else {
                line = testLine;
            }
        }
        context.fillText(line, marginLeft, marginTop);
    }
    function layout(ctx) {
        var colls = 0;
        var rows = 0;
        var colls_counter = 0;

        data.forEach(function (value, index) {
            rows++;
            colls_counter = 0;
            value.forEach(function (value, index) {
                colls_counter++
            })
            colls = Math.max(colls_counter, colls);
        })
        var cnv_height = rows * cell_height;
        var cnv_width = colls * cell_width;
        canvas.height = cnv_height;
        canvas.width = cnv_width + floor_width;
        ctx.beginPath();
//        ctx.strokeStyle = '#ededed'; // меняем цвет рамки
        for (var i = 0; i <rows; i++) {
            var y = (i+1) * cell_height + 10;
            for (var j = 0; j <= colls; j++) {
                var x = j * cell_width + floor_width;
//                ctx.strokeRect(x, y, x + cell_width, y + cell_height);
//                console.log( data[i][j], i, j);
                if(j<colls && typeof(data[i][j]) !='undefined' ){
//                    Рисуем ячейку
                    var cell_data = data[i][j];
                    //ось
                    var axis = cell_data['axis'];
                    ctx.textAlign = "left";
                    ctx.fillStyle ='#06629C';
                    ctx.font = ' 11px Segoe UI, sans-serif';
                    ctx.textBaseline = 'top';
                    ctx.fillText(axis, x+3, y-cell_height, cell_width-3, cell_height)

                    //номер
                    var number = cell_data['prenumber'];
                    ctx.textAlign = "left";
                    ctx.fillStyle ='#06629C';
                    ctx.font = 'bold 13px Segoe UI, sans-serif';
                    ctx.textBaseline = 'top';
                    ctx.fillText(number, x+3, y-cell_height +15, cell_width-3, cell_height)

                    //тип квартиры
                    var type = cell_data['flatstypesname'];
                    var typeInt = parseInt(type, 10);
                    if( cell_data['color']=='White'){
                        if(isNaN(typeInt)){
                            typeInt = 0;
                        }
                        ctx.textAlign = "center";
                        ctx.fillStyle ='#E0E0E0';
                        ctx.font = '48px Segoe UI, sans-serif';
                        ctx.textBaseline = 'middle';
                        ctx.fillText(typeInt, x+cell_width/2 -5, y-cell_height/2 +3, cell_width-3)
                    }
                    ctx.textAlign = "left";
                    ctx.fillStyle ='#06629C';
                    ctx.font = '12px Segoe UI, sans-serif';
                    ctx.textBaseline = 'top';
                    ctx.fillText(type, x+3, y-cell_height +30, cell_width-3, cell_height)

                    //Цена
                    var cost = +cell_data['costcurrent'];
                    cost = cost.toString().replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1 ");
                    ctx.textAlign = "right";
                    ctx.fillStyle ='#06629C';
                    ctx.font = '12px Segoe UI, sans-serif';
                    ctx.textBaseline = 'top';
                    ctx.fillText(cost, x+ cell_width -13, y-cell_height +30, cell_width, cell_height)


                    //площадь
                    var plan_sqr = parseFloat(cell_data['sqrplanreduced']).toFixed(2);
                    if(isNaN(plan_sqr)){
                        plan_sqr = 0;
                    }
                    ctx.textAlign = "right";
                    ctx.fillStyle ='#06629C';
                    ctx.font = '12px Segoe UI, sans-serif';
                    ctx.textBaseline = 'top';
                    ctx.fillText(plan_sqr, x+ cell_width -13, y-cell_height +45, cell_width, cell_height)


                    //площадь кухни
                    var kitchen_sqr = parseFloat(cell_data['sqrplanfullkitchen']).toFixed(2);
                    if(isNaN(kitchen_sqr)){
                        kitchen_sqr = 0;
                    }
                    ctx.textAlign = "right";
                    ctx.fillStyle ='#06629C';
                    ctx.font = '12px Segoe UI, sans-serif';
                    ctx.textBaseline = 'top';
                    ctx.fillText(kitchen_sqr, x+ cell_width -13, y-cell_height +60, cell_width, cell_height)

                    //BL
                    var bl = cell_data['bl'];
                    ctx.textAlign = "left";
                    ctx.fillStyle ='#06629C';
                    ctx.font = 'bold 12px Segoe UI, sans-serif';
                    ctx.textBaseline = 'top';
                    wrapText(ctx, bl, x, y-cell_height +75, cell_width-10)

                    //рисуем рамку(верхний, правый и нижний край)
                    ctx.beginPath()
//                    ctx.strokeStyle  =cell_data['color'];
                    ctx.strokeStyle = 'white';
                    ctx.fillStyle =cell_data['color'];
                    if(cell_data['color']=='White'){
                        ctx.fillStyle = '#E4E4E4'
                    }
                    ctx.lineWidth =1;
//                    ctx.globalAlpha = 0.3;
                    ctx.globalAlpha = 0.3;
                    ctx.moveTo(x + cell_width-10, y-cell_height);
                    ctx.lineTo(x + cell_width-10, y-3);
                    ctx.lineTo(x -5, y-3);
                    ctx.lineTo(x -5, y-cell_height);
                    ctx.lineTo(x + cell_width-10 , y-cell_height);
//                    ctx.lineTo(x + cell_width-5, y-cell_height +20);
//                    ctx.lineTo(x + cell_width -20 -5, y-cell_height +20);
//                    ctx.lineTo(x + cell_width - 20-5, y-cell_height);
//                    ctx.lineTo(x + cell_width-5 , y-cell_height);
                    ctx.fill()
                    ctx.globalAlpha = 1;

//                    ctx.beginPath()
//                    ctx.strokeStyle  =cell_data['color'];
//                    if(cell_data['color']=='White'){
//                        ctx.strokeStyle = '#06629C'
//                    }
////                    ctx.lineWidth =1;
//                    console.log(ctx.strokeStyle)
//                    ctx.moveTo(x -5, y-cell_height);
//                    ctx.lineTo(x + cell_width-5, y-cell_height);
////                    ctx.lineTo(x + cell_width-5, y -10);
//                    ctx.lineTo(x + cell_width-5, y-3);
////                    ctx.lineTo(x -5, y -10);
//                    ctx.lineTo(x -5, y -3);
//                    ctx.stroke();
////                    ctx.fill()

                }
            }


            //рисуем этаж
//            console.log(data[i][0])
            var floor = data[i][0][_this.y_key];
            ctx.textAlign = "left";
            ctx.fillStyle ='#06629C';
            ctx.font = '16px Segoe UI, sans-serif';
            ctx.textBaseline = 'top';
            ctx.fillText(floor, 0, y - cell_height/2-20, 30)

//            ctx.strokeStyle = '#99bce8'; // меняем цвет рамки

            //рисуем этаж
        }


        ctx.stroke();
    }

    layout(ctx);
    var ch_form = ChObjectStorage.create(this.$canvas.closest(('form'), 'ChForm'));
    this.$canvas.unbind('click');
    this.$canvas.on("click", function (e) {
        if(e.offsetX==undefined) // this works for Firefox
        {
            var xpos = e.pageX-$(this).offset().left;
            var ypos = e.pageY-$(this).offset().top;
        }
        else                     // works in Google Chrome
        {
            var xpos = e.offsetX;
            var  ypos = e.offsetY;
        }
        if(xpos >= floor_width){

            var x = ( xpos - floor_width) / cell_width, y =ypos/ cell_height;
            x = x - (x % 1);
            y = y - (y % 1);
//        console.log(x)
//        console.log(y)
            if(typeof( data[y][x])!='undefined'){

            var pk = data[y][x]['id'];
            ch_form.openCard(pk)
            }
        }
    })
}

/**
 *
 * @param options {ChCanvasOptions}
 */
ChCanvas.prototype.init = function (options) {

};
/**
 * @param $elem {jQuery}
 * @constructor
 */
function ChEditable($elem) {
    this.$elem = $elem;
}
ChEditable.prototype = {
    getTitle: function (pk, caption) {
        if (Chocolate.isNumeric(pk)) {
            return caption + ' [' + pk + ']'
        } else {
            return caption;
        }
    }
};/**
 * @param $elem {jQuery}
 * @constructor
 */
function ChTextAreaEditableColumn($elem) {
    ChEditable.apply(this, arguments);
}
ChTextAreaEditableColumn.prototype = Object.create(ChEditable.prototype);
ChTextAreaEditableColumn.prototype.create = function (context, e, allow_edit, name, caption) {
    if (!allow_edit) {
        $(context).unbind('click')
    }
    var $elem = this.$elem, $cell = $elem.parent(),
        ch_column = new ChGridColumnBody($elem),
        $text_modal = $('<a class="grid-textarea"></a>'),
        _this = this;
    $text_modal.appendTo($cell.closest('section'));
    var $btn = $elem.find('div.grid-modal-open');
    if ($btn.length == 0) {
        $btn = $("<div class='grid-modal-open'></div>").appendTo($cell);

        $btn.on('click', function (e) {
            Chocolate.setFocus();
            if (typeof($text_modal.attr('data-init')) == 'undefined') {
                $text_modal.editable({type: 'textarea', mode: 'popup', onblur: 'ignore', savenochange: false, title: _this.getTitle(ch_column.getID(), caption)});
                $text_modal.on('save', function (e, params) {
                    if (allow_edit) {
                        if (typeof(params.newValue) != 'undefined') {
                            ch_column.setChangedValue(name, params.newValue);
                            $elem.editable("setValue", params.newValue);
                            $text_modal.empty();
                        }
                    } else {
                        return false;
                    }
                })
                $text_modal.attr('data-init', 1);
            }

            var value = $elem.editable('getValue')[name];
            value = value.toString()
            $text_modal.editable('setValue', value);
            $text_modal.editable('show');
            if (!allow_edit) {
                $text_modal.next('div').find('textarea').attr('readonly', 'true')
            }
            e.preventDefault()
            return false
        })
    }

};
;
function ChTextAreaEditableCard($elem) {
    ChEditable.apply(this, arguments);
}
ChTextAreaEditableCard.prototype = Object.create(ChEditable.prototype);
ChTextAreaEditableCard.prototype.create = function (context, e, allow_edit, name, caption, isNeedFormat) {
    if (!allow_edit) {
        $(context).unbind('click')
    }
    var $elem = this.$elem, $cell = $elem.parent(),
        ch_card_element = new ChCardElement($cell),
        $text_modal = $('<a class="grid-textarea"></a>'),
        _this = this;
    $text_modal.appendTo($cell.closest('.card-content'));
    var $btn = $elem.find('div.grid-modal-open');
    if ($btn.length == 0) {
        $btn = $("<div class='grid-modal-open'></div>").appendTo($cell);

        $btn.on('click', function (e) {
            Chocolate.setFocus();
            if (typeof($text_modal.attr('data-init')) == 'undefined') {
                $text_modal.editable({type: 'textarea', mode: 'popup', onblur: 'ignore', savenochange: false, title: _this.getTitle(ch_card_element.getID(), caption)});
                $text_modal.on('save', function (e, params) {
                    if (allow_edit) {
                        if (typeof(params.newValue) != 'undefined') {
                            ch_card_element.setChangedValue(name, params.newValue);
                            $elem.editable("setValue", params.newValue);
                            $text_modal.empty();

                        }
                    } else {
                        return false;
                    }
                })
                $text_modal.attr('data-init', 1);
            }

            var value = $elem.editable('getValue')[name];
            if (typeof(value) == 'undefined') {
                value = '';
            }
            if(isNeedFormat){
                value = value.replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1 ");
            }
            value = value.toString()
            $text_modal.editable('setValue', value);
            $text_modal.editable('show');
            if (!allow_edit) {
                $text_modal.next('div').find('textarea').attr('readonly', 'true')
            }
            e.preventDefault()
            return false
        })
    }

};
;function FmCardsCollection(header, headerImage, cards) {
    this.header = header;
    this.headerImage = headerImage;
    this.cards = cards;
    this.templates = [];
    this.templatesNewRow = [];
}
/**
 *
 * @param cardKey
 * @param template
 * @param isNewRow {int}
 */
FmCardsCollection.prototype.setCardTemplate = function(cardKey, template, isNewRow){
    if(isNewRow){
        this.templatesNewRow[cardKey] = template;
    }else{
        this.templates[cardKey] = template;
    }
};
/**
 *
 * @param cardKey
 * @param isNewRow {int}
 * @returns {*}
 */
FmCardsCollection.prototype.getCardTemplate = function(cardKey, isNewRow){
    if(isNewRow){
        if(this.templatesNewRow && typeof( this.templatesNewRow[cardKey]) != 'undefined'){
            return this.templatesNewRow[cardKey];
        }
        return null;
    }else{
        if(this.templates && typeof( this.templates[cardKey]) != 'undefined'){
            return this.templates[cardKey];
        }
        return null;
    }
};
FmCardsCollection.prototype.hasHeader = function () {
    return this.header || this.headerImage;
}
FmCardsCollection.prototype._generateHeader = function () {
    var html = '<header class="card-header">';
    if (this.hasHeader()) {
        html += '<div class="card-top-header"><div class="card-header-left">';
        html += this.headerImage;
        html += '</div><div class="card-header-right">';
        html += this.header;
        html += '</div></div>';
    }
    html += '<div class="card-bottom-header card-error"></div></header>';
    return html;

};
FmCardsCollection.prototype.generateTabs = function (view, pk, viewID) {
    return this._generateHeader() + this._generateList(view, pk, viewID);

};
FmCardsCollection.prototype._generateList = function (view, pk, viewID) {

    var html = '<div data-url="/grid/cardDataGet?view=' + view + '"' + ' data-id="grid-tabs"' +
        'data-view="' + view + '"' +
        'data-pk="' + pk + '"' + 'data-form-id="' + viewID + '"' +
        'data-save-url="/grid/save?view=' + view + '">';
    if (Object.keys(this.cards).length > 1) {
        html += '<ul>';
    } else {
        html += '<ul class="hidden">';
    }
    for( var key in this.cards){
        html += ' <li class="card-tab" data-id="' +key+'"';
        var id = Chocolate.getNewID();
        html +=' aria-controls="' + id + '">';
        html +='<a href="1" title="'+ key +'">'+this.cards[key]['caption']+'</a>';
    }
    html += '</ul></div>';
    return html;
};
;function FmChildGridCollection() {
}
FmChildGridCollection.prototype.templates = [];
FmChildGridCollection.prototype.templatesForNewRow = [];
/**
 *
 * @param template
 * @param isNewRow {int}
 */
FmChildGridCollection.prototype.setCardTemplate = function(view,  ParentView, template, isNewRow){
    if(isNewRow){
        this.templatesForNewRow[view + ParentView] = template;
    }else{
        this.templates[view + ParentView] = template;
    }
};
/**
 *
 * @param isNewRow {int}
 * @returns {*}
 */
FmChildGridCollection.prototype.getCardTemplate = function(view,  ParentView, isNewRow){
    if(isNewRow){
        if(this.templatesForNewRow && typeof( this.templatesForNewRow[view + ParentView]) != 'undefined'){
            return this.templatesForNewRow[view + ParentView];
        }
        return null;
    }else{
        if(this.templates && typeof( this.templates[view + ParentView]) != 'undefined'){
            return this.templates[view + ParentView];
        }
        return null;
    }
};
;var Chocolate = {
    storage: new ObjectStorage(),
    getActiveTabObj: function () {
        return Chocolate.$tabs.children('ul').children('li.ui-tabs-active').children('a');
    },
    isNumeric: function (string) {
        return isFinite(+string);
    },
    setFocus: function () {
        Chocolate.$content.trigger('click');
    },
    _initStorage: function () {
        var settings = {};
        var grid_settings = {};
        if (typeof(this.storage.local.settings) != 'undefined') {
            settings = $.extend({}, this.storage.local.settings);
        }
        if (typeof(this.storage.local.grid_settings) != 'undefined') {
            grid_settings = $.extend({}, this.storage.local.grid_settings);
        }
        this.storage.session = {};
        this.storage.local.settings = settings;
        this.storage.local.grid_settings = grid_settings;
    },
    $window: null,
    $tabs: null,
    $header: null,
    $footer: null,
    $pagewrap: null,
    $content: null,
    formatNumber: function(number){
        return number.replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g, "\$1 ");
    },
    init: function () {
        this.$window = $(window);
        this.$tabs = $('#tabs');
        this.$header = $('#header');
        this.$footer = $('#footer');
        this.$content = $('#content');
        this.$pagewrap = $('#pagewrap');
//        this.storage = new ObjectStorage();
        this._initStorage()
    },
    eng2rus: function (str) {
        replacer = {
            "q": "й", "w": "ц", "e": "у", "r": "к", "t": "е", "y": "н", "u": "г",
            "i": "ш", "o": "щ", "p": "з", "[": "х", "]": "ъ", "a": "ф", "s": "ы",
            "d": "в", "f": "а", "g": "п", "h": "р", "j": "о", "k": "л", "l": "д",
            ";": "ж", "'": "э", "z": "я", "x": "ч", "c": "с", "v": "м", "b": "и",
            "n": "т", "m": "ь", ",": "б", ".": "ю", "/": "."
        };
        for (i = 0; i < str.length; i++) {
            if (replacer[ str[i].toLowerCase() ] != undefined) {
                if (str[i] == str[i].toLowerCase()) {
                    replace = replacer[ str[i].toLowerCase() ];
                } else if (str[i] == str[i].toUpperCase()) {
                    replace = replacer[ str[i].toLowerCase() ].toUpperCase();
                }
                str = str.replace(str[i], replace);
            }
        }
        return str;

    },
    PK_KEY_REG_EXP: /00pk00/g,
    attachment_view: 'attachments.xml',
    inserted_row_counter: 1,
    inserted_tab_counter: 1,
    addTab: function (href, name) {
        Chocolate.inserted_tab_counter++;
        var id = '_tab' + Chocolate.inserted_tab_counter;
        var template_link = '<a id="' + id + '" href="#{href}"><span>{name}</span></a><span class="tab-closed fa fa-times" role="presentation"></span>',
            $tab = $('<li>' + template_link.replace(/\{href\}/g, href).replace(/\{name\}/g, name) + '</li>');
//        Chocolate.inserted_tab_counter++;
        Chocolate.$tabs.children('ul').append($tab)
        Chocolate.$tabs.tabs()
        Chocolate.$tabs.tabs('refresh');
        return $tab;
    },
    getNewID: function(){
        Chocolate.inserted_tab_counter++ ;
        return 'choco' + Chocolate.inserted_tab_counter;
    },
    _generateTabID: function (id, view) {
        return 'card_' + view + '_' + id;
    },
    addTabAndSetActive: function(id, name){
            var tab_index = Chocolate.addTab(id, name).index(),
                $tabs = $('#tabs');
            $tabs.tabs({ active: tab_index })
            $tabs.tabs("refresh");

    },
    initTabs: function ($context) {
        $context.attr('data-type', 'chocolate-card');
        $context.find('div[data-id=grid-tabs]').tabs({
//            load: function (event, ui) {
////                console.log('load')
//                ChCardInitCallback.fireOnce();
//            },
            beforeLoad: function (event, ui) {
                var card = $(this)
//                console.log('before load')
                var li_id = card.parent("div").attr("id")
                var viewID = $("li[aria-controls=" + li_id + "]").attr("data-view-id")
                var tabID = $(ui.tab).attr('data-id');
                var view = card.attr("data-view");
                var pk = card.attr("data-pk");

//                ui.ajaxSettings.url = card.attr("data-url");
//                ui.ajaxSettings.url += "&pk=" + pk + "&tabID=" + encodeURIComponent(tabID) + "&viewID=" + viewID;

                /**
                 * Предотвращает обновление данных, когда данные уже закешированы
                 */
                if (ui.tab.data("loaded")) {

                } else {
                    /**
                     *
                     * @type {ChCard}
                     */
                    var chCard = ChObjectStorage.create(card,'ChCard')
                    var fmCardCollection = chCard.getGridForm().getFmCardsCollection();
                    var isNumeric = +!Chocolate.isNumeric(pk);
                    var template = fmCardCollection.getCardTemplate(tabID, isNumeric);
                    if(template == null){

                        $.get(
                                card.attr("data-url")+ "&tabID=" + encodeURIComponent(tabID) + "&viewID=" + viewID + "&pk=" + pk
                    ).done(function(data){
                                fmCardCollection.setCardTemplate(tabID, data, isNumeric)
//                                console.log(data)
                                data = data.replace(Chocolate.PK_KEY_REG_EXP, pk)
                                ui.panel.html(data)
                                ChCardInitCallback.fireOnce();
                                ChocolateDraw.drawCardPanel(ui.panel, $context)
                                /**
                                 *
                                 * @type {ChTab}
                                 */
                                var chTab =ChObjectStorage.create(Chocolate.getActiveTabObj(), 'ChTab')
                                ChocolateDraw.reflowTab(chTab)
                                ui.tab.data("loaded", 1)
                            })
                    }else{
                        var data = template.replace(Chocolate.PK_KEY_REG_EXP, pk)
//                        console.log(data)
                        ui.panel.html(data)
                        ChCardInitCallback.fireOnce();
                        ChocolateDraw.drawCardPanel(ui.panel, $context)

                        /**
                         *
                         * @type {ChTab}
                         */
                        var chTab =ChObjectStorage.create(Chocolate.getActiveTabObj(), 'ChTab')
                        ChocolateDraw.reflowTab(chTab)
                        ui.tab.data("loaded", 1)


                    }

                }


                event.preventDefault();
                return;


            },
            cache: true
        });
    },
    closeTab: function ($target) {
        var li = $target.closest("li");
        var tab_index = li.attr('tabindex');
        var panelId = li.remove()
            .attr("aria-controls");
        $("#" + panelId).remove();
        var $tab_list = $("#tabs");
        $tab_list.tabs({ active: tab_index + 1 });
        $tab_list.tabs("refresh");
        ChObjectStorage.garbageCollection();
        console.log('close')
    },
    updateAttachment: function ($table_cell) {
        console.log('updateAttach')
        var name = $table_cell.attr('name')
        var parent_id = $table_cell.attr('parent-id')
        var parent_data_id = $table_cell.attr('parent-data-id')
        var data_id = $table_cell.attr('data-id')
        var $input = $('[data-id="' + data_id + '"][name="' + name + '"][parent-data-id="' + parent_data_id + '"][parent-id="' + parent_id + '"]')
        var $tr = $input.closest('tr')
        var $form = $input.closest('form')
        var id = $tr.attr('data-id')
        console.log($input.parent().html())
        $form.fileupload('add', {
            fileInput: $input
        });
        console.log($form.find($input))
//        var ch_form = ChObjectStorage.create($form, 'ChGridForm');
        var ch_from = ChObjectStorage.create($form, 'ChGridForm');
//            new ChGridForm($form);
        ch_from.removeRow($input)
    },
    mergeData: function (original_data, changed_data) {
        return  $.extend(false, original_data, changed_data);
    },
    /**
     *
     * @param ch_form {ChGridForm}
     */
    saveAttachment: function (ch_form) {
        console.log('sa')
        var ch_messages_container = ch_form.getMessagesContainer(),
            deleted_obj = $.extend({}, ch_form.getDeletedObj());
        var deleted_data = JSON.stringify(deleted_obj),
            url = ch_form.getSaveUrl() + '&parentView=' + ch_form.getParentView() + '&parentID=' + ch_form.getParentPK(),
            data = {
                jsonChangedData: {},
                jsonDeletedData: deleted_data
            },
            _this = ch_form;
        if (!$.isEmptyObject(deleted_data)) {

            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                async: false
            })
                .done(function (response) {
                    var ch_response = new ChResponse(response);
                    if (ch_response.isSuccess()) {
                        _this._clearChangedObj();
                        _this._clearDeletedObj()
                        _this.refresh();
                    }
                    ch_response.sendMessage(ch_messages_container);
                })
                .fail(function (response) {
                    ch_messages_container.sendMessage('Возникла непредвиденная ошибка при сохранении вложений.', ChResponseStatus.ERROR);
                })

        }

    },
    parse: function (key, val) {
        if (typeof(val) == 'string') {
            return decodeURIComponent(val)
        } else {
            return val;
        }
    },
    createChildGridTabID: function (parent_id, view, parent_view) {
        return parent_id + "_" + parent_view.replace('.', '_') + '_' + view.replace('.', '_');

    },
    openForm:function(url){
        if (url != '#') {
            $.post(url, []).done(function (response) {
                Chocolate.$tabs.append(response)
            })
        }
    },
    setUser: function (name) {
        Chocolate.storage.session.user = {
            name: name
        }
    },
    getUserName: function () {
        return Chocolate.storage.session.user.name;
    },
    initNavSearch: function(jsonData){
        var autocomplete_data = json_parse(jsonData, Chocolate.parse);
        $('#nav-search').autocomplete({
            delay: 100,
            select: function (event, ui) {
                Chocolate.openForm(ui.item.url);
            },
            source: function (request, response) {
                var searchParam = Chocolate.eng2rus(request.term.toLowerCase())
                function outputItem(item, i, arr) {
                    var source = item.value.toLowerCase();
                    if (source.indexOf(searchParam) != -1) {
                        return true
                    }
                return false
                }
                response(autocomplete_data.filter(outputItem))
            }
        })
    }

};
;var bindingService ={
    binFromParentData: function(sql, data){
        var bindingSql = sql;
        var parentID = data.id;
        bindingSql = sql.replace(/\[parentid\]/i, parentID);
        return bindingSql;

    },
    bindFromData: function(sql, data){
        var bindingSql = sql.replace(/\[id\]/i,  data.id);
        return bindingSql;
    }
};var ChocolateEvents = {
    createEventsHandlers: function () {
        this.initAjaxAnimation();
        this.initCloseTabs();
        this.initGridActions();
        this.initOpeningCardColumn();
        this.initSelectableRow();
        this.initUpdateAttachment();
        this.initDefaultGridMenu();
        this.initAttachmentGridMenu();
        this.initCardCancel();
        this.initAddRowButton();
        this.initSaveGridButton();
        this.initRefreshGridButton();
        this.initCardSaveButton();
        this.initTabActivate();
        this.initResizeWindow();
        this.initTaskWizard();
        this.initCopyright();
        this.disableFilters();
        this.filterOnEnter();
        this.initMenuItems();
        this.downloadAttachments();
        this.initToggleSystemCols();

    },
    initToggleSystemCols: function(){
        Chocolate.$tabs.on('click', '.menu-button-toggle',function() {
            ChObjectStorage.create($(this).closest('form'), 'ChGridForm').toggleSystemCols();
        })
    },
    downloadAttachments: function () {
        Chocolate.$content.on('click', '.attachment-name a', function () {
            return false;
        });
        var download = function download() {
            window.open($(this).attr('href'), '_self');
            return false;

        }
        Chocolate.$content.on('click', '.attachment-name a', $.debounce(2000, true, download));
    },
    initMenuItems: function () {
        $('#footer').on('click', '.link a', function (e) {
            Chocolate.openForm($(this).attr('href'));
            e.preventDefault();
        })
        $('#header').on('click', 'a', function (e) {
            var url = $(this).attr('href');
            if (url != '#') {
                $.post(url, []).done(function (response) {
                    Chocolate.$tabs.append(response)
                })
            }
            e.preventDefault();
        });
    },
    filterOnEnter: function () {
        Chocolate.$content.on('keydown', 'input[type=text].filter', function (e) {
            var enterCode = 13;
            if (e.keyCode == enterCode) {
                /**
                 *
                 * @type {ChGridForm}
                 */
                var ch_form = ChObjectStorage.create($(this).closest('.section-filters').next('.section-grid').children('form'), 'ChGridForm')
                ch_form.refresh();
            }
        })
    },
    initResizeWindow: function () {
        Chocolate.$window.on('resize', $.debounce(300, false, function () {
            ChocolateDraw.clearReflowedTabs();
            ChocolateDraw.reflowTab(ChObjectStorage.create(Chocolate.getActiveTabObj(), 'ChTab'));
        }));
    },
    initTabActivate: function () {
        Chocolate.$tabs.on('click', 'ul.ui-tabs-nav>li>a', function () {
            ChocolateDraw.reflowTab(ChObjectStorage.create(Chocolate.getActiveTabObj(), 'ChTab'));
        });
    },
    initAjaxAnimation: function () {
        var $spinner = $('#fadingBarsG');
        $(document).ajaxStart(function () {
                if (!$spinner.is(':visible')) {
                    $spinner.show();
                }
            }
        )
            .ajaxStop(function () {
                $spinner.hide();
            })
            .ajaxError(function (e) {
                $spinner.hide();
            })
    },
    initCloseTabs: function () {
        Chocolate.$tabs.on('click', '.tab-closed', function () {
            Chocolate.closeTab($(this))
        });
    },
    initGridActions: function () {
        Chocolate.$tabs.on('click', '.menu-button-print, .menu-button-action', function (e) {
//            console.log('click to open')
            $(this).contextmenu('open', $(this))
        })
    },
    initOpeningCardColumn: function () {
        Chocolate.$tabs.on('dblclick', 'span[data-id=card-button]', function (e) {
            var $cell = $(this),
//                ch_form = new ChGridForm($cell.closest('form')),
                ch_form = ChObjectStorage.create($cell.closest('form'), 'ChGridForm'),
                ch_column = new ChGridColumnBody($cell);
            ch_form.openCard(ch_column.getID());
        });
    },
    initSelectableRow: function () {
        Chocolate.$tabs.on('click', 'tr', function (e) {
                var chForm = ChObjectStorage.create($(this).closest('form'), 'ChGridForm');
            chForm.selectRow($(this), e.ctrlKey || e.shiftKey)

        });
        Chocolate.$tabs.on('keydown', '.tablesorter', function (e) {
                var upKey = 38, downKey = 40, delKey = 46;
            if(e.keyCode == delKey){
                /**
                 *
                 * @type {ChGridForm}
                 */
                var chForm = ChObjectStorage.create($(this).closest('form'), 'ChGridForm');
                chForm.removeRows(chForm.getSelectedRows());
                return false;
            }
            if((e.ctrlKey || e.shiftKey) &&(e.keyCode == upKey || e.keyCode == downKey)){
                /**
                 *
                 * @type {ChGridForm}
                 */
            var chForm = ChObjectStorage.create($(this).closest('form'), 'ChGridForm');
            var $activeRow = chForm.getActiveRow(),
                $nextRow;
                if(e.keyCode == downKey){
                    $nextRow = $activeRow.next();
                }else{
                    $nextRow = $activeRow.prev();
                }
                if($nextRow.length){
                    chForm.selectRow($nextRow, true);
                }
            }

        })
    },
    initUpdateAttachment: function () {
        $(document).on('change', 'input[data-input-id=chocolate-upload-hidden]', function (e) {
            if (typeof($(e.target).attr("parent-id")) == "undefined" || $(e.target).attr("parent-id") == '') {
                Chocolate.inserted_row_counter++;
                $(e.target).attr("parent-id", 'chocolate_' + Chocolate.inserted_row_counter);
            }
            Chocolate.updateAttachment($(e.target))
        })
    },
    initDefaultGridMenu: function () {
        Chocolate.$tabs.contextmenu({
            delegate: "span.card-button",
            show: { effect: "blind", duration: 0 },
            menu: [
//                {title: "Открыть карточку", cmd: "open-card", uiIcon: "ui-icon-newwin"},
                {title: "Удалить", cmd: "delete", uiIcon: "ui-icon-trash"}
            ],
            select: function (event, ui) {
                var $target = ui.target
                switch (ui.cmd) {
                    case 'delete':
                        var ch_form = ChObjectStorage.create($target.closest('form'), 'ChGridForm');
//                            new ChGridForm($target.closest('form'));

                        ch_form.removeRows(ch_form.getSelectedRows());
                        break;
//                    case 'open-card':
//                        var ch_form = ChObjectStorage.create($target.closest('form'), 'ChGridForm'),
//                            ch_column = new ChGridColumnBody($target);
////                            ch_form = new ChGridForm($target.closest('form')),
//                        ch_form.openCard(ch_column.getID());
//                        break;
                    default :
                        alert('неизвестная команда')
                        break
                }
            }
        });
    },
    initAttachmentGridMenu: function () {
        $(document).contextmenu({
            delegate: "td.attachment-grid-menu",
            show: { effect: "blind", duration: 0 },
            menu: [
                {title: "Обновить вложение", cmd: "attachment-update", uiIcon: "ui-icon-newwin"},
                {title: "Удалить", cmd: "delete", uiIcon: "ui-icon-trash"}

            ],
            select: function (event, ui) {
                var $target = ui.target
                switch (ui.cmd) {
                    case 'delete':
//                        var ch_form = new ChGridForm($target.closest('form'));
                        var ch_form = ChObjectStorage.create($target.closest('form'), 'ChGridForm');
                        ch_form.removeRow($target);
                        break;
                    case 'attachment-update':
                        var tr = $target.closest('tr')
                        var update_button = tr.find('input[data-input-id=chocolate-upload-hidden]')
                        update_button.trigger('click')
                        break;
                    default :
                        alert('неизвестная команда')
                        break
                }
            }
        });
    },
    initCardCancel: function () {
        Chocolate.$tabs.on('click', 'input[data-id=card-cancel]', function () {
            var ch_card = ChObjectStorage.create($(this).closest('div[data-id=grid-tabs]'), 'ChCard');
//                new ChCard($(this).closest('div[data-id=grid-tabs]'));
            ch_card.undoChange();
        })
    },
    initAddRowButton: function () {
        Chocolate.$tabs.on('click', '.menu-button-add', function (e) {
            var ch_form = ChObjectStorage.create($(this).closest('form'), 'ChGridForm');
            if (ch_form.isAjaxAdd()) {
                var url = ChOptions.urls.addRow,
                    view = ch_form.getView();
                $.ajax({
                    method: "GET",
                    url: url,
                    cache: false,
                    data: {view: view},
                    success: function (response) {
                        //TODO: возможно существуют ошибки, не проверялось
                        var ch_response = new ChResponse(response);
                        if (ch_response.isSuccess()) {
                            ch_form.addRow(ch_response.getData());
                        } else {
                            ch_response.sendMessage(ch_form.getMessagesContainer());
                        }
                    },
                    error: function (xhr, status, error) {
                        ch_form.getMessagesContainer().sendMessage(xhr.responseText, ChResponseStatus.ERROR);
                    }
                })
            } else {
                var form_id = ch_form.getID(),
                    data = jQuery.extend({}, ch_form.getDefaultObj()),
                    row_id = ch_form.addRow(data);

            }
        })
    },
    initSaveGridButton: function () {
        Chocolate.$tabs.on('click', '.menu-button-save', function () {
            var ch_form = ChObjectStorage.create($(this).closest('form'), 'ChGridForm');
//                new ChGridForm($(this).closest('form'));
            ch_form.save();
        });
    },
    initCardSaveButton: function () {
        Chocolate.$tabs.on('click', '.card-save', function (e) {

            Chocolate.setFocus();
            /**
             *
             * @type {ChCard}
             */
            var chCard = ChObjectStorage.create($(this).closest('div[data-id=grid-tabs]'), 'ChCard');
            chCard.save();
        })
    },
    initRefreshGridButton: function () {
        Chocolate.$tabs.on('click', '.menu-button-refresh', function (e) {
            var chForm = ChObjectStorage.create($(this).closest('form'), 'ChGridForm');
            if (chForm.isHasChange()) {
                var $dialog = $('<div>Сохранить изменения?</div>');
                $dialog.dialog({
                    title: 'Апельсин',
                    dialogClass: 'wizard-dialog',
                    resizable: false,
                    height: 140,
                    modal: true,
                    buttons: {
                        'Да': function () {
                            $(this).dialog("close");
                            chForm.save();
                        },
                        'Нет': function () {
                            $(this).dialog("close");
                            chForm.refresh();
                        },
                        'Отмена': function () {
                            $(this).dialog("close");
                        }
                    },
                    create:function () {
                        var $buttons =$(this).siblings('div').find("button")
                            $buttons.first()
                            .addClass("wizard-next-button")
                                .nextAll().
                                addClass('wizard-cancel-button');
                    }
                });
            } else {
                chForm.refresh();
            }
        });

    },
    initTaskWizard: function () {
        $('body').on('click', 'section[data-id=header] a[href$=TasksWizard]', function () {
//            var ch_form = new ChGridForm($(this).closest('section').parent().find("section[data-id=grid-form]>form"));
            var ch_from = ChObjectStorage.create($(this).closest('section').parent().find("section[data-id=grid-form]>form"), 'ChGridForm');
//                new ChGridForm($(this).closest('section').parent().find("section[data-id=grid-form]>form"));
            var service_step = new SelectServiceTaskStep(),
                executor_step = new SelectExecutorsTaskStep(),
                description_step = new SelectDescriptionTaskStep(),
                task_wizard = new TaskWizard(ch_from);
            task_wizard.enqueue(service_step);
            task_wizard.enqueue(executor_step);
            task_wizard.enqueue(description_step);
            task_wizard.run();
            return false;
        })
    },
    initCopyright: function () {
        $(window).on('keydown', function (e) {
            var escape_key = 8;
            if (e.keyCode == escape_key && !(e.target.tagName == 'INPUT' || e.target.tagName == 'TEXTAREA')) {
                e.preventDefault();
            }
        })
        $('body').on('keydown', 'textarea', function (e) {

            var f4_code = 115;
            if (e.keyCode == f4_code) {
                var $elem = $(e.target),
                    date = new Date(),
                    copyright = ' ' + Chocolate.getUserName() + ' ' + date.format("dd.mm.yyyy HH:MM") + ' ';
                $elem.insertAtCaret(copyright);
                e.preventDefault();
                return false
            }
        })
    },
    disableFilters: function () {
        Chocolate.$content.on('click', '.section-filters div>label', function (e) {
            $(this).siblings('select, input').prop("disabled", function (i, val) {
                return !val;
            });
        })
    }
}
;var chCardFunction = {
    select2AjaxInitSelection: function(element, callback, sql){
        //TODO: не сделана подствека анимации в случаи долгой загрузки
        var $elem =  element.data().select2.opts.element.closest('.table-td').children('a'),
            elem = $elem.get(0),
            data = jQuery.data(elem,'data-loaded'),
            cardElement = new ChCardElement($elem),
            bindSql = bindingService.bindFromData(sql, cardElement.getCard().getDataObj());

        if(typeof(data) =='undefined'){
            data = [];
            $.ajax(
                MajesticVars.EXECUTE_URL,
                {
                    async: false,
                    data: {sql: bindSql},
                    success: function(reponse){
                        var chResponse = new ChGridResponse(reponse),
                            resData =  chResponse.getData();
                        for(var i in resData){
                            data.push({
                                id: resData[i].id,
                                text:resData[i].data.name
                            });
                        }
                        jQuery.data(elem,'data-loaded', data)
                    },
                    error: function(){
                        jQuery.data(elem,'data-loaded', data)
                    }
                }
            );
        }
        var result = [];
        $(element.val().split(",")).each(function () {
            var id = this;
            if (id.length) {
                var text = "";
                $(data).each(function () {
                    if (this.id == id) {
                        text = this.text;
                        return false;
                    }
                });
                result.push({id: id, text: text});
            }
        });
        callback(result);



    },
    select2AjaxDataFunc: function(query, $context){
        var elem = $context[0].element.closest('.table-td').children('a').get(0),
            data = {results: []};

        $.each(jQuery.data(elem,'data-loaded'), function(){
            if(query.term.length == 0 || this.text.toUpperCase().indexOf(query.term.toUpperCase()) >= 0 ){
                data.results.push({id: this.id, text: this.text });
            }
        });
        query.callback(data);
    },
    defaultValidateFunc: function ($context, value) {
        var $error = $context.closest(".card-col").children("label");
        if ($.trim(value) == "") {
            $error.addClass("card-error");
        } else {
            $error.removeClass("card-error");
        }
    },
    select2ColumnSaveFunction: function (e, params, name) {
        var jCell = $(e.target),
            chColumn = new ChGridColumnBody(jCell),
            new_value = "";
        if (params.newValue.length > 0 && params.newValue[0].indexOf(",") != -1) {
            //отсекаем первый элемент, тк это какой- то левак
            delete params.newValue[0];
        }
        for (var i in params.newValue) {
            new_value += params.newValue[i] + "|";
        }

        jCell.editable("setValue", params.newValue);
        chColumn.setChangedValue(name, new_value);
    },
    select2ColumnInitFunction: function (element, callback) {
        var data = [],
            sql_data = element.data().select2.opts.data;
        $(element.val().split(",")).each(function (i) {
            if (this != "") {
                var option_name = '';
                for (var i in sql_data) {
                    var el = sql_data[i];
                    if (el.id == this) {
                        option_name = el.text;
                        break;
                    }
                }
                data.push({id: this, text: option_name });
            }
        });

        callback(data);
    },
    defaultSaveFunc: function (e, params, name) {
        var $target = $(e.target);
        /**
         *
         * @type {ChCardElement}
         */
        ChObjectStorage.create($target, 'ChCardElement')
            .setChangedValue(name, params.newValue)
            .setChangedValueInGrid(name, params.newValue, $target.text());
    },
    dateSaveFunc: function (e, params, name) {
        var $target = $(e.target);
        /**
         *
         * @type {ChCardElement}
         */
        var dtValue = params.newValue.format(ChOptions.settings.formatDate);
        ChObjectStorage.create($target, 'ChCardElement')
            .setChangedValue(name, dtValue)
            .setChangedValueInGrid(name, params.newValue);
    },
    textAreaInitFunc: function (e, editable, attribute, isAllowEdit, caption, isNeedFormat, context) {
        var jCell = $(context);
        /**
         *
         * @type {ChCardElement}
         */
        var chCardElement = ChObjectStorage.create(jCell, 'ChCardElement'),
            chCard = chCardElement.getCard(),
            value = chCard.getActualDataObj()[attribute];
        if (isNeedFormat && value) {
            value = Chocolate.formatNumber(value);
        }
//        console.log(value, value ===null)
        if (value === null) {
            value = '';
        }
        ChCardInitCallback.add(function () {
            chCard.setElementValue(jCell, value, isAllowEdit);
            var ch_column = new ChTextAreaEditableCard(editable['$element']);
            ch_column.create(context, e, isAllowEdit, attribute, caption, isNeedFormat);
        });
    },
    checkBoxDisplayFunction: function (value, $context) {
        if (typeof(value) != 'undefined' && value) {
            $context.text('да');
        } else {
            $context.text('нет');
        }
    },
    checkBoxInitFunction: function ($context, attribute, isAllowEdit) {
        /**
         *
         * @type {ChCardElement}
         */
        var chCardElement = ChObjectStorage.create($context, 'ChCardElement');
        var chCard = chCardElement.getCard(),
            value = chCard.getActualDataObj()[attribute];
        $context.unbind('click');
        if (isAllowEdit) {
            $context.on('click', function () {
                var val = $context.editable('getValue');
                if ($.isEmptyObject(val)) {
                    val = 1
                } else {
                    val = +!val[attribute]
                }
                $context.editable('setValue', val);
                chCardElement
                    .setChangedValue(attribute, val)
                    .setChangedValueInGrid(attribute, val, $context.text());
            })
        }
        ChCardInitCallback.add(function () {
            chCard.setElementValue($context, value, isAllowEdit);
        })
        ;
    },
    dateInitFunction: function ($context, attribute, isAllowEdit) {
        /**
         *
         * @type {ChCardElement}
         */
        var chCardElement = ChObjectStorage.create($context, 'ChCardElement');
        var chCard = chCardElement.getCard(),
            value = chCard.getActualDataObj()[attribute],
            dtValue;

        if (value && typeof(value) == 'string') {
            value = value.replace(/\./g, '/')
//            var ss = new Date(value);
//             dtValue = new Date(ss.format('yyyy.mm.dd'));
            dtValue = new Date(value);
//            console.log(value, dtValue, ss)
        } else {
            dtValue = value;
        }
//        console.log(dtValue, attribute)
        ChCardInitCallback.add(function () {
            chCard.setElementValue($context, dtValue, isAllowEdit);
        })

    },
    select2SaveFunction: function (e, params, attribute) {

        var $context = $(e.target);
        /**
         *
         * @type {ChCardElement}
         */
        var chCardElement = ChObjectStorage.create($context, 'ChCardElement');

        var new_value = "";
        for (var i in params.newValue) {
            if (params.newValue[i]) {
                new_value += params.newValue[i] + "|";
            }
        }
        $context.editable('setValue', new_value);

        chCardElement
            .setChangedValue(attribute, new_value)
            .setChangedValueInGrid(attribute, params.newValue, $context.text());
    },
    select2GetParentDataSource: function (element, name) {
        var chCardElement = ChObjectStorage.create(element, "ChCardElement");
        return chCardElement.getParentElement(name).data().editable.options.source;
    },
    select2GetDataSource: function (formID, pk, name) {
        return $('#' + formID).find("a[data-pk=" + pk + "][rel$=" + name + "]").data().editable.options.source;
    },
    select2InitSelectionFunction: function (element, callback, name) {
        var parentData = chCardFunction.select2GetParentDataSource(element, name);
        var data = [];
        $(element.val().split(",")).each(function () {
            var id = this;
            if (id.length) {
                var text = "";
                $(parentData).each(function () {
                    if (this.id == id) {
                        text = this.text;
                        return false;
                    }
                });
                data.push({id: id, text: text});
            }
        });
        callback(data);
    },
    select2ShownFunction: function (editable) {
        if (typeof(editable) != "undefined") {
            editable.formOptions.input.postrender = function () {
                editable.$form.find(".select2-search-field input").focus()
            }
        }
    },
    select2SortFunction: function (results, container, query) {
        if (term != "") {
            var term = query.term.toLowerCase();
            results = results.sort(function (a, b) {
                if (a.text.toLowerCase().indexOf(term) == 0) {
                    return -1;
                }
                if (b.text.toLowerCase().indexOf(term) == 0) {
                    return 1
                }
                return 0;
            })
        }
        return results;
    },
    select2InitFunction: function ($context, attribute, isAllowEdit, titleKey, editable, caption) {
        /**
         *
         * @type {ChCardElement}
         */
        var chCardElement = ChObjectStorage.create($context, 'ChCardElement');
        var chCard = chCardElement.getCard(),
            actualDataObj = chCard.getActualDataObj(),
            changeObj = chCard.getChangedObj(),
            value = actualDataObj[attribute],
            html;
        if (typeof(value) == 'undefined' || value === null) {
            value = '';
        }
        var prepareValue = value.split('|');
        if (typeof(changeObj) == 'undefined') {
            html = actualDataObj[titleKey];
        } else {
            var data = chCardFunction.select2GetParentDataSource($context, attribute);
            html = '';
            $(data).each(function () {
                if ($.inArray(this.id, prepareValue) != -1) {
                    html += this.text + '/';
                    return false;
                }
            });
        }
        ChCardInitCallback.add(function () {
            chCard.setElementValue($context, prepareValue, isAllowEdit, html);
            chFunctions.select2ColumnInitFunc($context, caption )
        });
    },
    selectInitFunction: function ($context, attribute, isAllowEdit) {
        /**
         *
         * @type {ChCardElement}
         */
        var chCardElement = ChObjectStorage.create($context, 'ChCardElement');
        var chCard = chCardElement.getCard(),
            value = chCard.getActualDataObj()[attribute];

        ChCardInitCallback.add(function () {
            chCard.setElementValue($context, value, isAllowEdit);
        });
    },
    multimediaInitFunction: function (pk, sql, formID, url, id) {
        /**
         *
         * @type {ChGridForm}
         */
        var chGridForm = ChObjectStorage.create($('#' + formID), 'ChGridForm'),
            sql = bindingService.binFromParentData(sql, chGridForm.getDataObj()[pk]);
        $.get(MajesticVars.EXECUTE_URL, {sql: sql})
            .done(function (response) {
                var chResponse = new ChResponse(response);
                var data = chResponse.getData();
                if (data.length) {
                    var $context = $('#' + id), imagesHtml = '';
                    for (var i in data) {
                        var imageUrl = url + '?fileID=' + data[i].id;
                        if (i == 0) {
                            imagesHtml += '<a class="fancybox multimedia-main-image" rel="gallery"><img src="' + imageUrl + '"></img></a>';
                        } else {
                            imagesHtml += '<a class="fancybox multimedia-image" rel="gallery" style="display:none"><img src="' + imageUrl + '"></img></a>';
                        }
                    }
                    $context.append(imagesHtml)
                    $context.find('.fancybox').fancybox({
                        live: false,
                        maxWidth: 800,
                        maxHeight: 600,
                        closeClick: false,
                        openEffect: 'none',
                        closeEffect: 'none'
                    });
                }
            })
            //TODO: обработчик ошибки при получении данных
            .fail(function () {
            })

    }
};var chFunctions = {
//    validateColumn: function (value) {
//        if ($.trim(value) == "") {
//            return "Это поле обязательно для заполнения";
//        }
//
//    },
    select2ColumnInitFunc: function($context, caption){
        var chEditable = new ChEditable($context);
       $context.attr('data-original-title', chEditable.getTitle($context.attr('data-pk'), caption));

    },
    selectColumnInitFunc: function($context, isAllowEdit){
        if(!isAllowEdit){ $context.unbind('click')}
    },
    defaultColumnSaveFunc: function(e, params, name){
        var chColumn = new ChGridColumnBody($(e.target));
        chColumn.setChangedValue(name, params.newValue);
    },
    dateColumnInitFunction:function($context, isAllowEdit){
        if(!isAllowEdit){ $context.unbind('click').unbind('mouseenter')}
    },
    dateColumnSaveFunction: function(e, params, name){
        var chColumn = new ChGridColumnBody($(e.target));
        chColumn.setChangedValue(name, params.newValue.format(ChOptions.settings.formatDate));
    },
    treeOnQuerySelect: function (flag, node) {
        if (node.childList == null) {
            return true;
        } else {
            for (var i in node.childList) {
                node.childList[i].select(flag);
            }
        }
        return true
    },
    initPrintActions: function (id, jsonPrintActions) {
        var $actionButton = $('#' + id);
        /**
         *
         * @type {ChGridForm}
         */
        var chForm = ChObjectStorage.create($actionButton.closest('form'), 'ChGridForm'),
            rexExp = new RegExp('\[IdList\]');

        $actionButton.contextmenu({
            show: { effect: "blind", duration: 0 },
            menu: json_parse(jsonPrintActions),
            select: function (event, ui) {
                var url = ui.cmd;
                if (rexExp.test(url)) {
                    var idList = '',
                        rows = chForm.getSelectedRows(),
                        lng = rows.length;
                    for (var i = 0; i < lng; i++) {
                        idList += rows[i].attr('data-id') + ' ';
                    }
                    url = url.replace(/\[IdList\]/g, idList);
                }
                window.open(url);
            }
        })
    },
    initActions: function (id, jsonActions) {
        var $actionButton = $('#' + id);
        /**
         *
         * @type {ChGridForm}
         */
        var chForm = ChObjectStorage.create($actionButton.closest('form'), 'ChGridForm');
        $actionButton.contextmenu({
            show: { effect: "blind", duration: 0 },
            menu: json_parse(jsonActions),
            select: function (event, ui) {
                switch (ui.cmd) {
                    case 'window.print':
                        window.print();
                        break;
                    case 'ch.export2excel':
                        var data = {
                            data: $.extend(true, chForm.getDataObj(), chForm.getChangedObj()),
                            view: chForm.getView(),
                            settings: chForm.getSettingsObj()
                        };
                        $.fileDownload(
                            ChOptions.urls.export2excel,
                            {
                                httpMethod: "POST",
                                data: data
                            }
                        );
                        break;
                    case 'ch.settings':
                        var $dialog = $('<div></div>'),
                            $content = $('<div class="grid-settings"></div>'),
                            $autoUpdate = $('<div class="setting-item"><span class="setting-caption">Автоматические обновление данных(раз в 100 секунд)</span></div>'),
                            $input = $('<input type="checkbox">')
                        /**
                         *
                         * @type {ChFormSettings}
                         */
                        var chFormSettings = chForm.ch_form_settings;
                        if (chFormSettings.isAutoUpdate()) {
                            $input.attr('checked', 'checked');
                        }

                        $autoUpdate.append($input);
                        $content.append($autoUpdate)
                        $dialog.append($content)
                        $dialog.dialog({
                            resizable: false,
                            title: 'Настройки',
                            dialogClass: 'wizard-dialog',
                            modal: true,
//                            height: 500,
//                            width: 700,
                            buttons: {
                                OK: {
                                    text: 'OK',
                                    class: 'wizard-active',
                                    click: function (bt, elem) {
                                        chFormSettings.setAutoUpdate($input.is(':checked'));
                                        $(this).dialog("close");
                                        $(this).remove();
                                    }},
                                Отмена: {
                                    text: 'Отмена',
                                    class: 'wizard-cancel-button',
                                    click: function () {
                                        $(this).dialog("close");
                                        $(this).remove();
                                    }
                                }

                            }
                        });
                        $dialog.dialog('open')
                        break;

                }
            }
        });
    },
    initGrid: function (jsonData, jsonPreview, jsonDefault, jsonRequired, jsonGridProperties, formID, header, headerImg, jsonCardCollection) {
        /**
         * @type {ChGridForm}
         */
        var chForm = ChObjectStorage.create($('#' + formID), 'ChGridForm');
        chForm.saveInStorage(
            json_parse(jsonData, Chocolate.parse),
            json_parse(jsonPreview),
            json_parse(jsonDefault),
            json_parse(jsonRequired),
            json_parse(jsonGridProperties)
        );
        chForm.setFmCardsCollection(
            new FmCardsCollection(header, headerImg, json_parse(jsonCardCollection, Chocolate.parse))
        );
    },
    initCardGrid: function (jsonDefault, jsonRequired, jsonGridProperties, formID, header, headerImg, jsonCardCollection, view, parentView, parentID, sql) {
        /**
         * @type {ChGridForm}
         */
        var chForm = ChObjectStorage.create($('#' + formID), 'ChGridForm');
        chForm.saveInStorage(
            {},
            {},
            json_parse(jsonDefault),
            json_parse(jsonRequired),
            json_parse(jsonGridProperties)
        );

        chForm.setFmCardsCollection(
            new FmCardsCollection(header, headerImg, json_parse(jsonCardCollection, Chocolate.parse))
        );

//        var url =ChOptions.urls.gridExecute + '?view='+encodeURIComponent(view)
//            + '&parentView='+ encodeURIComponent(parentView)
//            + '&parentID=' + parentID;
        var params = {
            sql: sql,
            view: view,
            parentView: parentView,
            parentID: parentID
        };
        var ajaxTask = new ChAjaxTaks(formID, 'ChGridForm', params);
        chAjaxQueue.enqueue(ajaxTask);
//        var data ={sql:sql};
//        $.post(url, data).done(function(response){
//            var ch_response = new ChSearchResponse(response);
//            chForm.updateData(ch_response.getData(), ch_response.getPreview());
//        })
    }
};
var MajesticVars = {
    EXECUTE_URL: '/majestic/execute',
    params: [],
    get: function (name, form_id) {
        return this.params[form_id][name.toLowerCase()];
    },
    set: function (name, value, form_id) {
        this.params[form_id][name.toLowerCase()] = value;
    },
    clear: function (form_id) {
        this.params[form_id] = [];
    },
    remove: function (name, form_id) {
        delete this.params[form_id][name.toLowerCase()];
    }
};/**
 * Абстрактныый метод для метода CoreScript
 * @param code
 * @param ch_form {ChGridForm}
 * @constructor
 */
function MajesticMethod(code, ch_form) {
    this.code = code;
    this.ch_form = ch_form;
    this.set_expression_separator = '=';
    this.params_separator = ' ';
    this.params_prefix = '@';

};
MajesticMethod.prototype = {
    run: function () {
        throw "Необходимо реализовать функцию run для метода";
    },
    getParams: function () {
        var params = this.code.split(this.params_separator);

        //удаляем имя метода
        params.shift();
        return params;
    }

};;function ClearVarsMethod(code, ch_form) {
    MajesticMethod.apply(this, arguments);
}
ClearVarsMethod.prototype = Object.create(MajesticMethod.prototype);
ClearVarsMethod.prototype.run = function () {
    MajesticVars.clear(this.ch_form.getID());
};function SetVarMethod(code, ch_form) {
    MajesticMethod.apply(this, arguments);
}
SetVarMethod.prototype = Object.create(MajesticMethod.prototype);
SetVarMethod.prototype.run = function () {
    var parts = this.code.split(MajesticMethod.set_expression_separator),
        name = jQuery.trim(parts[0]),
        method = jQuery.trim(parts[1]),
        mjMethod = MajesticMethodFactory.create(method, this.ch_form),
        value = mjMethod.run();
    MajesticVars.set(name, value, this.ch_form.getID());
};
function GetActiveRowIDMethod(code, ch_form) {
    MajesticMethod.apply(this, arguments);
}

GetActiveRowIDMethod.prototype = Object.create(MajesticMethod.prototype);
GetActiveRowIDMethod.prototype.run = function () {
    var id = this.ch_form.getActiveRowID();
    if (typeof(id) == 'undefined') {
        id = null;
    }
    return id;
}
;
function RemoveVarMethod(code, ch_form) {
    MajesticMethod.apply(this, arguments);
}
RemoveVarMethod.prototype = Object.create(MajesticMethod.prototype);
RemoveVarMethod.prototype.run = function () {
    var param_name = this.getParams()[0];
    MajesticVars.remove(param_name, this.ch_form.getID());
};
var MajesticMethodFactory = {
    /**
     * @param ch_form {ChGridForm}
     * @param code
     * @returns {MajesticMethod}
     */
    create: function (code, ch_form) {
        var code = jQuery.trim(code);
        if (this._isSetExpression()) {
            return new SetVarMethod(code, ch_form);
        } else {
            switch (code.toLowerCase()) {
                case 'vars.clear':
                    return new ClearVarsMethod(code, ch_form);
                    break;
                case 'dataform.currentrowid':
                    return new GetActiveRowIDMethod(code, ch_form);
                    break;
                case 'vars.remove':
                    return new RemoveVarMethod(code, ch_form)
                    break;

            }

        }
        return this.code;
    },
    _isSetExpression: function () {
        return this.code.indexOf(MajesticMethod.set_expression_separator) != -1;
    }
};;/**
 * @param ch_form {ChGridForm}
 * @param code
 * @constructor
 */
function MajesticExpression(code, ch_form) {
    this.code = code;
    this.ch_form = ch_form;
}
MajesticExpression.prototype = {
    run: function () {
        var method = MajesticMethodFactory.create(this.code, ch_form);
        method.run();
    }
};/**
 * Интерпретатор core script из vb6(КИС)
 * @param code
 * @param ch_form {ChGridForm}
 * @constructor
 */
function MajesticInterpreter(code, ch_form) {
    this.expressions = [];
    this.code = code;
    this.ch_form = ch_form;
    this.sequence_separator = ';';
}
MajesticInterpreter.prototype = {
    interpret: function () {
        var queue = [],
            ch_from = this.ch_form;
        var expessions = this.code.split(this.sequence_separator);
        expessions.forEach(function (code) {
            var mjExpression = new MajesticExpression(code, ch_from);
            queue.push(mjExpression);

        });
        return queue;
    },
    run: function () {
        var queue = this.interpret();
        while (queue.length > 0) {
            /**
             * @param expression {MajesticExpression}
             */
            var expression = queue.shift();
            expression.run();
        }
    }
};;function MajesticWizardMethod() {
}
MajesticWizardMethod.prototype = {
    run: function (mjWizard) {
        throw 'Необходимо реализовать метод run Для MajesticWizardMethod';
    },
    done: function (mjWizard) {
        mjWizard.next();
    },
    cancel: function (mjWizard) {
        mjWizard.cancel();
    }
};function MajesticQueue() {
    this.queue = [];
}
MajesticQueue.prototype = {

    enqueue: function (mjMethod) {
        this.queue.push(mjMethod);
    },
    dequeue: function () {
        return this.queue.shift();
    },
    valid: function () {
        return this.queue.length > 0;
    }
};
function MajesticWizard() {
    MajesticQueue.apply(this, arguments);
    this._steps_count = null;
}
MajesticWizard.prototype = Object.create(MajesticQueue.prototype);
MajesticWizard.prototype.run = function () {
    this._steps_count = this.queue.length;
    this.next();
};
MajesticWizard.prototype.getStepsCount = function(){
    return this._steps_count;
}
MajesticWizard.prototype.getCurrentStep = function(){
    return this.getStepsCount() - this.queue.length;
}
MajesticWizard.prototype.cancel = function () {
    throw 'Необходимо реализовать метод exit для MajesticWizard';
};
MajesticWizard.prototype.done = function () {
    throw 'Необходимо реализовать метод done для MajesticWizard';
};
MajesticWizard.prototype.next = function () {
    if (this.valid()) {
        /**
         * @param mjMethod {MajesticWizardMethod}
         */
        var mjMethod = this.dequeue();
        mjMethod.run(this);
    } else {
        this.done();
    }
};;
/**
 * Мастер создание поручений
 * @param ch_form {ChGridForm}
 */
function TaskWizard(ch_form) {
    MajesticWizard.apply(this, arguments);
    this.ch_form = ch_form;
    this.usersidlist = null;
    this.description = null;
}
TaskWizard.prototype = Object.create(MajesticWizard.prototype);
TaskWizard.prototype.cancel = function () {
//    alert("Создаем строку")
};
TaskWizard.prototype.done = function () {
//    console.log(this.description)
//    console.log(this.usersidlist)
    var data = jQuery.extend({},this.ch_form.getDefaultObj());
    data.usersidlist = this.usersidlist;
    data.description = this.description;

    this.ch_form.addRow(data)
//    alert("Создаем строку")
};
TaskWizard.prototype.openStep = function () {

}
TaskWizard.prototype.getStepCaption = function () {
    return '( Шаг ' + this.getCurrentStep() + ' из ' + this.getStepsCount() + ')';
};function SelectServiceTaskStep() {
    MajesticWizardMethod.apply(this, arguments);
}
SelectServiceTaskStep.prototype = Object.create(MajesticWizardMethod.prototype);
SelectServiceTaskStep.prototype.run = function (mjWizard) {
    var _this = this;
    //Строим дерево
    jQuery.get(
        MajesticVars.EXECUTE_URL,
        {cache: true, sql: 'Tasks.ServicesGet'},
        function (response) {
            var ch_response = new ChResponse(response);
            if (ch_response.isSuccess()) {
                var nodes = ch_response.getData(),
                    map = {}, node, roots = [], autocomplete_data = [];
                for (var i in nodes) {
                    node = nodes[i];
                    node.title = node.data.name;
                    node.key = i;
                    node.icon = false;
                    node.parentid = node.data.parentid;
                    node.description = node.data.description;
                    node.usersidlist = node.data.usersidlist;
                    delete node.data;
                    node.children = [];
                    map[node.id] = i; // use map to look-up the parents
                    if (node.parentid !== null) {
                        autocomplete_data.push({label: node.title, id: node.key})
                        nodes[map[node.parentid]].children.push(node);
                    } else {

                        roots.push(node);
                    }
                }
                var $content = $('<div></div>');
                var $tree = $('<div class="widget-tree"></div>');
                $tree.dynatree({
                    children: roots,
                    selectMode: 1,
                    onQueryActivate: function (flag, node) {
                        if (node.data.children.length == 0) {
                            return true;
                        } else {
                            return false
                        }
                    },
                    onRender: function (node, nodeSpan) {
                        if (node.data.children.length == 0) {

                            $(nodeSpan).attr("data-id", node.data.id);
                            $(nodeSpan).attr("data-title", node.data.title.toLowerCase());
                        }
                    },
                    onActivate: function (node) {
                        var desc = node.data.description,
                            useridlist = node.data.usersidlist;
                        mjWizard.description = desc;
                        mjWizard.usersidlist = useridlist;
                        var $span = $(node.span),
                            $nxt_btn = $span.closest('div.ui-widget').find('button.wizard-next-button.wizard-no-active');
                        $nxt_btn.removeClass('wizard-no-active').addClass('wizard-active');
                    }
                });
                $tree.dynatree("getRoot").visit(function (node) {
                    node.expand(true);
                });
                var $search = $('<input type="text">');
                var $header = $('<div class="widget-header-tree"></div>')

//                $header.prepend('<div class="widget-steps-container"><div class="widget-step">Первый</div></div>');
                $header.append($search);
                $content.prepend($header)
                $content.append($tree)

                openWizardDialog($content, mjWizard, _this, false, 'Выберите тип поручения '+mjWizard.getStepCaption());
                $search.autocomplete({
                    delay: 100,
                    source: function (request, response) {
                        var searchParam = Chocolate.eng2rus(request.term.toLowerCase())

                        function outputItem(item, i, arr) {
                            var source = item.label.toLowerCase();
                            if (source.indexOf(searchParam) != -1) {
                                return true
                            }
                            else {
                                return false
                            }
                        }

                        var responseArray = autocomplete_data.filter(outputItem);
                        $content.find('.node-searched').removeClass('node-searched')
                        $content.find('[data-title*=\'' + searchParam + '\']').addClass('node-searched')
                        response(responseArray)
                    },
                    close: function (event, ui) {
                        $content.find('.node-searched').removeClass('node-searched')

                    },
                    select: function (event, ui) {
                        var id = ui.item.id
                        var $elem = $content.find('[data-id=' + id + ']')
                        $content.find('.node-searched').removeClass('node-searched')
                        $tree.dynatree("getTree").activateKey(id)

                    }
                });
            }
            else {
                alert("Произошла ошибка при открытии мастера поручений. Обратитесь к разработчикам.")
            }
        }
    ).
        fail(function () {
            alert("Произошла ошибка при открытии мастера поручений. Обратитесь к разработчикам.")
        })
};
function SelectExecutorsTaskStep() {
    MajesticWizardMethod.apply(this, arguments);
}
SelectExecutorsTaskStep.prototype = Object.create(MajesticWizardMethod.prototype);
SelectExecutorsTaskStep.prototype.run = function (mjWizard) {
    var _this = this;
    jQuery.get(
        MajesticVars.EXECUTE_URL,
        {cache: true, sql: 'tasks.uspGetUsersListForTasksUsers'},
        function (response) {
            var ch_response = new ChResponse(response);
            var data = ch_response.getData();
            var select2_data = [];
            var init_data = [];
            var idlist = mjWizard.usersidlist.split('|')
            for (var i in data) {
                var id = data[i].id,
                    name = data[i].data.name;
                if (idlist.indexOf(id) != -1) {
                    init_data.push({
                        id: id,
                        text: name
                    })
                }
                select2_data.push({
                    id: id,
                    text: name
                })
            }
            var $content = $('<div><div class="widget-header"><div class="widget-titles">Выберите исполнителей</div><div class="widget-titles-content">Пожалуйста, выберите исполнителей вашего поручения</div></div></div>'),
                $select = $('<div class="wizard-select2"></div>');
            $content.append($select)
            openWizardDialog($content, mjWizard, _this, true, 'Выберните исполнителей '+mjWizard.getStepCaption());
            $select.select2({
                multiple: true,
                data: select2_data
            });
            $select.select2('data', init_data)
            $select.on('change', function (e) {
                var val = e.val.join('|') + '|';
                mjWizard.usersidlist = val;
            })
        }

    )
};function SelectDescriptionTaskStep() {
    MajesticWizardMethod.apply(this, arguments);
}
SelectDescriptionTaskStep.prototype = Object.create(MajesticWizardMethod.prototype);
SelectDescriptionTaskStep.prototype.run = function (mjWizard) {
    var _this = this;
    var $content = $('<div class="widget-task-description"><div class="widget-header"><div class="widget-titles">Заполните описание</div><div class="widget-titles-content">Пожалуйста, заполните описание для вашего поручения</div></div></div>'),
        $text = $('<div class="widget-editable-input"></div>');
    $content.append($text)
    $text.editable({
        value: mjWizard.description,
        onblur: 'submit',
        mode: 'inline',
        type: 'textarea',
        display: true,
        inputclass: 'wizard-text',
        showbuttons: false
    });

    openTaskWizardDialogEnd($content, mjWizard, this, 'Заполните описание '+mjWizard.getStepCaption(), $text)


};function openWizardDialog($content, mjWizard, mjMethod, nextIsActive, title) {
    var next_class = 'wizard-no-active wizard-next-button'
    if (nextIsActive) {
        next_class = 'wizard-active wizard-next-button'
    }
    $content.dialog({
        resizable: false,
        title: title,
        dialogClass: 'wizard-dialog',
        modal: true,
//        height: 500,
//        width: 700,
        buttons: {
            Next: {
                text: 'Далее >',
                class: next_class,
                click: function (bt, elem) {
                    if (nextIsActive) {
                        $(this).dialog("close");
                        $(this).closest('div.ui-dialog').remove();

                        mjMethod.done(mjWizard);
                        return true;
                    }
                    var $nxt_btn = $(this).parent().find('button.wizard-next-button.wizard-no-active');
                    if ($nxt_btn.length > 0) {
                        //TODO: немодально сделать
                        alert("Сделайте действие.")
                        return false;
                    } else {
                        $(this).dialog("close");
                        $(this).closest('div.ui-dialog').remove();
                        mjMethod.done(mjWizard);
                    }
                }},
            Cancel: {
                text: 'Отмена',
                class: 'wizard-cancel-button',
                click: function () {
                    $(this).dialog("close");
                    mjMethod.cancel(mjWizard);

                }
            }
        }
    });
};
function openTaskWizardDialogEnd($content, mjWizard, mjMethod, title, $text) {
    var next_class = 'wizard-active wizard-next-button';
    $content.dialog({
        resizable: false,
        title: title,
        dialogClass: 'wizard-dialog',
        modal: true,
//        height: 500,
//        width: 700,
        buttons: {
            Next: {
                text: 'Добавить',
                class: next_class,
                click: function (e, elem) {
                    var description = $(e.target).closest('div.wizard-dialog').find('.wizard-text').val()
                    if (typeof(description) != 'undefined') {
                        mjWizard.description = description;
                    }
                    $(this).dialog("close");
                    $(this).closest('div.ui-dialog').remove();
                    mjMethod.done(mjWizard);
                }},
            Cancel: {
                text: 'Отмена',
                class: 'wizard-cancel-button',
                click: function () {
                    $(this).dialog("close");
                    mjMethod.cancel(mjWizard);

                }
            }
        }
    });
};
$(function () {
    Chocolate.init();
    if (typeof($.fn.editableform) == 'function') {

        $.fn.editableform.buttons =
            '<button type="submit" class="editable-submit wizard-cancel-button">Сохранить</button>' +
                '<button type="button" class="editable-cancel wizard-cancel-button">Отменить</button>';

    }
    jQuery.fn.extend({
        insertAtCaret: function (myValue) {
            return this.each(function (i) {
                if (document.selection) {
                    //For browsers like Internet Explorer
                    this.focus();
                    sel = document.selection.createRange();
                    sel.text = myValue;
                    this.focus();
                }
                else if (this.selectionStart || this.selectionStart == '0') {
                    //For browsers like Firefox and Webkit based
                    var startPos = this.selectionStart;
                    var endPos = this.selectionEnd;
                    var scrollTop = this.scrollTop;
                    this.value = this.value.substring(0, startPos) + myValue + this.value.substring(endPos, this.value.length);
                    this.focus();
                    this.selectionStart = startPos + myValue.length;
                    this.selectionEnd = startPos + myValue.length;
                    this.scrollTop = scrollTop;
                } else {
                    this.value += myValue;
                    this.focus();
                }
            })
        }
    });

    ;
    (function ($) {
        $.fn.datetimepicker.dates['ru'] = {
            days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"],
            daysShort: ["Вск", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Суб", "Вск"],
            daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"],
            months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
            monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
            today: "Сегодня",
            suffix: [],
            meridiem: []
        };
    }(jQuery));
    ChocolateEvents.createEventsHandlers();
    $()
});