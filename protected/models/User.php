<?php
/**
 * Created by JetBrains PhpStorm.
 * User: tselishchev
 * Date: 24.06.13
 * Time: 16:23
 * To change this template use File | Settings | File Templates.
 */
use \FrameWork\DataBase\DataBaseRoutine;
class User extends CFormModel
{
    CONST CACHE_NAME = 'User_Identity';
    public $userID;
    public $firstName;
    public $lastName;
    public $patronymic;
    public $email;

    public function rules()
    {
        return array(array('email', 'required'));
    }

    public static function domainIdentityGet($windowDomain, $windowLogin){
           try{
               return Yii::app()->erp->getDomainIdentity($windowDomain, $windowLogin);
           }catch (Exception $e){
               return null;
           }
    }

    public function authenticate($username, $password){
        try{

            $identityData = Yii::app()->erp->getUserIdentity($username, $password);

            if (isset($identityData))
            {
                $this->userID = $identityData['userid'];
                $this->firstName = $identityData['firstname'];
                $this->lastName = $identityData['lastname'];
                $this->patronymic = $identityData['patronymic'];
                $this->email = $identityData['email'];

            }
        }catch (Exception $e){
            Yii::log('Неудачная попатка авторизоваться: Логин: '. $username . ' Пароль: ' . $password, CLogger::LEVEL_WARNING);
        }
    }

    public function sendRestoreData(){
        if($this->email){
            try{
                Yii::app()->erp->sendRestoreData($this->email);
                return true;
            }catch(Exception $e){
                $this->addError('email', $e->getMessage());
                return false;
            }
        }else{
            $this->addError('email', 'Не указан e-mail');
            return false;
        }
    }

    public function attributeLabels()
    {
        return [
            'email'=> 'E-mail'
        ];
    }
}