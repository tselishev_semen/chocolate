<?php
use FrameWork\DataBase\Recordset;
use FrameWork\DataBase\RecordsetRow;
use FrameWork\XML\XML;

class MenuForm
{
    /**
     * @var Recordset
     */
    public static function loadData(){
        return Yii::app()->erp->getFormsForUser();

    }
    public static function convertToAutocomplete(Recordset $recordset){
        $result = [];
        foreach($recordset as $row){
            if($row['viewname']){

            $view = XML::prepareViewName($row['viewname']);
            if(stripos($view, 'map.xml') !==false){
                $url = Yii::app()->createUrl('map/index', ['view' => $view]);
            }else if(stripos($view, 'flatsgramm.xml')  !==false){
                $url = Yii::app()->createUrl('canvas/index', ['view' => $view]);
            }else{
                $url = Yii::app()->createUrl('grid/index', ['view' => $view]);
            }
            $label = $row['mainformmenucaption'];

            $result[] = ['value'=> rawurlencode($label), 'url'=>$url];
            }
        }
            $result[] = ['value'=> rawurlencode('Поручения'),'url' =>  Yii::app()->createUrl('grid/index', ['view' => 'tasks.xml'])];
        return $result;
    }

    public static function convertToTree(Recordset $recordset)
    {
        $tree = [];
        $subTree = [0 => &$tree];
        /**
         * @var $row RecordsetRow
         */
        foreach ($recordset as $row) {
            $id = $row->id;
            $label = $row['mainformmenucaption'];
            $parent = $row['parentid'];
            $view = XML::prepareViewName($row['viewname']);
            if(stripos($view, 'map.xml') !==false){
                $url = Yii::app()->createUrl('map/index', ['view' => $view]);
            }else if(stripos($view, 'flatsgramm.xml')  !==false){
                $url = Yii::app()->createUrl('canvas/index', ['view' => $view]);
            }else{
                $url = Yii::app()->createUrl('grid/index', ['view' => $view]);
            }
            $branch = & $subTree[$parent];
            if ($parent == 0) {
                $branch[$id] = ['label' => $label, 'url' => '#', 'items' => []];
                $subTree[$id] = & $branch[$id];
            } else {
                $branch['items'][$id] = ['label' => $label, 'url' => $url, 'items' => []];
                $subTree[$id] = & $branch['items'][$id];
            }
        }

        return $tree;
    }
}