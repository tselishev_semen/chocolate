<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    private $_id;
//    private $_identityInfo;

    public function authenticate()
    {
        $user = new User();
        $user->authenticate($this->username, $this->password);
        if (!isset($user->userID)) {

            $this->errorCode = self::ERROR_UNKNOWN_IDENTITY;

        } else {

            $this->errorCode = self::ERROR_NONE;
            $this->_id = $user->userID;
            $cookie = new CHttpCookie(User::CACHE_NAME, $user->lastName .' ' . $user->firstName . ' ' . $user->patronymic);
            $cookie->expire = time()+60*60*24*180;
            Yii::app()->request->cookies[User::CACHE_NAME] = $cookie;
        }
        return $this->errorCode==self::ERROR_NONE;
    }

    public function getId()
    {
        return $this->_id;
    }
//    public function getIdentityInfo(){
//        return $this->_identityInfo;
//    }
}