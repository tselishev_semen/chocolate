<?php
/**
 * Created by JetBrains PhpStorm.
 * User: tselishchev
 * Date: 14.06.13
 * Time: 11:40
 * To change this template use File | Settings | File Templates.
 */

namespace FrameWork\DataForm\DataFormModel;


use Rmk\Collection\ObjectMap;

class ActionPropertiesCollection extends ObjectMap
{

    public function __construct($from = null)
    {
        parent::__construct('\FrameWork\DataForm\DataFormModel\ActionProperties', $from);
    }

    public function getData()
    {

        $data = [];
        /**
         * @var $actionProperties ActionProperties
         */
        foreach ($this as $actionProperties) {
            $data[] = [
                'title' => $actionProperties->getCaption(),
                'cmd' => $actionProperties->getAction()
            ];

        }
        $data = array_merge($data, $this->getDefaultAction());
        return $data;
    }

    protected function getDefaultAction()
    {
        return [
            ['title' => 'Экспорт в Excel', 'cmd' => 'ch.export2excel'],
            ['title' => 'Настройка', 'cmd' => 'ch.settings']
        ];
    }

}