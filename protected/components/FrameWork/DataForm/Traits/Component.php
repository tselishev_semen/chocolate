<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 18.12.13
 * Time: 16:19
 */

namespace FrameWork\DataForm\Traits;


use FrameWork\DataBase\DataBaseRoutine;
use FrameWork\DataBase\RecordsetRow;

trait Component
{

    /**
     * @param $expressionString
     * @param $default int
     * @return int
     */
    public static function intExpressionEval($expression, $default = 0){
        if($expression !== null){
            if(is_numeric($expression)){
                return intval($expression);
            }else{
                return $expression;
            }
        }else{
            return $default;
        }

    }
    /**
     * @param $expression String
     * @param $default Boolean
     * @return Boolean
     */
    public static function boolExpressionEval($expression, $default = false){
        $prepareExpr =strtolower(trim($expression));
        switch(true){
            case $prepareExpr === 'true':
                return true;
            case $prepareExpr === 'false':
                return false;
            case strpos($prepareExpr, 'sql') === 0:
                $posEqualSign = strpos($prepareExpr, '=');
                if($posEqualSign !== false){
                    $posSql = $posEqualSign +1;
                    $sql = trim(substr($prepareExpr, $posSql));
                    $sql = \Yii::app()->bind->bindRawSql($sql);
                    $routine = \Yii::app()->bind->bindProcedureFromModel(new DataBaseRoutine($sql));
                    try{
                        $recordset = \Yii::app()->erp->execFromCache($routine);
                        /**
                         * @var $row RecordsetRow
                         */
                        $row = array_shift($recordset->toArray());
                        $access = array_shift($row->data);
                      if($access =='1'){
                          return true;
                      }else{
                          return false;
                      }
                    }catch (\Exception $e){
                        return false;
                    }
                }else{
                    return false;
                }
                break;
            case strpos($prepareExpr, 'role') === 0:
                //todo: реализовать поддержку кисовских ролей
                return false;
            default:
                return $default;

        }

    }

    public static function scriptExpressionEval($expession){
        if(strpos($expession, 'script') === 0){
            $script = substr($expession,6);
            $result = '';
            $commands = explode(';', $script);
            foreach($commands as $command){
                switch(true){
                    case trim($command) == 'dataform.refreshdata':
                        $result .= <<<JS
                    var jForm = $(this).closest('.section-filters').siblings('.section-grid').children('form');
                    /**
                    *
                    * @type {ChGridForm}
                    */
                    var chForm = ChObjectStorage.create(jForm, 'ChGridForm');
//                    console.log(jForm)
                    chForm.refresh();
JS;

                        break;
                    default:
                        break;
                }
            }
            return $result;
        }
        return null;
    }
    public function init(\SimpleXMLElement $xmlFile, $class)
    {
        if ($xmlFile) {
            $reflectionClass = new \ReflectionClass($class);
            foreach ($xmlFile->children() as $attribute => $data) {
                $data = (String)$data;
                $attribute = strtolower($attribute);
                if (!$data instanceof \SimpleXMLElement) {
                    if ($reflectionClass->hasProperty($attribute)) {
                        $this->$attribute = $data;
                    }
                }
            }
        }
    }
} 