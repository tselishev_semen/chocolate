<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 31.01.14
 * Time: 11:35
 */

namespace FrameWork\DataBase;

use Rmk\Collection\ObjectMap;

class Recordset extends ObjectMap
{
    CONST KEY_FIELD = 'id';
    protected $_keys;
    protected $_data;
    protected $_types;

    public function toRawArray()
    {
        $result = [];
        /**
         * @var $row RecordsetRow
         */
        foreach ($this->array as $row){
            $result[] = $row->data;
        }
        return $result;
    }
    public function __construct($from = null)
    {
        parent::__construct('\FrameWork\DataBase\RecordsetRow', $from);
    }

    public function setTypes(array $types)
    {
        $this->_types = $types;
    }

    /**
     * @param $key
     * @return ColumnTypes
     */
    public function getKeyTypes($key)
    {
        if ($this->_types[$key] instanceof ColumnTypes) {
            return $this->_types[$key];
        }
        $this->_types[$key] = new ColumnTypes($this->_types[$key]);
        return $this->_types[$key];
    }

    public function getData($refresh = false)
    {
        if ($this->_data == null || $refresh) {
            $raw = [];
            /**
             * @var RecordsetRow $row
             */
            foreach ($this as $row) {
                $raw[] = $row->data;
            }
            $this->_data = $raw;
        }
        return $this->_data;
    }

    public function rawUrlEncode()
    {
        $encodedData = [];
        /**
         * @var RecordsetRow $row
         */
        foreach ($this as $row) {
            $encodedData[$row->id] = array_map(function ($value) {
                if ($value !== null) {
                    return rawurlencode($value);
                } else {
                    return $value;
                }
            }, $row->data);
        }

        return $encodedData;
    }
}
