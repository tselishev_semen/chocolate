<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 12.11.13
 * Time: 12:45
 */

namespace Chocolate\HTML\Card\Settings;

use Chocolate\HTML\Card\Traits\DefaultSaveFunction;
use Chocolate\HTML\Card\Traits\DefaultValidateFunction;
use Chocolate\HTML\Card\Traits\TextAreaInitFunction;
use Chocolate\HTML\ChHtml;

class TextAreaSettings extends EditableCardElementSettings
{
    use DefaultSaveFunction;
    use DefaultValidateFunction;
    use TextAreaInitFunction;
    public function render($pk, $view,$formID)
    {

        $name = $this->getName();
        $isAllowEdit = $this->isAllowEdit();
        $options = [
            'type' => 'textarea',
            'name' => $name,
            'pk' => ChHtml::ID_KEY,
            'onSave' => $this->createSaveFunction($name,$isAllowEdit),
            'title' => $this->getCaption(),
            'showbuttons' => false,
            'emptytext' =>'',
            'mode' => 'inline',
            'options' => [
                'onblur' => 'submit',
                //                'toggle' => 'mouseenter',
            ],

            'inputclass' => 'chocolate-textarea',
            'onInit' => $this->createInitFunction($name, $isAllowEdit, $this->getCaption(), $this->columnProperties->isNeedFormat()),
            'validate'=> $this->createValidateFunction($isAllowEdit, $this->isRequired())

        ];

        return \Yii::app()->controller->widget('Chocolate.Widgets.ChCardEditable',
            $options, true);
    }
    public function renderBeginData()
    {
        echo '<div class="'.$this->getEditClass().' card-input card-input-text">';
    }
}