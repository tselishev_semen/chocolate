<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 12.11.13
 * Time: 12:46
 */

namespace Chocolate\HTML\Card\Settings;

use Chocolate\HTML\Card\Traits\DefaultValidateFunction;
use Chocolate\HTML\Card\Traits\Select2AjaxData;
use Chocolate\HTML\Card\Traits\Select2AjaxInitSelection;
use Chocolate\HTML\Card\Traits\Select2DataFunction;
use Chocolate\HTML\Card\Traits\Select2InitFunction;
use Chocolate\HTML\Card\Traits\Select2InitSelectionFunction;
use Chocolate\HTML\Card\Traits\Select2SaveFunction;
use Chocolate\HTML\Card\Traits\Select2ShownFunction;
use Chocolate\HTML\Card\Traits\Select2SortFunction;
use Chocolate\HTML\ChHtml;
use Chocolate\HTML\Grid\Traits\Select2ColumnInitFunction;
use Chocolate\HTML\Traits\Select2GetSource;

class Select2Settings extends EditableCardElementSettings
{
    use DefaultValidateFunction;
    use Select2InitFunction;
    use Select2SaveFunction;
    use Select2ShownFunction;
    use Select2SortFunction;
    use Select2GetSource;
    use Select2InitSelectionFunction;
    use Select2DataFunction;
    use Select2AjaxInitSelection;
    use Select2AjaxData;

    public function render($pk, $view, $formID)
    {
        $name = $this->getName();
        $isAllowEdit = $this->isAllowEdit();
        $options = [
            'type' => 'select2',
            'name' => $name,
            'pk' => ChHtml::ID_KEY,
            'onShown' => $this->createShownFunction(),
            'showbuttons' => true,
            'onSave' => $this->createSaveFunction($name),
//            'title' => $this->getCaption(),
            'mode' => 'modal',
            'options' => ['onblur' => 'ignore',],
            'select2' => [
//                'multiple' => !$this->columnProperties->isSingleMode(),
                'multiple' => true,
                'separator' => ',',
                'width' => '300',
                'formatNoMatches' => 'js:function(term){return "Совпадение не найдено"}',
                'sortResults' => $this->createSortFunction(),
            ],
            'onInit' => $this->createInitFunction($name, $isAllowEdit, $this->columnProperties->getKey(),$this->getCaption()),
            'validate'=> $this->createValidateFunction($isAllowEdit, $this->isRequired())

        ];
        if ($this->isNeedAjax()) {
            $options['select2']['initSelection']= $this->ajaxInitSelection($this->columnProperties->getReadProc()->getRawName());
            $options['select2']['query'] = $this->createAjaxData();
        } elseif ($this->isDataLoaded()) {
            $options['select2']['initSelection'] = $this->createInitSelectionFunction($name);
            $options['select2']['data'] = $this->createDataFunction($formID, ChHtml::ID_KEY, $name );
        } else {
            $options['source'] = $this->getSource($this->columnProperties);
        }
        return \Yii::app()->controller->widget('Chocolate.Widgets.ChCardEditable',
            $options, true);
    }

    /**
     * @return bool
     */
    protected function isNeedAjax()
    {

        $readProc = \Yii::app()->bind->bindProcedureFromModel($this->columnProperties->getReadProc());
        if($readProc->isSuccessBinding()){
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    protected function isDataLoaded()
    {
        return $this->columnProperties->isVisible();
    }
}
