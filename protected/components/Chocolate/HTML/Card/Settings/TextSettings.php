<?php
namespace Chocolate\HTML\Card\Settings;
use Chocolate\HTML\Card\Traits\DefaultSaveFunction;
use Chocolate\HTML\Card\Traits\DefaultValidateFunction;
use Chocolate\HTML\Card\Traits\TextAreaInitFunction;
use Chocolate\HTML\ChHtml;

class TextSettings extends EditableCardElementSettings{
    use TextAreaInitFunction;
    use DefaultSaveFunction;
    use DefaultValidateFunction;
    public function render($pk, $view,$formID)
    {
        $name = $this->getName();
        $isAllowEdit = $this->isAllowEdit();
        $options = [
            'type' => 'text',
            'name' => $name,
            'pk' => ChHtml::ID_KEY,
            'onSave' => $this->createSaveFunction($name, $isAllowEdit),
            'title' => $this->getCaption(),
            'mode' => 'inline',
            'showbuttons' => false,
            'emptytext' =>'',
            'options' => [
                'onblur' => 'submit',
//                'toggle' => 'mouseenter',
            ],
            'inputclass' => 'chocolate-textarea',
            'onInit' => $this->createInitFunction($name, $isAllowEdit, $this->getCaption(),  $this->columnProperties->isNeedFormat()),
            'validate'=> $this->createValidateFunction($isAllowEdit, $this->isRequired())
        ];
        return \Yii::app()->controller->widget('Chocolate.Widgets.ChCardEditable',
            $options, true);
    }



}

