<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 12.11.13
 * Time: 12:45
 */

namespace Chocolate\HTML\Card\Settings;
use Chocolate\HTML\Card\Traits\DefaultSaveFunction;
use Chocolate\HTML\Card\Traits\DefaultValidateFunction;
use Chocolate\HTML\Card\Traits\SelectInitFunction;
use Chocolate\HTML\ChHtml;
use Chocolate\HTML\Traits\SelectGetSource;

class SelectSettings extends EditableCardElementSettings {
    use DefaultValidateFunction;
    use DefaultSaveFunction;
    use SelectInitFunction;
    use SelectGetSource;
    public function render( $pk, $view,$formID)
    {
//        return '';
        $name =  $this->getName();
        $isAllowEdit = $this->isAllowEdit();
        $options = [
            'type' => 'select',
            'name' => $name,
            'pk' => ChHtml::ID_KEY,
            'source' => $this->getSource($this->columnProperties),
            'onSave' => $this->createSaveFunction($name, $isAllowEdit),
            'title' => $this->getCaption(),
            'showbuttons' => false,
            'mode' => 'inline',
//            'options' => [
////                'toggle' => 'mouseenter',
////                'onblur' => 'ignore',
//            ],
//            'emptytext' =>'',
            'inputclass'=> 'chocolate-select',
            'onInit' =>$this->createInitFunction($name, $isAllowEdit),
            'validate'=> $this->createValidateFunction($isAllowEdit, $this->isRequired())
        ];

        return \Yii::app()->controller->widget('Chocolate.Widgets.ChCardEditable', $options, true);
    }
}