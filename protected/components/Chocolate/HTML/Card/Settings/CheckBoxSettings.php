<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 05.12.13
 * Time: 12:22
 */

namespace Chocolate\HTML\Card\Settings;

use Chocolate\HTML\Card\Traits\CheckBoxDisplayFunction;
use Chocolate\HTML\Card\Traits\CheckBoxInitFunction;
use Chocolate\HTML\ChHtml;

class CheckBoxSettings extends EditableCardElementSettings
{
    use CheckBoxInitFunction;
    use CheckBoxDisplayFunction;

    public function render($pk, $view, $formID)
    {
        $name = $this->getName();
        $options = [
            'type' => 'checklist',
            'name' => $name,
            'mode' => 'inline',
            'options' => [
                'onblur' => 'submit',
//                 'toggle' => 'mouseenter',
            ],
            'showbuttons' => false,
            'pk' => ChHtml::ID_KEY,
            'onInit' => $this->createInitFunction($name, $this->isAllowEdit()),
            'source' => [1 => ''],
            'display' => $this->createDisplayFunction(),
        ];

        return \Yii::app()->controller->widget('Chocolate.Widgets.ChCardEditable',
            $options, true);
    }

} 