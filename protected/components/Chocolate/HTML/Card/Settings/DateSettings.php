<?php
namespace Chocolate\HTML\Card\Settings;
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 12.11.13
 * Time: 12:44
 */


use Chocolate\HTML\Card\Traits\DateInitFunction;
use Chocolate\HTML\Card\Traits\DefaultSaveFunction;
use Chocolate\HTML\Card\Traits\DefaultValidateFunction;
use Chocolate\HTML\ChHtml;

class DateSettings extends EditableCardElementSettings {
    use DefaultValidateFunction;
    use DefaultSaveFunction;
    use DateInitFunction;
    public function render($pk, $view,$formID)
    {
        $name = $this->getName();
        $isAllowEdit = $this->isAllowEdit();
        $options = [
            'type' => 'date',
            'name' => $name,
            'pk' => ChHtml::ID_KEY,
            'format'      =>   \Yii::app()->params['soap_date_format'],
            'viewformat'  =>  \Yii::app()->params['editable_date_format'],
            'onSave' => $this->createSaveFunction($name, $isAllowEdit),
            'title' => $this->getCaption(),
            'mode' =>'inline',
            'onInit' => $this->createInitFunction($name, $isAllowEdit),
            'validate' => $this->createValidateFunction($isAllowEdit, $this->isRequired()),
            'options' =>[
                'onblur' => 'submit',
//                'datetimepicker' => [
////                    'language' => 'ru',
//                    'todayBtn' => true,
//                ],
            ]
        ];

        return \Yii::app()->controller->widget('Chocolate.Widgets.ChCardEditable',
            $options, true);
    }
}
