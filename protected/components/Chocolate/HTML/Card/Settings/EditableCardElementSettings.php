<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 12.11.13
 * Time: 14:56
 */

namespace Chocolate\HTML\Card\Settings;


use Chocolate\HTML\Card\Interfaces\ICardElementSettings;
use FrameWork\DataForm\Card\CardElementType;
use FrameWork\DataForm\DataFormModel\ColumnProperties;
use FrameWork\DataForm\DataFormModel\ColumnPropertiesCollection;

abstract class EditableCardElementSettings implements ICardElementSettings
{
    public $columnProperties;
    public $columnPropertiesCollection;
    private $_posY;

    public function __construct(ColumnProperties $columnProperties, ColumnPropertiesCollection $columnPropertiesCollection)
    {
        $this->columnProperties = $columnProperties;
        $this->columnPropertiesCollection = $columnPropertiesCollection;
    }

    public function getName()
    {
        return $this->columnProperties->getVisibleKey();
    }

    public function renderEndData()
    {
        echo '</div>';
    }

    public function processBeforeRender($id)
    {
        echo \CHtml::label(
            $this->getCaption(),
            $id,
            ['required' => $this->isRequired()]
        );
//        echo \CHtml::tag('span', ['class' => 'card-input-error']);
    }

    public function renderBeginData()
    {
        echo '<div class="' . $this->getEditClass() . ' card-input card-default-height">';
    }

    protected function getEditClass()
    {
        if ($this->isAllowEdit()) {
            return '';
        }
        return 'card-input-no-edit';
    }

    public function isAllowEdit()
    {
        return $this->columnProperties->isAllowEditInCard();
    }

    /**
     * @return CardElementType
     */
    public function getType()
    {
        return $this->columnProperties->getCardEditType();
    }

    public function getCaption()
    {
        return $this->columnProperties->getVisibleCaption();
    }

    public function getX()
    {
        return $this->columnProperties->getCardX();
    }

    public function getY()
    {
        if ($this->_posY) {
            return $this->_posY;
        }
        $posY = $this->getRecursiveY(0, $this->columnProperties);
        $this->_posY = $posY;
        return $posY;
    }

    public function getRecursiveY($curPosY, ColumnProperties $columnProperties)
    {
        $posY = $columnProperties->getCardY();
        if (strripos($posY, '+')) {
            $matches = explode('+', $posY);
            $parentKey = $matches[0];
            $digit = $matches[1];
            $parentColumn = $this->columnPropertiesCollection->getByKey(mb_strtolower($parentKey, 'UTF-8'));
            return $this->getRecursiveY($curPosY + $digit, $parentColumn);
        } else {
            return $curPosY + $posY;
        }
    }

    public function getHeight()
    {
        return $this->columnProperties->getCardHeight();
    }

    public function getWidth()
    {
        return $this->columnProperties->getCardWidth();
    }

    public function isRequired()
    {
        return $this->columnProperties->isRequired();
    }


}