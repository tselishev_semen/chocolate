<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 12.12.13
 * Time: 9:15
 */

namespace Chocolate\HTML\Card\Traits;


trait Select2SaveFunction {

    protected function createSaveFunction($attribute)
    {
        $script = <<<JS
    chCardFunction.select2SaveFunction(e, params, '$attribute');
JS;
        return 'js:function(e, params){' . $script . '}';

    }
} 