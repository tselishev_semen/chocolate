<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 12.12.13
 * Time: 8:48
 */

namespace Chocolate\HTML\Card\Traits;


trait CheckBoxInitFunction
{
    public function createInitFunction( $attribute, $isAllowEdit)
    {

        $initScript = <<<JS
    chCardFunction.checkBoxInitFunction($(this), '$attribute', '$isAllowEdit');
JS;
        return 'js:function(){' . $initScript . '}';
    }
}