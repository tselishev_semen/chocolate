<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 28.05.14
 * Time: 13:57
 */

namespace Chocolate\HTML\Card\Traits;


trait Select2SortFunction {

    public function createSortFunction(){
        $script = <<<JS
    return chCardFunction.select2SortFunction(results, container, query);
JS;
        return 'js:function(results, container, query){' . $script . '}';
    }
} 