<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 17.12.13
 * Time: 12:38
 */

namespace Chocolate\HTML\Card\Traits;


trait CheckBoxDisplayFunction {
    public function createDisplayFunction(){
        $displayScript = <<<JS
    chCardFunction.checkBoxDisplayFunction(value, $(this));
JS;
        return 'js:function(value, sourceData){' . $displayScript . '}';
    }

} 