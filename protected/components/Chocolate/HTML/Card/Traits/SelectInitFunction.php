<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 12.12.13
 * Time: 9:19
 */

namespace Chocolate\HTML\Card\Traits;


trait SelectInitFunction {
    protected function createInitFunction( $attribute, $isAllowEdit)
    {

        $initScript = <<<JS
    chCardFunction.selectInitFunction($(this), '$attribute', '$isAllowEdit');
JS;
        return 'js:function(){' . $initScript . '}';
    }
} 