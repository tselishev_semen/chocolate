<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 17.03.14
 * Time: 11:59
 */

namespace Chocolate\HTML\Card\Traits;


trait TextAreaInitFunction {
    protected function createInitFunction($attribute, $isAllowEdit, $caption, $isNeedFormat )
    {
        $initScript = <<<JS
        chCardFunction.textAreaInitFunc(e, editable, '$attribute', '$isAllowEdit', '$caption', '$isNeedFormat', this);
JS;
        return 'js:function(e, editable){' . $initScript . '}';
    }
} 