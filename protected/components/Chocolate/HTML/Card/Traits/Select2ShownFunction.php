<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 28.05.14
 * Time: 12:34
 */

namespace Chocolate\HTML\Card\Traits;


trait Select2ShownFunction {

    public function createShownFunction(){
        $script = <<<JS
chCardFunction.select2ShownFunction(editable);
JS;
        return 'js:function(e, editable){' . $script . '}';

    }
} 