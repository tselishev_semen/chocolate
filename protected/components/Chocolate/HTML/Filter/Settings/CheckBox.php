<?php
/**
 * Created by JetBrains PhpStorm.
 * User: tselishchev
 * Date: 27.09.13
 * Time: 15:10
 */

namespace Chocolate\HTML\Filter\Settings;

class CheckBox extends EditableFilterSettings{
    /**
     * @return mixed
     */
    public function render(\CModel $model, \ChFilterForm $form)
    {
        $id = uniqid();
        echo '<li class="filter-item" id="'. $id .'">';
        \Yii::app()->controller->renderPartial('//_filters/_check_box', [
            'form' => $form,
            'model' => $model,
            'settings' => $this
        ]);
//        echo '</li>';
        return $id;
    }
}