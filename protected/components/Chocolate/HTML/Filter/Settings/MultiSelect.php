<?php
/**
 * Created by JetBrains PhpStorm.
 * User: tselishchev
 * Date: 27.09.13
 * Time: 15:12
 */

namespace Chocolate\HTML\Filter\Settings;

use Chocolate\HTML\ChHtml;

class MultiSelect extends EditableFilterSettings
{
    public function toArray()
    {
        return array_merge(
            parent::toArray(),
            [
                'data' => $this->getData(),
                'asDropDownList' => true,
                'options' => array(
                    'placeholder' => $this->getToolTip(),
                    'width' => '210px'
                ),
                'htmlOptions' => [
                    'id' => uniqid(),
                    'multiple' => 'multiple'
                ]
            ]);
    }

//    public function getData()
//    {
//        return ChHtml::createMultiSelectListData(parent::getData());
//    }
    public function isMultiSelect(){
        return $this->filter->isMultiSelect();
    }

    public function getDataUrl(){
        $url = \Yii::app()->controller->createAbsoluteUrl('/majestic/execute', [
            'cache' => true,
            'sql' => $this->filter->getReadProc()->__toString()
        ]);
        return $url;
    }
    /**
     * @return mixed
     */
    public function render(\CModel $model, \ChFilterForm $form)
    {
        $id = uniqid();
        echo '<li class="filter-item" id="'. $id .'">';
        \Yii::app()->controller->renderPartial('//_filters/_multi_select', [
            'form' => $form,
            'model' => $model,
            'settings' => $this
        ]);
//        echo '</li>';
        return $id;

    }


}