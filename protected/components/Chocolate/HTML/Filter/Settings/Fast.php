<?php
/**
 * Created by JetBrains PhpStorm.
 * User: tselishchev
 * Date: 30.09.13
 * Time: 12:47
 */

namespace Chocolate\HTML\Filter\Settings;

use Chocolate\HTML\ChHtml;


class Fast extends EditableFilterSettings
{
    public function render(\CModel $model, \ChFilterForm $form)
    {
        $id = uniqid($this->getName());

//        $tt = $this->getParentFilterKey();
        if (!$this->getParentFilterKey()) {
            echo '<li class="fast-filter filter-item" id="' . $id . '">';
            \Yii::app()->controller->renderPartial('//_filters/_fast', [
                'form' => $form,
                'model' => $model,
                'settings' => $this
            ]);
        }else{
            echo '<li rel="'. $this->getParentFilterKey(). '" class="fast-filter filter-item" id="' . $id . '">';
        }
//        echo '</li>';
        $this->filter->attachEvents($id);
        return $id;
    }

    public function getData($parentID = null)
    {
        if ($this->isMultiSelect()) {

            return ChHtml::createMultiSelectListData(parent::getData($parentID));
        }else{
            $listData = array();
            /**
             * @var $row RecordsetRow
             */
            foreach(parent::getData($parentID) as $row){
                $listData[$row->id  .'|'] = $row['name'];
            }
            return $listData;
        }
    }


}