<?php
/**
 * Created by JetBrains PhpStorm.
 * User: tselishchev
 * Date: 27.09.13
 * Time: 11:33
 */

namespace Chocolate\HTML\Filter;
use Chocolate\HTML\Filter\Settings\Tree;
use FrameWork\DataForm\DataFormModel\AgileFilter;
use Chocolate\HTML\Filter\Interfaces\IFilterSettings;
use FrameWork\DataForm\DataFormModel\FilterType;
use Chocolate\HTML\Filter\Interfaces\ IFilterWidget;
use Chocolate\HTML\Filter\Settings\DateRange;
use Chocolate\HTML\Filter\Settings\CheckBox;
use Chocolate\HTML\Filter\Settings\Select;
use Chocolate\HTML\Filter\Settings\MultiSelect;
use Chocolate\HTML\Filter\Settings\Fast;
use Chocolate\HTML\Filter\Settings\Text;

class EditableFilterWidget implements IFilterWidget{
    function __construct()
    {
    }
    /**
     * @var IFilterSettings
     */
    protected $setting;
    public function create(AgileFilter $filter)
    {
        $type = $filter->getFilterType();
        switch($type){
            case FilterType::DateBetween:
                $this->setting = new DateRange($filter);
                break;
            case FilterType::CheckBox:
                $this->setting = new CheckBox($filter);
                break;
            case FilterType::Tree:
                $this->setting = new Tree($filter);
                break;
            case FilterType::CustomFilter:
                $this->setting = new Select($filter);
                break;
            case FilterType::CustomFilterWithMultiselect:
                $this->setting = new MultiSelect($filter);
                break;
            case FilterType::FastFilter:
                $this->setting = new Fast($filter);
                break;
            default:
                $this->setting = new Text($filter);
                break;
        }
        return $this->setting;
    }



}