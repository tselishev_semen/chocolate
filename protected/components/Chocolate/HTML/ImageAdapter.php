<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 12.11.13
 * Time: 8:58
 */

namespace Chocolate\HTML;


class ImageAdapter {

    public static function getHtml($image = null){
        $class = self::convertImageToCssClass($image);
        if($class){
           return '<span class="'. $class .'"></span>';
        }
        return '';

    }

    protected static function convertImageToCssClass($image = null){
        switch($image){
            case 'TaskBig.jpg':
                return 'fa-tasks';
            default:
                return null;
        }
    }

}