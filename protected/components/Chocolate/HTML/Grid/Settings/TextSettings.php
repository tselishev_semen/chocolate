<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 11.12.13
 * Time: 11:16
 */

namespace Chocolate\HTML\Grid\Settings;


use Chocolate\HTML\Grid\Traits\DefaultSaveFunction;
use Chocolate\HTML\Grid\Traits\DefaultValidateFunction;
use Chocolate\HTML\Grid\Traits\TextInitFunction;

class TextSettings extends XEditableSettings
{
    use DefaultSaveFunction;
//    use DefaultValidateFunction;
    use TextInitFunction;

    public function getData()
    {
        $name = mb_strtolower($this->getName(), 'UTF-8');
        $allowEdit = $this->isAllowEdit();

        return [
            'header' => $this->getHeader(),
            'name' => $name,
            'htmlOptions' => $this->getHtmlOptions(),
            'headerHtmlOptions' => $this->getHeaderHtmlOptions(),
            'class' => $this->getClass(),
            'editable' => [
                'type' => 'text',
                'mode' => 'inline',
//                'mode' => 'popup',
                'options' => [
                    //Если поставить субмит - будут левые изменения
                    'onblur' => 'cancel',
                    'savenochange' => false,
                ],
                'showbuttons' => false,
                'onSave' => $this->createSaveFunction($name, $allowEdit),
                'onInit' => $this->createInitFunction($allowEdit, $name, $this->getCaption()),

            ]
        ];
    }


} 