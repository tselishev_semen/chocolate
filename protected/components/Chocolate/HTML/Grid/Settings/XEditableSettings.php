<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 11.12.13
 * Time: 11:52
 */

namespace Chocolate\HTML\Grid\Settings;


use Chocolate\HTML\Grid\Interfaces\IGridColumnSettings;
use ClassModules\Attachments;
use FrameWork\DataForm\Card\CardElementType;
use FrameWork\DataForm\DataFormModel\ColumnProperties;
use FrameWork\DataForm\DataFormModel\GridColumnType;

abstract class XEditableSettings implements IGridColumnSettings
{
    protected $columnProperties;
    protected $key;

    public function __construct(ColumnProperties $columnProperties)
    {
        $this->columnProperties = $columnProperties;
        $this->key = $columnProperties->getVisibleKey();
    }

    public function getCaption(){
        return $this->columnProperties->getVisibleCaption();
    }

    public function isRequired()
    {
        return $this->columnProperties->isRequired();
    }

    public function isAllowEdit()
    {
        return $this->columnProperties->isAllowEdit();
    }

    public function getHeader()
    {
        $caption = $this->getCaption();
        $captionHtml = self::getHeaderHtml($caption);
        if ($this->key != Attachments::KEY) {
            return $this->columnProperties->isRequired() ? '<div><a data-id=' . $this->key . '><span class="fa-asterisk"></span>'.$captionHtml.'</a></div>' : '<div><a>'.$captionHtml.'</a></div>';
        } else {
            return '<div><a><span class="fa-paperclip"></span>'.$captionHtml.'</a></div>';
        }
    }
    public static function getHeaderHtml($caption){
        return '<span class="grid-caption">' . $caption . '</span><span class="grid-sorting"></span>';
    }

    public function getName()
    {
        return $this->key;
    }

    public function getHtmlOptions()
    {

        if ($this->columnProperties->isAllowEdit()) {
            return array();
        } else {
            return array('class' => 'not-changed');
        }
    }

    public function getHeaderHtmlOptions()
    {
        $options = ['data-id' => $this->key ];
        if (!$this->columnProperties->isAllowEdit() && $this->columnProperties->getKey() != Attachments::KEY) {
            $options['data-changed'] = 0;
        }
        if ($this->columnProperties->getGridEditType() == GridColumnType::Attachments || $this->columnProperties->getGridEditType() == GridColumnType::Button) {
            $options['data-grid-button'] = 1;
        }
        if(!$this->columnProperties->isVisibleInAllField()){
            $options['data-col-hide'] = 1;
        }
        return $options;
    }

    public function getClass()
    {
        return 'Chocolate.Widgets.ChEditableColumn';
    }

}