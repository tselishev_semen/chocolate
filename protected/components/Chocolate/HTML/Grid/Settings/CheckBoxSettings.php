<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 11.12.13
 * Time: 11:17
 */

namespace Chocolate\HTML\Grid\Settings;


use Chocolate\HTML\Grid\Traits\CheckBoxDisplayFunction;
use Chocolate\HTML\Grid\Traits\DefaultInitFunction;
use Chocolate\HTML\Grid\Traits\DefaultSaveFunction;
use Chocolate\HTML\Grid\Traits\DefaultValidateFunction;

class CheckBoxSettings extends XEditableSettings{
//TODO: Сохранение переделать как в карточке
    use DefaultSaveFunction;
//    use DefaultValidateFunction;
    use DefaultInitFunction;
    use CheckBoxDisplayFunction;

    public function getData()
    {
        $name = $this->getName();
        $allowEdit = $this->isAllowEdit();

        $data = array(
            'header' => $this->getHeader(),
            'name' =>$name,
            'htmlOptions' => $this->getHtmlOptions(),
            'headerHtmlOptions' => $this->getHeaderHtmlOptions(),
            'class' => $this->getClass(),
            'editable' => array(
                'mode' => 'inline',
                'options' => array(
                    'onblur' => 'submit',
//                    'toggle' => 'manual',
                ),
                'showbuttons' => false,
                'type' => 'checklist',
                'source' => array(1=>''),
//                'emptytext' => '<input type="ckeckbox"/>',
                'onSave' => $this->createSaveFunction($name),
                'onInit' => $this->createInitFunction($allowEdit),
                'display' => $this->createDisplayFunction()
            )
        );
//        $validateFunction = $this->createValidateFunction( $this->isRequired());
//        if($validateFunction){
//            $data['editable']['validate']  = $validateFunction;
//        }
        return $data;
    }


} 