<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 11.12.13
 * Time: 11:40
 */

namespace Chocolate\HTML\Grid\Settings;
use Chocolate\HTML\Grid\Traits\GridColumnInitFunction;

class GridSettings extends XEditableSettings
{
    use GridColumnInitFunction;
    public function getData()
    {
        return array(
            'header' => $this->getHeader(),
            'name' => $this->getName(),
            'htmlOptions' => array('class' => 'grid-button'),
            'headerHtmlOptions' => $this->getHeaderHtmlOptions(),
            'class' => $this->getClass(),
            'editable' => array(
                'type' => 'text',
                'mode' => 'inline',
                'showbuttons' => false,
                'onInit' => $this->createInitFunction($this->columnProperties->getViewName(),  $this->columnProperties->getCaption())
            )
        );
    }


} 