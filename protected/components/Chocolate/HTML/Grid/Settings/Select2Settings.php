<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 11.12.13
 * Time: 11:16
 */

namespace Chocolate\HTML\Grid\Settings;


use Chocolate\HTML\Card\Traits\Select2ShownFunction;
use Chocolate\HTML\Card\Traits\Select2SortFunction;
use Chocolate\HTML\Grid\Traits\Select2ColumnInitFunction;
use Chocolate\HTML\Grid\Traits\Select2ColumnSaveFunction;
use Chocolate\HTML\Traits\Select2GetSource;
use Chocolate\HTML\Traits\Select2InitFunction;
use Chocolate\HTML\Traits\Select2TitleFunction;

class Select2Settings extends XEditableSettings
{
//    use DefaultValidateFunction;
    use Select2ColumnInitFunction;
    use Select2ColumnSaveFunction;
    use Select2GetSource;
    use Select2SortFunction;
    use Select2ShownFunction;
//    use Select2TitleFunction
use Select2InitFunction;

    public function getData()
    {
        //TODO: allowEdit не работает(как и в Кисе)
        $name = $this->getName();
//        $allowEdit = $this->isAllowEdit();
        return [
            'header' => $this->getHeader(),
            'name' => $name,
            'htmlOptions' => $this->getHtmlOptions(),
            'headerHtmlOptions' => $this->getHeaderHtmlOptions(),
            'class' => $this->getClass(),
            'editable' => [
                'type' => 'select2',
//                'title' => $this->getCaption(),
//                'title' => rawurlencode($this->createTitle($this->getCaption())),
                'onSave' => $this->createSaveFunction($name),
                'onShown' => $this->createShownFunction(),
                'onInit' =>$this->createInit($this->getCaption()),
                'mode' => 'modal',
                'options' => [
                    'onblur' => 'ignore',
                    'savenochange' => false,
                ],
                'showbuttons' => true,
                'source' => $this->getSource($this->columnProperties),
                'select2' => [
                    'multiple' => true,
                    'separator' => '|',
                    'formatNoMatches' => 'js:function(term){return "Совпадение не найдено"}',
                    'sortResults' => $this->createSortFunction(),
                    'initSelection' => $this->createSelect2InitFunction(),
                    'width' => '300',
                ],
            ]
        ];


    }
}