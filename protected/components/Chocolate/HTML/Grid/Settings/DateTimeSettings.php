<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 11.12.13
 * Time: 11:02
 */

namespace Chocolate\HTML\Grid\Settings;

use Chocolate\HTML\Grid\Traits\DateInitFunction;
use Chocolate\HTML\Grid\Traits\DateSaveFunction;

class DateTimeSettings extends XEditableSettings {
    use DateSaveFunction;
    use DateInitFunction;

    public function getData()
    {
        $name = $this->getName();
        $isAllowEdit = $this->isAllowEdit();

        return [
            'header' => $this->getHeader(),
            'name' =>$name,
            'htmlOptions' => $this->getHtmlOptions(),
            'headerHtmlOptions' => $this->getHeaderHtmlOptions(),
            'class' => $this->getClass(),
            'editable' =>[
                'type' => 'datetime',
                'mode' => 'inline',

                'options' => [
                    'onblur' => 'submit',
                    'datetimepicker' => [
                        'language' => 'ru',
                        'todayBtn' => true,
                    ],
                ],
                'showbuttons' => false,
                'format'      => \Yii::app()->params['soap_date_format'],
                'viewformat'  => \Yii::app()->params['editable_date_time_format'],
                'onSave' => $this->createSaveFunction($isAllowEdit,$name),
                'onInit' => $this->createInitFunction($isAllowEdit)

            ]
        ];
    }


} 