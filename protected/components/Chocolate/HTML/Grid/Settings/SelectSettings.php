<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 11.12.13
 * Time: 11:16
 */

namespace Chocolate\HTML\Grid\Settings;


use Chocolate\HTML\Grid\Traits\DefaultSaveFunction;
use Chocolate\HTML\Grid\Traits\SelectInitFunction;
use Chocolate\HTML\Traits\SelectGetSource;

class SelectSettings  extends XEditableSettings{
    use DefaultSaveFunction;
    use SelectInitFunction;
    use SelectGetSource;
    public function getData()
    {
        $name = $this->getName();
        $allowEdit = $this->isAllowEdit();

        return [
            'header' => $this->getHeader(),
            'name' =>$name,
            'htmlOptions' => $this->getHtmlOptions(),
            'headerHtmlOptions' => $this->getHeaderHtmlOptions(),
            'class' => $this->getClass(),
            'editable' => [
                'type' => 'select',
                'source' => $this->getSource($this->columnProperties),
                'mode' => 'inline',
                'showbuttons' => false,
                'onSave' => $this->createSaveFunction($name, $allowEdit),
                'onInit' => $this->createInitFunction($allowEdit)

            ]
        ];
    }


} 