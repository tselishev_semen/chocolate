<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 11.12.13
 * Time: 11:17
 */

namespace Chocolate\HTML\Grid\Settings;


use Chocolate\HTML\Grid\Traits\TextInitFunction ;
use Chocolate\HTML\Grid\Traits\DefaultSaveFunction;
//use Chocolate\HTML\Grid\Traits\DefaultValidateFunction;

class TextAreaSettings extends XEditableSettings{
//    use DefaultValidateFunction;
    use DefaultSaveFunction;
    use TextInitFunction ;
    public function getData()
    {



        $name = strtolower($this->getName());
        $allowEdit = $this->isAllowEdit();

        return [
            'header' => $this->getHeader(),
            'name' =>$name,
            'htmlOptions' => $this->getHtmlOptions(),
            'headerHtmlOptions' => $this->getHeaderHtmlOptions(),
            'class' => $this->getClass(),
            'editable' => [
                'type' => 'textarea',
                'mode' => 'inline',
                    'savenochange' =>false,
                'options' => ['onblur' => 'submit'],
                'showbuttons' => false,
                'onSave' => $this->createSaveFunction($name, $allowEdit),
                'onInit' => $this->createInitFunction($allowEdit, $name, $this->getCaption())

            ]
        ];
    }


} 