<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 13.01.14
 * Time: 15:48
 */

namespace Chocolate\HTML\Grid\Traits;


trait TextInitFunction
{
    public function createInitFunction($allowEdit, $columnName, $caption)
    {
        $initScript = <<<JS
        var column = new ChTextAreaEditableColumn(editable['\$element']);
            column.create(this, e, '$allowEdit', '$columnName', '$caption');
JS;
        return 'js:function(e, editable){' . $initScript . '}';
    }
} 