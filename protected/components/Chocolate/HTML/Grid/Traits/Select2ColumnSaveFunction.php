<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 11.12.13
 * Time: 15:17
 */

namespace Chocolate\HTML\Grid\Traits;


trait Select2ColumnSaveFunction {

    public function createSaveFunction($columnName){

        $initScript = <<<JS
chCardFunction.select2ColumnSaveFunction(e, params, '$columnName');
JS;
        return 'js:function(e, params){' . $initScript . '}';
    }
} 