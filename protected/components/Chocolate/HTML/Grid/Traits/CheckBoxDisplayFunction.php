<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 17.12.13
 * Time: 12:12
 */

namespace Chocolate\HTML\Grid\Traits;


trait CheckBoxDisplayFunction {
    public function createDisplayFunction(){
        $displayScript = <<<JS
        if(value.length > 0 && value[0]){
            $(this).text('да');
        }else{
            $(this).text('нет');
        }
JS;
        return 'js:function(value, sourceData){' . $displayScript . '}';
    }
}