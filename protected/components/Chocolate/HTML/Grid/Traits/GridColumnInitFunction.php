<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 11.12.13
 * Time: 15:05
 */

namespace Chocolate\HTML\Grid\Traits;


trait GridColumnInitFunction
{
    public function createInitFunction($view, $caption)
    {

        $url = \Yii::app()->createUrl('grid/getChildGrid');
        $initScript = <<<JS
                        var chColumn = new ChGridColumn($(this));
                        chColumn.init(e, '$view', '$caption', '$url', editable);
JS;
        return 'js:function(e, editable){ ' . $initScript . '}';
    }

} 