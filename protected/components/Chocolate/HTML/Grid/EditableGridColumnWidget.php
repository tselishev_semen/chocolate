<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 11.12.13
 * Time: 10:57
 */

namespace Chocolate\HTML\Grid;


use Chocolate\HTML\Grid\Interfaces\IGridColumnSettings;
use Chocolate\HTML\Grid\Interfaces\IGridColumnWidget;
use Chocolate\HTML\Grid\Settings\AttachmentsSettings;
use Chocolate\HTML\Grid\Settings\CheckBoxSettings;
use Chocolate\HTML\Grid\Settings\DateSettings;
use Chocolate\HTML\Grid\Settings\DateTimeSettings;
use Chocolate\HTML\Grid\Settings\GridSettings;
use Chocolate\HTML\Grid\Settings\Select2Settings;
use Chocolate\HTML\Grid\Settings\SelectSettings;
use Chocolate\HTML\Grid\Settings\TextAreaSettings;
use Chocolate\HTML\Grid\Settings\TextSettings;
use FrameWork\DataForm\DataFormModel\ColumnProperties;
use FrameWork\DataForm\DataFormModel\GridColumnType;

class EditableGridColumnWidget implements IGridColumnWidget{
    /**
     * @param ColumnProperties $columnProperties
     * @return IGridColumnSettings
     */
    public function create(ColumnProperties $columnProperties)
    {
//        $type =  $columnProperties->getGridEditType();
        switch($columnProperties->getGridEditType()){
            case GridColumnType::TextDialog:
                return new TextAreaSettings($columnProperties);
            case GridColumnType::Text:
                return new TextSettings($columnProperties);
            case GridColumnType::CheckBox:
                return new CheckBoxSettings($columnProperties);
            case GridColumnType::Attachments:
                return new GridSettings($columnProperties);
            case GridColumnType::Button:
                return new GridSettings($columnProperties);
            case GridColumnType::Date:
                return new DateSettings($columnProperties);
            case GridColumnType::DateTime:
                return new DateTimeSettings($columnProperties);
            case GridColumnType::OnOff:
                //TODO: стоит временная заглушка
                return new CheckBoxSettings($columnProperties);
            case GridColumnType::SelectItems:
                return new Select2Settings($columnProperties);
            case GridColumnType::ValueList:
                return new SelectSettings($columnProperties);
            case GridColumnType::ValueListWithoutBlank:
                return new SelectSettings($columnProperties);
            case GridColumnType::TreeDialog:
                return new Select2Settings($columnProperties);
            default:
                return new TextSettings($columnProperties);
        }
    }


} 