<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 11.12.13
 * Time: 10:49
 */

namespace Chocolate\HTML\Grid\Interfaces;
use FrameWork\DataForm\DataFormModel\ColumnProperties;

interface IGridColumnWidget {

    /**
     * @param ColumnProperties $columnProperties
     * @return IGridColumnSettings
     */
    public function create(ColumnProperties $columnProperties);
} 