<?php
/**
 * Created by PhpStorm.
 * User: tselishchev
 * Date: 11.12.13
 * Time: 10:54
 */

namespace Chocolate\HTML\Grid\Interfaces;


use FrameWork\DataForm\DataFormModel\ColumnProperties;

interface IGridColumnSettings {
    public function __construct(ColumnProperties $columnProperties);
    public function getHeader();
    public function getName();
    public function isRequired();
    public function isAllowEdit();
    public function getData();

} 