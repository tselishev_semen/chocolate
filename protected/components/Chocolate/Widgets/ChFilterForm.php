<?php
use Chocolate\HTML\Filter\Interfaces\IFilterSettings;
use Chocolate\HTML\Filter\Settings\FilterSettingsCollection;

/**
 * Created by JetBrains PhpStorm.
 * User: tselishchev
 * Date: 13.06.13
 * Time: 15:15
 * To change this template use File | Settings | File Templates.
 */
Yii::import('bootstrap.widgets.TbActiveForm');
class ChFilterForm extends TbActiveForm
{
    /**
     * @var $filters FilterSettingsCollection
     */
    public $filters;
    /**
     * @var $model GridForm
     */
    public $model;
    protected $layoutFilters = [];

    public function run()
    {
        $this->layoutFilters();
        parent::run();

    }

    protected function layoutFilters()
    {
        if ($this->filters instanceof FilterSettingsCollection) {
            ob_start();
            $this->beginFilterList();
            $this->beginFilterRow();
            /**
             * @var $setting IFilterSettings
             */
            foreach ($this->filters as $setting) {
                if ($setting->isNextRow()) {
                    $this->endFilterRow();
                    $this->beginFilterRow();
                }
                $currentID = $setting->render($this->model, $this);
                $this->layoutFilters[$setting->getName()] = $currentID;
                if (($parentKey = $setting->getParentFilterKey())) {
//                    $filter = $this->filters->getFilterByKey($parentKey);
                    $id = $this->layoutFilters[$parentKey];
                    $name =$setting->getName();
                    $parentName = 'GridForm[filters]['.$parentKey . ']';
                    $view = $this->model->getView();
                    $url =Yii::app()->controller->createUrl('majestic/filterLayout');
                    Yii::app()->clientScript->registerScript(uniqid($parentName), <<<JS
                $('#' +'$id').on('click', function(e){
//                console.log('ccc')
                    var jForm = $(this).closest('form');
                   var ChFilterForm = ChObjectStorage.create(jForm, 'ChFilterForm');
                   //TODO: поддержка всех типов фильтро
                 var value = ChFilterForm.getData()['$parentName']
                 if(typeof(value)!='undefined'){
//                    value = value.slice(0, value.length-1);
console.log(value)
                    var url = '$url'+ '?name=' +'$name' + '&view=' + '$view' +'&parentID=' + value;
                    $.post(url)
                    .done(function(response){
//                    console.log(response)
                        $('#'+ '$currentID').html(response);
                        jForm.find("[rel='$name']").html('');
                    })
                    .fail(function(){
                        alert('fail');
                    });
//                    console.log(value)
                 }
//                   e.preventDefault();
                });
JS
                        , CClientScript::POS_END
                    );
                }
            }
            $this->endFilterRow();
            $this->endFilterList();
        }
        echo '</ul>';
        ob_end_flush();
    }

    protected function beginFilterList()
    {
        echo '<div><ul class="filters-list">';
    }

    protected function beginFilterRow()
    {
        echo '<div class="filter-row">';
    }

    protected function endFilterRow()
    {
        echo '</div>';
    }

    protected function endFilterList()
    {
        echo '</ul></div>';
    }
}