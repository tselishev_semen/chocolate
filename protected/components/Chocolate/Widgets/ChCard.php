<?php
use Chocolate\HTML\Card\Interfaces\ICardElementSettings;
use Chocolate\HTML\Card\Settings\CardElementSettingsCollection;
use Chocolate\HTML\ChHtml;
use FrameWork\DataForm\Card\CardElementPQ;

class ChCard extends CWidget
{
    CONST START_Y_POSITION = 1;
    CONST MAX_POSITION = 'max';
    public $cols;
    public $rows;
    public $pk;
    public $view;
    public $viewID;
    /**
     * @var CardElementSettingsCollection $CardElementSettingsCollection ;
     */
    public $CardElementSettingsCollection;
    protected $cellHeight;
    protected $cellWidth;
    protected $card_id;

    protected static function isNewRow($prevPosY, $curPosY)
    {
        if ($prevPosY != $curPosY) {
            return true;
        }
        return false;
    }

    public function init()
    {
        parent::init();
        $this->cellHeight = intval(100 / $this->rows);
        $this->cellWidth = intval(100 / $this->cols);
    }

    public function run()
    {
        parent::run();
        $this->renderCard();
    }

    public function renderCard()
    {
        ob_start();
        $this->renderTopContent();

        foreach ($this->createQueue() as $element) {
            $this->createCell($element['data'], $element['priority']);
        }
        echo '</div>';
        $this->renderCardButtons();
        ob_end_flush();
        $this->registerScripts();
    }

    protected function renderTopContent()
    {
        $this->card_id = ChHtml::generateUniqueID('card');
        echo '<div class="card-content" data-id="card-control" data-view-id="' . $this->viewID . '" id="' . $this->card_id . '">';
    }

    protected function createQueue()
    {
        $cardElementPQ = new CardElementPQ();
        /** @var $cardSettings ICardElementSettings */
        foreach ($this->CardElementSettingsCollection as $cardSettings) {
            $cardElementPQ->insert(
                $cardSettings->render($this->pk, $this->view, $this->viewID),
                $cardSettings
            );
        }
        $cardElementPQ->setExtractFlags($cardElementPQ::EXTR_BOTH);
        return $cardElementPQ;
    }

    protected function createCell($data, ICardElementSettings $elementSettings)
    {
        $height = $elementSettings->getHeight();
        if(strcasecmp($height, self::MAX_POSITION)== 0){
            $height = ($this->rows -$elementSettings->getY()+1)  * $this->cellHeight . '%';
        }else{
            $height = $this->cellHeight * $height . '%';
        }
        $width = $elementSettings->getWidth();
        if (strcasecmp($width, self::MAX_POSITION) == 0) {
            $width = ($this->cols -$elementSettings->getX()+1)* $this->cellWidth;
        } else {
            $width = $this->cellWidth * $width;
        }
        $id = ChHtml::generateUniqueID('chocolate');

        $this->renderTopCell($id, $elementSettings->getX(), $elementSettings->getY(), $width, $height);
        $elementSettings->processBeforeRender($id);
        $elementSettings->renderBeginData();
        echo $data;
        $elementSettings->renderEndData();
        echo '</div>';
    }

    protected function renderTopCell($id, $posX, $posY, $width, $height)
    {
        $top = $this->cellHeight * ($posY - 1);
        $left = $this->cellWidth * ($posX - 1);
        echo '<div id=' . $id . ' data-x=' . $posX . ' data-y=' . $posY . ' class="card-col" style="top:' . $top . '%;left:' . $left . '%;width:' . $width . '%;height:' . $height . '">';
    }

    protected function renderCardButtons()
    {
        echo '<div class="card-action-button" data-id="action-button-panel" data-view-id=' . $this->viewID . '>';
        echo CHtml::button('Ок', ['class' => 'card-save', 'data-id' => 'card-save',]);
        echo CHtml::button('Отмена', ['class' => 'card-cancel', 'data-id' => 'card-cancel',]);
        echo '</div>';
    }

    protected function registerScripts()
    {
        Yii::app()->clientScript->registerScript($this->card_id, <<<JS
            chAjaxQueue.send();
            ChocolateDraw.drawCardControls($('#' +'$this->card_id'));
JS
            ,
            CClientScript::POS_END);
    }
}