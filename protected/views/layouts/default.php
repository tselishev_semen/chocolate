<? /* @var $this SiteController */
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8"/>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
    <link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
    <link href="/css/libs/erp.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
    <? if (defined('CHOCOLATE_DEBUG')): ?>
        <script src="/js/erp.js"></script>
    <? else: ?>
        <script src="/js/erp.min.js"></script>
    <? endif; ?>
    <script src="/js/main.js"></script>
    <?
    Yii::app()->clientScript->registerScript('yandexMap', <<<JS
        var node = document.createElement('script');
        node.async = true;
        node.src = '//api-maps.yandex.ru/2.0.36/?load=package.standard,package.geoObjects,package.clusters&lang=ru-RU';
        document.body.appendChild(node)
JS
        , CClientScript::POS_END
    );
    ?>
    <title>Апельсин</title>
</head>
<body>
<header id="header">
    <? if (isset($this->navigation)):
        $this->widget('Chocolate.Widgets.ChNavbar', [
//                'type' => 'inverse', // null or 'inverse'
            'brand' => 'КИС Апельсин',
            'brandUrl' => '#',
            'fluid' => true,
            'collapse' => false, // requires bootstrap-responsive.css
            'items' => [
                '<input type="text" id="nav-search" placeholder="Быстрый поиск">',
                [
                    'class' => 'Chocolate.Widgets.ChMenu',
                    'items' => MenuForm::convertToTree($this->navigation)
                ],
            ],
        ]);
    endif ?>
    <div id="fadingBarsG">
        <div id="fadingBarsG_1" class="fadingBarsG">
        </div>
        <div id="fadingBarsG_2" class="fadingBarsG">
        </div>
        <div id="fadingBarsG_3" class="fadingBarsG">
        </div>
        <div id="fadingBarsG_4" class="fadingBarsG">
        </div>
        <div id="fadingBarsG_5" class="fadingBarsG">
        </div>
        <div id="fadingBarsG_6" class="fadingBarsG">
        </div>
        <div id="fadingBarsG_7" class="fadingBarsG">
        </div>
        <div id="fadingBarsG_8" class="fadingBarsG">
        </div>
    </div>
</header>

<div id="pagewrap">
    <!--    <aside id="sidebar">-->
    <!--    </aside>-->
    <div id="content">
        <? echo $content; ?>
    </div>
</div>
<footer id="footer">
    <?php
    $userName = Yii::app()->request->cookies[User::CACHE_NAME]->value;
    $this->widget('Chocolate.Widgets.ChNavbar', [
        'type' => 'inverse',
        'brand' => $userName,
        'brandUrl' => '#',
        'fluid' => true,
        'fixed' => 'bottom',
        'collapse' => false, // requires bootstrap-responsive.css
        'items' => [
            [
                'class' => 'Chocolate.Widgets.ChMenu',
                'items' => [
                    ['label' => 'Поручения', 'itemOptions' => ['class' => 'link'], 'url' => Yii::app()->createUrl('grid/index', ['view' => 'tasks.xml'])],
//                    ['label' => 'Журнал', 'url' => '#'],
                    ['label' => 'Выйти', 'url' => Yii::app()->createUrl('site/logout')],
                ]
            ],
        ],
    ]);
    $data = json_encode(MenuForm::convertToAutocomplete($this->navigation));
    Yii::app()->clientScript->registerScript('authorization', <<<JS
        Chocolate.setUser('$userName');
        Chocolate.initNavSearch('$data');

JS
        , CClientScript::POS_READY
    )
    ?>

</footer>
</body>
<script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade" data-id="{%=file.id%}">
        <td class="attachment-grid-menu"><?php ChControlsColumn::renderCardButton() ?></td>
        <td class="attachment-name">
            <div class="table-td">
                <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}">{%=file.name%}</a>
            </div>
        </td>
        <td class="attachment-update" style="padding:0">
            <div class="table-td" >
              		<span class="btn fileinput-button">
            <input type="hidden" value="" name="Files[files]"/>
                <input data-id="attachments.xml" parent-data-id="tasks.xml"
                       parent-id={%=file.id%} enctype="multipart/form-data"
                       data-input-id="chocolate-upload-hidden" multiple="mul"
                       name="Files[files]" type="file"/>
		</span>
            </div>
        </td>
        <td>
            <div class="table-td">
                <span>{%=file.version%}</span>
            </div>
        </td>
    </tr>
    {% } %}

</script>
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade" >
         <td class="attachment-grid-menu"><?php ChControlsColumn::renderCardButton() ?></td>
        <td class="attachment-name">
            <div class="table-td">
                <span>{%=file.name%}</span>
            </div>
        </td>
         <td class="attachment-update" style="padding:0">
            <div class="table-td">
              		<span class="btn fileinput-button">
            <input type="hidden" value="" name="Files[files]"/>
                <input data-id="attachments.xml" parent-data-id="tasks.xml"
                       parent-id="{%=file.id%}" enctype="multipart/form-data"
                       data-input-id="chocolate-upload-hidden" multiple="mul"
                       name="Files[files]" type="file"/>
		</span>
            </div>
        </td>

        <td>
            <div class="table-td start">
                   <span>1</span>
                    <button style="display:none"> </button>
            </div>
        </td>
    </tr>
{% } %}

</script>
</html>