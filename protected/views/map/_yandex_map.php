<?
/**
 * @var $model GridForm
 * @var $this CController
 */
?>
<?
$mapID = uniqid('map');
$recordset = $model->loadData();
$data = json_encode($recordset->rawUrlEncode());
?>

<div class="map" id="<? echo $mapID?>" style="width: 100%; height: 600px"></div>
<?
Yii::app()->clientScript->registerScript('yandex_map', <<<JS
$(function() {
  // здесь код, который должен быть выполнен после загрузки апи

      ymaps.ready(function(){
      /**
* @type {ChMap}
*/
      var ch_map =ChObjectStorage.create($('#' + '$mapID'), 'ChMap');
         ch_map.init(ymaps, '$data');
//var fullScreen = false;
//      $('#toggler').click(toggle);
//
//function toggle () {
//    fullScreen = !fullScreen;
//    if (fullScreen) {
//       $('#' + '$mapID').removeClass('smallMap');
//    } else {
//        $('#' + '$mapID').addClass('smallMap');
//    }
//    console.log( ch_map.map)
//        ch_map.map.container.fitToViewport();
//}
  });
})
JS
,CClientScript::POS_END
)
?>