<?php
use Chocolate\HTML\ImageAdapter;

/**
 * @var $model GridForm
 * @var $this Controller
 */

$dataFormProperties = $model->getDataFormProperties();

?>
<div class="top-header">
    <div class='left-header'>
        <?php echo ImageAdapter::getHtml($dataFormProperties->getHeaderImage()); ?>
    </div>
    <div class="right-header">
        <?php if (empty($dataFormProperties->getHeaderText())) {

            echo $dataFormProperties->getWindowCaption();
        } else {

            echo $dataFormProperties->getHeaderText();
        }
        ?>
    </div>
</div>
<div class='bottom-header'>
    <?php echo $model->getStateProcData(); ?>
</div>
