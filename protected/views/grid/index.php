<?php
/**
 * @var $model GridForm,
 * @var $this Controller
 * @var $parentViewID String|null
 */
//$start = microtime(1);
?>
<?php $tabID = uniqid('tb'); ?>
<div id='<?php echo $tabID ?>'>
    <? if($model->hasHeader()):?>
    <section class ='section-header' data-id="header">
        <?php $this->renderPartial('_header', ['model' => $model]); ?>
    </section>
    <? endif; ?>

    <? if ($model->hasFilters()) : ?>
    <section class='section-filters' data-id="filters">

    <? $this->renderPartial('//components/_filter_form', ['model' => $model]); ?>
        </section>
    <? endif ?>

    <?php
    if ($parentID = $model->getParentID()) {
        $parentAttribute = 'data-parent-id ="' . $parentID . '"';
    } else {
        $parentAttribute = null;
    }

    ?>
    <section class='section-grid' data-id="grid-form" <?php echo $parentAttribute ?>>
        <?php
        $this->renderPartial('//components/_grid_form',
            [
                'model' => $model,
                'parentViewID' => $parentViewID,
            ]
        );
        ?>
    </section>
</div>
<script>
    $(function () {
        Chocolate.addTabAndSetActive(
          '<? echo $tabID ?>',
          '<? echo $model->getDataFormProperties()->getWindowCaption()?>'
        );
    })
</script>
<?
//$end = microtime(1) - $start;
//$tt = '';
?>





