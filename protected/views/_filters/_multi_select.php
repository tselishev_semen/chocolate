<?
use Chocolate\HTML\Filter\Settings\MultiSelect;

/**
 * @var $form ChActiveForm
 * @var $model CModel
 * @var $settings MultiSelect
 */
$properties = $settings->getProperties();
$form->widget('Chocolate.Widgets.ChDynaTree', [
    'model' => $model,
    'attribute' => $settings->getAttribute(),
    'url' => $settings->getDataUrl(),
    'descriptionData' => $properties->getDescriptionData(),
    'isRestoreState' => $properties->isRestoreState(),
    'isExpandNodes' => $properties->isExpandNodes(),
    'isSelectAll' => $properties->isSelectAll(),
    'isMultiSelect' => $settings->isMultiSelect(),
    'htmlOptions' => ['class' => 'tree-container'],
    'options' => []
]);
?>