<?
use Chocolate\HTML\Filter\Settings\DateRange;

/**
 * @var $form ChActiveForm
 * @var $model CModel
 * @var $settings DateRange
 */

$attribute = $settings->getAttributeFrom();
echo $form->labelEx($model, $attribute);
?>
<div>
    <?php
    $form->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model' => $model,
        'attribute' => $attribute,
//    'language' => 'ru',
        'options' => array(
//        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
            'format' => 'yyyy.mm.dd',
//        'showSecond'=>true,
            'autoclose' => 'true',
//        'showButtonPanel' => true,
        ),
        'htmlOptions' => array(
//        'style' => 'width: 120px;',
            'class' => 'filter-date',
            'id' => uniqid('from')
        ),
    ));
    $form->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model' => $model,
        'attribute' => $settings->getAttributeTo($attribute),
//    'language' => 'ru',
        'options' => array(
            'format' => 'yyyy.mm.dd',
            'autoclose' => 'true',

        ),
        'htmlOptions' => array(
            'class' => 'filter-date',
//        'style' => 'width: 120px;',
            'id' => uniqid('to')
        ),
    ));?>
</div>