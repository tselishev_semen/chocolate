<?php
use Chocolate\HTML\Filter\Settings\Fast;

/**
 * @var $form ChActiveForm
 * @var $model CModel
 * @var $settings Fast
 * @var $parentID int/null
 */
//if($settings->getParentFilterKey()){
//    $data = null;
//}else{
//    $data = $settings->getData();
//}
if($settings->isMultiSelect()){

echo $form->checkBoxListInlineRow(
    $model,
    $settings->getAttribute(),
    $settings->getData($parentID),
    [
        'template' => '<span class="{labelCssClass}">{input}{label}</span>',
        'label' => false,
//        'onCLick' => 'alert(1)'
    ]
);
}else{
    echo $form->radioButtonListInlineRow(
//    echo $form->(
        $model,
        $settings->getAttribute(),
        $settings->getData($parentID),
        [
            'template' => '<span class="{labelCssClass}">{input}{label}</span>',
            'label' => false
        ]
    );
}
?>
