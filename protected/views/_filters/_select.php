<?
use Chocolate\HTML\Filter\Settings\Select;

/**
 * @var $form ChFilterForm
 * @var $model CModel
 * @var $settings Select
 */
?>
<div>
    <?
    echo $form->select2Row(
        $model,
        $settings->getAttribute(),
        [
            'data' => $settings->getData(),
            'id' => uniqid(),
            'options' => ['width' => '200px']
        ]
    );
    ?>
</div>
