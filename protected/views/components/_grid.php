<?
use Chocolate\HTML\ImageAdapter;

/**
 * @var $containerId String
 * @var $gridID String
 * @var $this Controller
 * @var $formID
 * @var $model GridForm
 *
 */
$recordet = $model->loadData();
//$start = microtime(1);
$this->widget('Chocolate.Widgets.ChGridView', [
    'itemsCssClass' => 'table-bordered items',
    'summaryText' => '',
    'id' => $containerId,
    'selectableRows' => 0,
    'dataProvider' => new CArrayDataProvider([]),
    'columns' => $model->getColumns(),
    'htmlOptions' => [
        'data-id' => 'user-grid',
        'id' => $gridID,
    ]
]);
//$end = microtime(1) - $start;
//$tt = '';
?>
<script>
    $(function () {
        chFunctions.initGrid(
            '<? echo json_encode($recordet->rawUrlEncode())?>',
            '<? echo $model->previewDataToJS($recordet)?>',
            '<? echo $model->defaultValuesToJS()?>',
            '<? echo $model->requiredFieldsToJS()?>',
            '<? echo $model->gridPropertiesToJS()?>',
            '<? echo $formID ?>',
            '<? echo $model->getCardCollection()->getHeader() ?>',
            '<? echo ImageAdapter::getHtml($model->getCardCollection()->getHeaderImage())?>',
            '<? echo $model->cardCollectionToJs() ?>'
        )
    })
</script>