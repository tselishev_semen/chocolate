<?php

class MajesticController extends Controller
{
    public function filters()
    {
        return [
            'accessControl',
            'ajaxOnly -export2Excel, fileGet'];
    }

    public function accessRules()
    {
        return [
            ['allow', 'users' => ['@']],
            ['deny', 'users' => ['*']]
        ];
    }

    public function actionExecute($cache =false, $sql){
        $response = MajesticModel::execute($cache,$sql);
        $response->send();
    }

    public function actionQueueExecute(){
        $reponse = MajesticModel::packageExecute();
        $reponse->send();
    }

    public function actionFilterLayout($name, $view, $parentID){
        $model = Controller::loadForm($view);
        $collection = $model->getFilterSettingsCollection();
        $setting = $collection->getByKey($name);
        $this->renderPartial('//_filters/_fast', [
            'form' => new ChFilterForm(),
            'model' => $model,
            'settings' => $setting,
            'parentID' =>  $parentID
        ]);
//        echo 'semen';
//        echo $data;

    }

    public function actionFileGet($fileID){
        $data = FileModel::getFile($fileID);
//        header('Content-type: application/octet-stream; charset=windows-1251');
//        if($name){
//            //Поддержка браузеров, не понимающих атрибут download
//            header('Content-Disposition: attachment; filename='. rawurlencode($name) );
//        }
        echo $data;

    }
//    public function actionGridExecute($view, $parentView = null, $parentID = null){
//        $sql = Yii::app()->request->getPost('sql');
//        $model = Controller::loadForm($view, $parentView, $parentID);
//        $response = MajesticModel::gridExecute($sql, $model);
//        $response->send();
//    }

    public function actionExport2Excel(){
        ExcelModel::export(
            Controller::loadForm(Yii::app()->request->getParam('view')),
            Yii::app()->request->getParam('data'),
            Yii::app()->request->getParam('settings')
        );
        Yii::app()->end();
    }

//    public function actionTest(){
//        ini_set("soap.wsdl_cache_enabled", 0);
//        ini_set("soap.wsdl_cache_ttl", 1);
////        $client = new SoapClient('http://morozova-n/MyApp/ws/WebServiceHOPE?wsdl');
//        $client = new SoapClient('http://morozova-n/MyApp/ws/CompanySearch?wsdl');
//        $data = $client->CompanyRead();
////        $data = $client->plus2(['Param' => 1]);
////        $data = $client->CompanyDel([
////            'Kod' => '',
////            'Name' => 'Поставщик3',
////            'INN' => '',
////            'KPP' => ''
////        ]);
//        echo 1;
//    }
}